const headerSwiper = new Swiper('.header-swiper', {
    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },  
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
});

const latestBooksSwiper = new Swiper('.latest-books-swiper', {
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 1
        },
        560: {
            slidesPerView: 1.5
        },
        768: {
            slidesPerView: 2.5
        },
        1024: {
            slidesPerView: 3
        },
        1200: {
            slidesPerView: 4
        }
    }
});

const populerBooksSwiper = new Swiper('.populer-books-swiper', {
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 1
        },
        560: {
            slidesPerView: 1.5
        },
        768: {
            slidesPerView: 2.5
        },
        1024: {
            slidesPerView: 3
        },
        1200: {
            slidesPerView: 4
        }
    }
});

const favoriteBooksSwiper = new Swiper('.favorite-books-swiper', {
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 1
        },
        560: {
            slidesPerView: 1.5
        },
        768: {
            slidesPerView: 2.5
        },
        1024: {
            slidesPerView: 3
        },
        1200: {
            slidesPerView: 4
        }
    }
});

const relatedBooksSwiper = new Swiper('.related-books-swiper', {
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        0: {
            slidesPerView: 1
        },
        560: {
            slidesPerView: 1.5
        },
        768: {
            slidesPerView: 2.5
        },
        1024: {
            slidesPerView: 3
        },
        1200: {
            slidesPerView: 4
        }
    }
});