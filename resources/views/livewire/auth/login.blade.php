<div>
    <div class="container bg-login pb-2">
        <div class="d-flex flex-lg-row flex-column justify-content-center">
            <div class="">
                <img src="{{ asset('assets/img/logo-skenbooks.png') }}" alt="">
            </div>
            <div class="w-30 ms-4">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                        {{ session('error') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <form wire:submit.prevent="login">
                    <div class="mb-3">
                        <label for="username">Username</label>
                        <input type="text" wire:model="username" class="form-control @error('username') is-invalid @enderror" placeholder="Masukkan username">
                        @error('username')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="username">Password</label>
                        <input type="password" wire:model="password" class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan password">
                        @error('password')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <button type="submit" class="btn mt-2 btn-primary">Login</button>
                </form>
            </div>
        </div>
        <div class="text-center mt-5">
            <p>Belum mempunyai akun? <a href="{{ route('register') }}" class="text-primary">Daftar sekarang</a></p>
        </div>
    </div>
</div>
