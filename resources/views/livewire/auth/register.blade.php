<div>
    {{-- @if (session('error'))
        {{ session('error') }}
    @endif
    <form wire:submit.prevent="register">
        <input type="text" wire:model="name" placeholder="Masukkan nama">
        @error('name')
            <p>{{ $message }}</p>
        @enderror
        <input type="text" wire:model="username" placeholder="Masukkan username">
        @error('username')
            <p>{{ $message }}</p>
        @enderror
        <input type="text" wire:model="email" placeholder="Masukkan email">
        @error('email')
            <p>{{ $message }}</p>
        @enderror
        <input type="text" wire:model="password" placeholder="Masukkan password">
        @error('password')
            <p>{{ $message }}</p>
        @enderror
        <input type="text" wire:model="confirmation_password" placeholder="Masukkan konfirmasi password">

        <button type="submit">Register</button>
    </form> --}}
    <div class="container bg-register">
        <div class="card">
            <div class="card-header text-center">
                <h5 class="text-primary">Buat Akun</h5>
            </div>
            <div class="card-body">
                <form wire:submit.prevent="register">
                    <div class="row g-4">
                        <div class="col-md-6 col-12">
                            <div>
                                <label for="name" class="h6 text-primary">Nama</label>
                                <input type="text" id="name" class="form-control @error('name') is-invalid @enderror" wire:model="name"
                                    placeholder="Masukkan nama">
                                @error('name')
                                    <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div>
                                <label for="username" class="h6 text-primary">Username</label>
                                <input type="text" id="username" class="form-control @error('username') is-invalid @enderror" wire:model="username"
                                    placeholder="Masukkan username">
                                @error('username')
                                    <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <div>
                                <label for="email" class="h6 text-primary">Email</label>
                                <input type="text" id="email" class="form-control @error('email') is-invalid @enderror" wire:model="email"
                                    placeholder="Masukkan email">
                                @error('email')
                                    <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div>
                                <label for="password" class="h6 text-primary">Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" wire:model="password"
                                    placeholder="Masukkan password">
                                @error('password')
                                    <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div>
                                <label for="confirmation_password" class="h6 text-primary">Konfirmasi Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" id="confirmation_password"
                                    wire:model="confirmation_password" placeholder="Masukkan konfirmasi password">

                            </div>
                        </div>
                        <div class="d-block">
                            <button type="submit" class="btn btn-primary w-100">Register</button>
                        </div>
                    </div>
                </form>
                <div class="text-center mt-3">
                    <p>Sudah memiliki akun? <a href="{{ route('login') }}" class="text-primary">Login sekarang</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
