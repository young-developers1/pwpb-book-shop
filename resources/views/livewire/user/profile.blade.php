<div>
    @component('components.home-navbar')
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="profile-page">
        <div class="row">
            <div class="col-lg-3 col-12 d-lg-block d-none">
                @include('partials.order-nav')
            </div>
            <div class="col-lg-9 col-12">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-profile-tab" data-bs-toggle="tab"
                            data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile"
                            aria-selected="true">Pengaturan Akun</button>
                        <button class="nav-link" id="nav-address-tab" data-bs-toggle="tab" data-bs-target="#nav-address"
                            type="button" role="tab" aria-controls="nav-address" aria-selected="false">Daftar
                            Alamat</button>
                    </div>
                </nav>
                <div class="tab-content mt-3" id="nav-tabContent">
                    <div class="tab-pane fade show active" wire:ignore.self id="nav-profile" role="tabpanel"
                        aria-labelledby="nav-profile-tab" tabindex="0">
                        <form>
                            <div class="row row-cols-lg-2 row-cols-1">
                                <div>
                                    @if (session()->has('successProfile'))
                                        <div class="alert alert-success alert-dismissible fade show my-3"
                                            role="alert">
                                            {{ session('successProfile') }}
                                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                aria-label="Close"></button>
                                        </div>
                                    @elseif (session()->has('errorProfile'))
                                        <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                                            {{ session('errorProfile') }}
                                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                                aria-label="Close"></button>
                                        </div>
                                    @endif
                                    <div class="mb-3">
                                        <label for="">Nama Lengkap</label>
                                        <input type="text" placeholder="Masukkan nama lengkap"
                                            class="col-4 form-control @error('name') is-invalid @enderror"
                                            wire:model="name">
                                        @error('name')
                                            <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Username</label>
                                        <input type="text" placeholder="Masukkan username"
                                            class="col-4 form-control @error('username') is-invalid @enderror"
                                            wire:model="username">
                                        @error('username')
                                            <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <label for="">Email</label>
                                        <input type="text" placeholder="Masukkan email"
                                            class="col-4 form-control @error('email') is-invalid @enderror"
                                            wire:model="email">
                                        @error('email')
                                            <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <button class="btn btn-primary btn-sm mt-3"
                                        wire:click.prevent="updateUserProfile">Simpan
                                        perubahan akun</button>

                        </form>

                        <hr>

                        <h6>Ubah kata sandi</h6>
                        @if (session()->has('passwordUpdateSuccess'))
                            <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                                {{ session('passwordUpdateSuccess') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                        @elseif (session()->has('passwordNotMatch'))
                            <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                                {{ session('passwordNotMatch') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                        @endif
                        <div class="mb-3">
                            <label for="">Kata sandi lama</label>
                            <input type="password" placeholder="Masukkan kata sandi lama"
                                class="col-4 form-control @error('old_password') is-invalid @enderror"
                                wire:model="old_password">
                            @error('old_password')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="">Kata sandi baru</label>
                            <input type="password" placeholder="Masukkan kata sandi baru"
                                class="col-4 form-control @error('new_password') is-invalid @enderror"
                                wire:model="new_password">
                            @error('new_password')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="">Konfirmasi kata sandi baru</label>
                            <input type="password"
                                placeholder="Masukkan konfirmasi kata sandi baru"
                                class="col-4 form-control @error('new_password') is-invalid @enderror" wire:model="confirm_new_password">
                        </div>

                        <button class="btn btn-primary btn-sm mt-3" wire:click.prevent="updatePassword">Simpan
                            perubahan password</button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="nav-address" wire:ignore.self role="tabpanel"
                aria-labelledby="nav-address-tab" tabindex="0">
                @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                            aria-label="Close"></button>
                    </div>
                @elseif (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                        {{ session('error') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert"
                            aria-label="Close"></button>
                    </div>
                @endif
                <div class="d-flex justify-content-end mb-3">
                    <button data-bs-toggle="modal" data-bs-target="#createAddressModal"
                        class="btn btn-primary">Tambah Alamat</button>
                </div>
                <div>
                    @foreach ($user_addresses as $address)
                        <div class="card px-3 py-2 mb-3">
                            <div class="d-flex">
                                <p class="text-primary text-capitalize" style="margin-bottom: -5px">
                                    {{ $address->label }}</p>
                                @if ($address->is_active)
                                    <span class="badge text-bg-primary ms-2 mt-1">Utama</span>
                                @endif
                            </div>
                            <div class="mt-3">
                                <h6>{{ $address->receipent_name }}</h6>
                                <p>{{ $address->district . ' ' . $address->pos_code }}</p>
                                <p>{{ $address->regency . ' - ' . $address->province }}</p>
                                <p>No Telp. {{ $address->phone }}</p>
                            </div>
                            <div class="mt-3">
                                @if ($address->is_active == 0)
                                    <a class="me-3" href="#" data-bs-toggle="modal"
                                        data-bs-target="#editActiveAddressModal"
                                        wire:click="editActiveAddress({{ $address->id }})"><i
                                            class="fa-regular fa-circle-check me-1"></i> Set Utama</a>
                                @endif
                                <a href="#" class="me-3" data-bs-toggle="modal"
                                    data-bs-target="#editAddressModal"
                                    wire:click="editAddress({{ $address->id }})"><i
                                        class="fa-solid fa-pen me-1"></i> Ubah</a>
                                @if ($address->is_active == 0)
                                    <a href="#" data-bs-toggle="modal" data-bs-target="#deleteAddressModal"
                                        wire:click="deleteAddress({{ $address->id }})"><i
                                            class="fa-solid fa-trash me-1"></i> Hapus</a>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@include('partials.modal-checkout')
</div>

@component('components.home-footer')
    @slot('categories')
        @foreach ($categories as $category)
            <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                    class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
            </li>
        @endforeach
    @endslot
@endcomponent

</div>

@push('js')
    <script>
        window.livewire.on('addressAdded', () => {
            $("#createAddressModal").modal('hide')
        })
        window.livewire.on('updatedActiveAddress', () => {
            $("#allAddressModal").modal('hide')
        })
        window.livewire.on('addressUpdated', () => {
            $("#editAddressModal").modal('hide')
        })
        window.livewire.on('addressDeleted', () => {
            $("#deleteAddressModal").modal('hide')
        })
        window.livewire.on('activeAddressUpdated', () => {
            $("#editActiveAddressModal").modal('hide')
        })
    </script>
@endpush
