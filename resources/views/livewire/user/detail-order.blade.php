<div>
    @component('components.home-navbar')
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="order-detail-page">
        <div class="d-flex">
            <h4 class="mb-4">Detail Pesanan</h4>
            @if ($order->status == 'Pesanan sampai')
                <button class="btn btn-primary ms-3" data-bs-toggle="modal"
                    data-bs-target="#completingOrderModal">Selesaikan Pesanan</button>
            @elseif($order->status == 'Menunggu pembayaran')
                <button class="btn btn-danger ms-3" data-bs-toggle="modal"
                    data-bs-target="#cancellingOrderModal">Batalkan Pesanan</button>
                <a href="https://wa.link/5juxc6" target="_blank" class="btn btn-success ms-3"><i class="fa-brands fa-whatsapp me-2 fa-lg"></i>Konfirmasi Pesanan</a>
            @endif
        </div>
        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @elseif (session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-7 col-12">
                <p>Rincian Barang</p>
                <div class="mt-3">
                    <div class="card p-4">
                        @foreach ($order->order_items as $item)
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex">
                                    @if ($item->book->thumbnail_img)
                                        <img src="{{ asset('storage/' . $item->book->thumbnail_img) }}" width="70"
                                            height="100%" alt="Buku {{ $item->book->title }}">
                                    @else
                                        <img src="{{ asset('assets/img/image-not-available.jpg') }}" width="70"
                                            height="100%" alt="Buku {{ $item->book->title }}">
                                    @endif
                                    <div class="ms-2">
                                        <h6>{{ $item->book->title }}</h6>
                                        <p class="text-muted" style="margin-top: -5px; font-size: 0.9em">
                                            {{ $item->quantity }} barang
                                            ({{ $item->quantity * $item->book->weight }})
                                            gram</p>
                                        @if ($item->book->discount > 0)
                                            <div class="d-flex" style="margin-top: -5px">
                                                <p class="cart-price" style="font-size: 0.9em">Rp. @rupiah(($item->book->price * (100 - $item->book->discount)) / 100)</p>
                                                <p class="ms-2" style="font-size: 0.85em"><s>Rp.
                                                        @rupiah($item->book->price)</s>
                                                </p>
                                            </div>
                                        @else
                                            <p class="cart-price" style="margin-top: -5px; font-size: 0.9em">Rp.
                                                @rupiah($item->book->price)</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="d-flex justify-content-between mb-3">
                                <p>Subtotal</p>
                                @if ($item->book->discount > 0)
                                    <h6>Rp. @rupiah((($item->book->price * (100 - $item->book->discount)) / 100) * $item->quantity)</h6>
                                @else
                                    <h6>Rp. @rupiah($item->book->price * $item->quantity)</h6>
                                @endif
                            </div>
                            @if ($order->status == 'Pesanan selesai')
                                <div class="d-flex justify-content-end">
                                    <button class="btn btn-info btn-sm me-3" data-bs-toggle="modal"
                                        data-bs-target="#reviewBookModal" wire:click="review({{ $item->book->id }})"><i
                                            class="fa-solid fa-star me-1 position-relative" style="top: -2px"></i>Beri
                                        Ulasan</button>
                                    <button class="btn btn-warning btn-sm"
                                        wire:click="buyAgain({{ $item->book->id }})"><i
                                            class="fa-solid fa-store me-1 position-relative" style="top: -2px"></i>Beli
                                        Lagi</button>
                                </div>
                            @endif
                        @endforeach
                    </div>

                </div>
            </div>

            <div class="col-lg-5 col-12">
                <div class="mb-4">
                    <p>Info Pemesanan</p>
                    <div class="card p-4">
                        <div class="mb-1">
                            <p class="font-weight-bold" style="margin-bottom: 5px; font-size: 0.9em">Status Transaksi
                            </p>
                            @if ($order->status === 'Menunggu pembayaran' ||
                                $order->status === 'Sedang diproses' ||
                                $order->status === 'Sedang dikirim' ||
                                $order->status === 'Pesanan sampai')
                                <p class="order-processing px-3 py-2 rounded font-weight-bold"
                                    style="font-size: 0.7em; width: max-content">
                                    {{ $order->status }}
                                </p>
                            @elseif ($order->status === 'Pesanan selesai')
                                <p class="order-completed px-3 py-2 rounded font-weight-bold"
                                    style="font-size: 0.7em; width: max-content">
                                    {{ $order->status }}
                                </p>
                            @elseif ($order->status === 'Pesanan dibatalkan')
                                <p class="px-3 py-2 rounded font-weight-bold order-cancelled"
                                    style="font-size: 0.7em; width: max-content">
                                    {{ $order->status }}
                                </p>
                            @endif
                        </div>
                        <div class="mb-1">
                            <p class="font-weight-bold" style="margin-bottom: 5px; font-size: 0.9em">No. Pesanan</p>
                            <p style="font-size: 0.8em">{{ $order->order_code }}</p>
                        </div>
                        <div class="mb-1">
                            <p class="font-weight-bold" style="margin-bottom: 5px; font-size: 0.9em">Tanggal Pemesanan
                            </p>
                            <p style="font-size: 0.8em">{{ $order->created_at->format('d M Y') }}</p>
                        </div>
                    </div>
                </div>
                <div class="mb-4">
                    <p>Rincian Pembayaran</p>
                    <div class="card p-4">
                        <div class="detail-cost">
                            <h6>Total biaya</h6>
                            <div class="d-flex justify-content-between">
                                <p>Subtotal</p>
                                <p>Rp. @rupiah($order->sub_total)</p>
                            </div>
                            <div class="d-flex justify-content-between">
                                <p>Biaya pengiriman</p>
                                <p>+ Rp. @rupiah($order->shipping_price)</p>
                            </div>
                            <div class="d-flex justify-content-between">
                                <p>Potongan kupon</p>
                                @if ($order->voucher_id != null)
                                    @if ($order->voucher->type == 'Persentase')
                                        <p>- @rupiah($order->voucher->nominal)%</p>
                                    @elseif ($order->voucher->type == 'Potongan Tetap')
                                        <p>- Rp. @rupiah($order->voucher->nominal)</p>
                                    @endif
                                @else
                                    <p>- Rp. 0</p>
                                @endif
                            </div>
                        </div>

                        <hr>

                        <div class="d-flex justify-content-between">
                            <p>Total Biaya</p>
                            @if ($order->voucher_id != null)
                                @if ($order->voucher->type == 'Persentase')
                                    <p>Rp. @rupiah(($order->sub_total + $order->shipping_price) * (100 - $order->voucher->nominal) / 100)</p>
                                @elseif ($order->voucher->type == 'Potongan Tetap')
                                    <p>Rp. @rupiah($order->subtotal + $order->shipping_price - $order->voucher->nominal)</p>
                                @endif
                            @else
                                <p>Rp. @rupiah($order->sub_total + $order->shipping_price)</p>
                            @endif
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Metode Pembayaran</p>
                            <p class="font-weight-bold">{{ strtoupper($order->payment_method) }}</p>
                        </div>
                    </div>
                </div>

                <div class="mb-4">
                    <p>Rincian Pengiriman</p>
                    <div class="card p-4">
                        <div class="mb-1">
                            <p class="font-weight-bold" style="margin-bottom: 5px; font-size: 0.9em">Metode Pengiriman
                            </p>
                            <p style="font-size: 0.8em">{{ strtoupper($order->shipping_method) }} (Perkiraan sampai 19
                                November 2022 - 23 November 2022)</p>
                        </div>
                        <hr>
                        <div class="mb-1">
                            <p class="font-weight-bold" style="margin-bottom: 5px; font-size: 0.9em"><i
                                    class="fa-solid fa-truck me-2"></i> Alamat Tujuan Pengiriman
                            </p>
                            <div class="d-flex">
                                <p style="font-size: 0.8em">{{ $order->user_address->receipent_name }}</p>
                                <span class="mx-2 position-relative" style="top: -5px ">|</span>
                                <p style="font-size: 0.8em">{{ $order->user_address->phone }}</p>
                            </div>
                            <div class="d-flex">
                                <p style="font-size: 0.8em">{{ $order->user_address->address }},
                                    {{ $order->user_address->district }}, {{ $order->user_address->regency }},
                                    {{ $order->user_address->province }} - {{ $order->user_address->pos_code }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @include('partials.modal-detail-order')
</div>

@component('components.home-footer')
    @slot('categories')
        @foreach ($categories as $category)
            <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                    class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
            </li>
        @endforeach
    @endslot
@endcomponent
</div>

@push('js')
    <script>
        window.livewire.on('reviewCreated', () => {
            $("#reviewBookModal").modal('hide')
        })
    </script>
@endpush
