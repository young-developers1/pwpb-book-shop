<div>
    @component('components.home-navbar')
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="book-page">
        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @elseif (session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="row g-5 mb-5">
            <div class="col-xl-5 col-lg-6 col-12">
                <div class="container-slideshow">

                    <div class="mySlides">
                        @if ($book->thumbnail_img)
                            <div>
                                <img src="{{ asset('storage/' . $book->thumbnail_img) }}" class="card-img-top shadow-sm"
                                    alt="Buku {{ $book->title }}" width="100%" style="background-position: center">
                            </div>
                        @else
                            <div>
                                <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                    class="card-img-top shadow-sm" alt="Buku {{ $book->title }}" width="100%" style="background-position: center">
                            </div>
                        @endif
                    </div>

                    @foreach ($book_images as $image)
                        <div class="mySlides">
                            <img class="" src="{{ asset('storage/' . $image->image) }}" width="100%" style="background-position: center"
                                alt="sub gambar {{ $book->title }}">
                        </div>
                    @endforeach

                    <!-- Next and previous buttons -->
                    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                    <a class="next" onclick="plusSlides(1)">&#10095;</a>

                    <!-- Thumbnail images -->
                    <div class="row mt-3">
                        <div class="column">
                            @if ($book->thumbnail_img)
                                <img src="{{ asset('storage/' . $book->thumbnail_img) }}"
                                    class="card-img-top demo cursor" onclick="currentSlide(1)"
                                    alt="Buku {{ $book->title }}">
                            @else
                                <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                    class="card-img-top demo cursor" onclick="currentSlide(1)"
                                    alt="Buku {{ $book->title }}">
                            @endif
                        </div>
                        @foreach ($book_images as $image)
                            <div class="column">
                                <img onclick="currentSlide({{ $loop->index + 2 }})" class="demo cursor m-2"
                                    src="{{ asset('storage/' . $image->image) }}" width="100%"
                                    alt="sub gambar {{ $book->title }}">
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xl-7 col-lg-6 col-12">
                <a href="/books?category={{ $book->category->id }}"
                    class="text-danger font-weight-bold fs-6">{{ $book->category->category_name }}</a>
                <div class="my-2">
                    <h2>{{ $book->title }}</h2>
                    <div class="d-flex">
                        <div class="d-flex">
                            <div><i class="fa-solid fa-star me-2 text-warning"></i></div>
                            <p>{{ ($rating_value * 2) % 2 == 0 ? $rating_value . '.0' : $rating_value }}</p>
                        </div>
                        <span class="mx-2">|</span>
                        <p>Terjual {{ $book_sold_count }}</p>
                        <span class="mx-2">|</span>
                        <p>Stok {{ $book->stock }}</p>
                    </div>
                    <div class="mt-3">
                        <h5 class="text-primary">Deskripsi Buku</h5>
                        <p>{{ $book->description }}</p>
                    </div>
                    <div class="mt-5">
                        <h5 class="text-primary">Detail Buku</h5>
                        <div class="row detail-book mb-2">
                            <div class="col-3 me-5">
                                <p>Jumlah Halaman</p>
                                <p class="data">{{ $book->total_page }} halaman</p>
                            </div>
                            <div class="col-3">
                                <p>Tanggal Terbit</p>
                                <p class="data">{{ $book->issue_date->format('d M Y') }}</p>
                            </div>
                        </div>
                        <div class="row detail-book mb-2">
                            <div class="col-3 me-5">
                                <p>Penulis</p>
                                <p class="data">{{ $book->writer }}</p>
                            </div>
                            <div class="col-3">
                                <p>Penerbit</p>
                                <p class="data">{{ $book->publisher }}</p>
                            </div>
                        </div>
                        <div class="row detail-book mb-2">
                            <div class="col-3 me-5">
                                <p>Berat</p>
                                <p class="data">{{ $book->weight }}</p>
                            </div>
                            <div class="col-3">
                                <p>Bahasa</p>
                                <p class="data">{{ $book->language }}</p>
                            </div>
                        </div>
                        <div class="row detail-book mb-2">
                            <div class="col-3 me-5">
                                <p>ISBN</p>
                                <p class="data">{{ $book->isbn }}</p>
                            </div>
                            <div class="col-3">
                                <p>Stok</p>
                                <p class="data">{{ $book->stock }}</p>
                            </div>
                        </div>
                        <div class="d-flex mt-4">
                            <div>
                                @if ($book->discount > 0)
                                    <h5>Rp. @rupiah(($book->price * (100 - $book->discount)) / 100)</h5>
                                    <s>Rp. @rupiah($book->price)</s>
                                @else
                                    <h5>Rp. @rupiah($book->price)</h5>
                                @endif
                            </div>
                            <span class="mx-3">|</span>
                            <div class="d-flex">
                                <h5 class="me-3">Kuantitas</h5>
                                <button class="btn" wire:click="decrementQty"><i
                                        class="fa-solid fa-minus"></i></button>
                                <input type="text" class="input-qty" wire:model="quantity">
                                <button class="btn" wire:click="incrementQty"><i
                                        class="fa-solid fa-plus"></i></button>
                            </div>
                        </div>
                        <hr>

                        <div class="d-flex mt-4">
                            <button wire:click="addToCart" class="btn btn-primary me-3"><i
                                    class="fa-solid fa-cart-shopping me-2"></i>Keranjang</button>
                            <button wire:click="buyNow" class="btn btn-danger"><i
                                    class="fa-solid fa-bag-shopping me-2"></i>Beli
                                Langsung</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if ($related_books->count() > 0)
            <div class="p-5 rounded" style="background: #C5E1EC;">
                <h3 class="mb-4">Buku Terkait</h3>
                <div class="swiper related-books-swiper">
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach ($related_books as $book)
                            <div class="swiper-slide" wire:ignore>
                                <a
                                    href="{{ route('book', [strtolower($book->category->category_name), $book->slug]) }}">
                                    <div class="card" style="height: 400px">
                                        <div style="height: 250px; overflow: hidden">
                                            @if ($book->thumbnail_img)
                                                <img src="{{ asset('storage/' . $book->thumbnail_img) }}"
                                                    class="card-img-top" alt="Buku {{ $book->title }}">
                                            @else
                                                <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                                    class="card-img-top" alt="Buku {{ $book->title }}">
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <p class="text-muted category-text">{{ $book->category->category_name }}
                                            </p>
                                            <h5>{{ $book->title }}</h5>
                                            <div class="text-dark">
                                                @if ($book->discount > 0)
                                                    <p class="mb-0 text-primary font-weight-bold fs-5">Rp.
                                                        @rupiah(($book->price * (100 - $book->discount)) / 100)</p>
                                                    <p class="fs-6"><s>Rp. @rupiah($book->price)</s></p>
                                                @else
                                                    <p class="mb-0 text-primary font-weight-bold fs-5">Rp.
                                                        @rupiah($book->price)</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
            </div>
        @endif

        <div class="mt-5 border-top pt-5">
            <div class="row">
                <div class="col-md-4 col-12">
                    <h3 class="mb-3">Ulasan Pembeli</h3>
                    <div class="d-flex">
                        <h2><i class="fa-solid fa-star text-warning"></i>
                            {{ ($rating_value * 2) % 2 == 0 ? $rating_value . '.0' : $rating_value }}/</h2>
                        <span class="text-dark" style="position: relative; top: 18px">5.0</span>
                    </div>
                    <div class="d-flex text-dark">
                        <p>{{ $rating_count }} rating</p>
                        <span class="mx-3">|</span>
                        <p>{{ $review_count }} ulasan</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-8 col-12">
                    @if (session()->has('successReview'))
                        <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                            {{ session('successReview') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                aria-label="Close"></button>
                        </div>
                    @elseif (session()->has('errorReview'))
                        <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                            {{ session('errorReview') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert"
                                aria-label="Close"></button>
                        </div>
                    @endif
                    @if ($reviewBooks->count() > 0)
                        <div class="d-flex justify-content-end">
                            <select class="form-select bg-primary" wire:model="filter_rating" style="width: 200px">
                                <option value="latest">Terbaru</option>
                                <option value="oldest">Terlama</option>
                                <option value="highest_rate">Rating tertinggi</option>
                                <option value="lowest_rate">Rating terendah</option>
                            </select>
                        </div>
                        @foreach ($reviewBooks as $book)
                            <div class="card shadow-none border-bottom rounded-0 border-secondary p-3 mt-4">
                                <div class="d-flex justify-content-between mb-2">
                                    <div class="d-flex">
                                        <div class="d-flex">
                                            @for ($i = 1; $i <= $book->rating; $i++)
                                                <i class="fa-solid fa-star text-warning me-1"></i>
                                            @endfor
                                            @for ($j = $book->rating + 1; $j <= 5; $j++)
                                                <i class="fa-regular fa-star text-warning me-1"></i>
                                            @endfor
                                        </div>
                                        <p class="position-relative ms-1" style="top: -2px; font-size: 0.9em">
                                            {{ $book->created_at->diffForHumans() }}</p>
                                    </div>
                                    @auth
                                        @if (auth()->id() == $book->user_id)
                                            <div>
                                                <li class="nav-item dropdown list-unstyled">
                                                    <a class="" href="#" role="button"
                                                        data-bs-toggle="dropdown" aria-expanded="false">
                                                        <i class="fa-solid fa-ellipsis-vertical"></i>
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        @if (auth()->user()->role_as == 'user')
                                                            <li><a class="dropdown-item" href="#"
                                                                    data-bs-toggle="modal"
                                                                    data-bs-target="#editReviewBookModal"
                                                                    wire:click="review({{ $book->id }})"><i
                                                                        class="fa-solid fa-pencil me-2"></i>Edit</a></li>
                                                            <li><a class="dropdown-item" data-bs-toggle="modal"
                                                                    data-bs-target="#deleteReviewBookModal"
                                                                    wire:click="deleteReview({{ $book->id }})"
                                                                    href="#"><i
                                                                        class="fa-solid fa-trash me-2"></i>Hapus</a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </li>
                                            </div>
                                        @endif
                                    @endauth
                                </div>
                                <div>
                                    <div class="d-flex">
                                        <p><i class="fa-solid fa-circle-user fa-xl"></i></p>
                                        @if ($book->is_hidden)
                                            <p class="ms-2 font-weight-bold">Anonymous</p>
                                        @else
                                            <p class="ms-2 font-weight-bold">{{ $book->user->username }}</p>
                                        @endif
                                    </div>
                                    <p style="font-size: 0.9em">{{ $book->review }}</p>
                                </div>
                            </div>
                        @endforeach
                        <div class="d-flex justify-content-end">
                            {{ $reviewBooks->links() }}
                        </div>
                    @else
                        <p class="text-center">Belum ada ulasan</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('partials.modal-detail-book')

    @component('components.home-footer')
        @slot('categories')
            @foreach ($categories as $category)
                <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                        class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
                </li>
            @endforeach
        @endslot
    @endcomponent
</div>

@push('js')
    <script>
        window.livewire.on('reviewUpdated', () => {
            $('#editReviewBookModal').modal('hide')
        })
        window.livewire.on('reviewDeleted', () => {
            $('#deleteReviewBookModal').modal('hide')
        })

        let slideIndex = 1;
        showSlides(slideIndex);

        // Next/previous controls
        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            let i;
            let slides = document.getElementsByClassName("mySlides");
            let dots = document.getElementsByClassName("demo");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
        }
    </script>
@endpush
