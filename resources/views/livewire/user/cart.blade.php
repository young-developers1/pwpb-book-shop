<div>
    @component('components.home-navbar')
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="cart-page">
        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @elseif (session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="row g-4">
            @if ($carts->count() > 0)
                <div class="col-xl-9 col-lg-7 col-md-6 col-12">
                    <div class="border mb-3 rounded px-3 d-flex justify-content-between py-2">
                        <div class="d-flex">
                            <input type="checkbox" wire:click="checkAll" wire:model="is_checked" id="check-all"
                                class="form-check">
                            <label for="check-all" class="ms-2 mt-1">Pilih Semua ({{ $count_cart }})</label>
                        </div>
                        <a href="#" wire:click="deleteAllCart" class="text-danger">Hapus Semua</a>
                    </div>
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($carts as $cart)
                        <div class="d-flex justify-content-between align-items-center border px-3 py-2 mb-3">
                            <div class="d-flex">
                                <form>
                                    <input type="checkbox" wire:click.prevent="updateChecked({{ $cart->id }})"
                                        {{ $cart->is_checked ? 'checked' : '' }} class="form-check"
                                        id="book{{ $cart->id }}">
                                </form>
                                <label for="book{{ $cart->id }}">
                                    @if ($cart->book->thumbnail_img)
                                        <img src="{{ asset('storage/' . $cart->book->thumbnail_img) }}" class="mx-2"
                                            width="70" alt="Buku {{ $cart->book->title }}">
                                    @else
                                        <img src="{{ asset('assets/img/image-not-available.jpg') }}" class="mx-2"
                                            width="70" alt="Buku {{ $cart->book->title }}">
                                    @endif
                                </label>
                                <div class="cart-items-detail">
                                    <h6>{{ $cart->book->title }}</h6>
                                    @if ($cart->book->discount > 0)
                                        <p class="cart-price text-primary font-weight-bold" style="margin-top: -5px">Rp. @rupiah(($cart->book->price * (100 - $cart->book->discount)) / 100)</p>
                                        <p style="margin-top: -10px; font-size: 0.85em"><s>Rp. @rupiah($cart->book->price)</s></p>
                                    @else
                                        <p class="cart-price text-primary font-weight-bold" style="margin-top: -5px">Rp. @rupiah($cart->book->price)</p>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex">
                                <button class="btn" wire:click="decrementQty({{ $cart->id }})"><i
                                        class="fa-solid fa-minus"></i></button>
                                <input type="text" class="input-qty"
                                    wire:click="updateQuantity({{ $cart->id }})" value="{{ $cart->quantity }}">
                                <button class="btn" wire:click="incrementQty({{ $cart->id }})"><i
                                        class="fa-solid fa-plus"></i></button>
                            </div>
                            <div>
                                @if ($cart->book->discount > 0)
                                    <h6>Rp. @rupiah((($cart->book->price * (100 - $cart->book->discount)) / 100) * $cart->quantity)</h6>
                                @else
                                    <h6>Rp. @rupiah($cart->book->price * $cart->quantity)</h6>
                                @endif
                                <a href="#" wire:click="deleteCart({{ $cart->id }})" class="text-danger">
                                    <i class="fa-solid fa-trash"></i>
                                    Hapus
                                </a>
                            </div>
                        </div>

                        @if ($cart->is_checked)
                            @if ($cart->book->discount > 0)
                                @php
                                    $total += (($cart->book->price * (100 - $cart->book->discount)) / 100) * $cart->quantity;
                                @endphp
                            @else
                                @php
                                    $total += $cart->book->price * $cart->quantity;
                                @endphp
                            @endif
                        @endif
                    @endforeach
                </div>
                <div class="col-xl-3 col-lg-5 col-md-6 col-12">
                    <div class="border px-3 pt-2 pb-0">
                        <h5>Rincian Belanja</h5>
                        <hr>
                        <div class="my-2">
                            Total Biaya Rp. @rupiah($total)
                        </div>
                        <button class="btn btn-primary mt-3" wire:click="checkBeforeCheckout">Lanjut ke
                            pembayaran</button>
                    </div>
                </div>
            @else
                <div class="text-center">
                    <img src="{{ asset('assets/img/empty-cart.png') }}" width="200" alt="">
                    <p>Kamu belum menambahkan buku di keranjang</p>
                    <a href="{{ route('books') }}" class="btn btn-primary">Cari buku</a>
                </div>
            @endif
        </div>

        <div class="p-5 rounded related-books" wire:ignore>
            <h3 class="mb-4">Rekomendasi Buku Untukmu</h3>
            <div class="swiper related-books-swiper">
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    @foreach ($recomended_books as $book)
                        <div class="swiper-slide">
                            <a href="{{ route('book', [strtolower($book->category->category_name), $book->slug]) }}">
                                <div class="card" style="height: 400px">
                                    <div style="height: 250px; overflow: hidden">
                                        @if ($book->thumbnail_img)
                                            <img src="{{ asset('storage/' . $book->thumbnail_img) }}"
                                                class="card-img-top" alt="Buku {{ $book->title }}">
                                        @else
                                            <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                                class="card-img-top" alt="Buku {{ $book->title }}">
                                        @endif
                                    </div>
                                    <div class="card-body">
                                        <p class="text-muted category-text">{{ $book->category->category_name }}
                                        </p>
                                        <h5>{{ $book->title }}</h5>
                                        <div class="text-dark">
                                            @if ($book->discount > 0)
                                                <p class="mb-0 text-primary font-weight-bold fs-5">Rp. @rupiah(($book->price * (100 - $book->discount)) / 100)</p>
                                                <p class="fs-6"><s>Rp. @rupiah($book->price)</s></p>
                                            @else
                                                <p class="mb-0 text-primary font-weight-bold fs-5">Rp. @rupiah($book->price)</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>


    @component('components.home-footer')
        @slot('categories')
            @foreach ($categories as $category)
                <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                        class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
                </li>
            @endforeach
        @endslot
    @endcomponent
</div>
