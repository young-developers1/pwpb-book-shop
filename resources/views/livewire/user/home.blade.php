<div>
    @component('components.home-navbar')
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="home-page">
        <header>
            <!-- Slider main container -->
            <div class="swiper header-swiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide"><img src="{{ asset('assets/img/hero-img4.png') }}" width="100%"
                            alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/img/hero-img1.jpg') }}" width="100%"
                            alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/img/hero-img2.png') }}" width="100%"
                            alt=""></div>
                    <div class="swiper-slide"><img src="{{ asset('assets/img/hero-img3.png') }}" width="100%"
                            alt=""></div>
                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination"></div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>

            </div>
        </header>

        <section id="latest-books">
            <div class="d-flex justify-content-between mb-4">
                <h3>Buku Terbaru</h3>
                <a href="{{ route('books') }}" class="text-primary">Lihat Semua</a>
            </div>

            <div class="swiper latest-books-swiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    @foreach ($latest_books as $book)
                        <div class="swiper-slide">
                            <a href="{{ route('book', [strtolower($book->category->category_name), $book->slug]) }}">
                                <div class="card" style="height: 400px">
                                    <div style="height: 250px; overflow: hidden">
                                        @if ($book->thumbnail_img)
                                            <img src="{{ asset('storage/' . $book->thumbnail_img) }}"
                                                class="card-img-top" alt="Buku {{ $book->title }}">
                                        @else
                                            <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                                class="card-img-top" alt="Buku {{ $book->title }}">
                                        @endif
                                    </div>
                                    <div class="card-body">
                                        <p class="text-muted category-text">{{ $book->category->category_name }}</p>
                                        <h5>{{ $book->title }}</h5>
                                        <div class="text-dark">
                                            @if ($book->discount > 0)
                                                <p class="mb-0 text-primary font-weight-bold fs-5">Rp. @rupiah(($book->price * (100 - $book->discount)) / 100)
                                                </p>
                                                <p class="fs-6"><s>Rp. @rupiah($book->price)</s></p>
                                            @else
                                                <p class="mb-0 text-primary font-weight-bold fs-5">Rp. @rupiah($book->price)
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>

            </div>
        </section>

        <section id="populer-books">
            <div class="d-flex justify-content-between mb-4">
                <h3>Buku - Buku Terpopuler</h3>
                <a href="{{ route('books') }}" class="text-primary">Lihat Semua</a>
            </div>

            <div class="swiper populer-books-swiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    @foreach ($latest_books as $book)
                        <div class="swiper-slide">
                            <a href="{{ route('book', [strtolower($book->category->category_name), $book->slug]) }}">
                                <div class="card" style="height: 400px">
                                    <div style="height: 250px; overflow: hidden">
                                        @if ($book->thumbnail_img)
                                            <img src="{{ asset('storage/' . $book->thumbnail_img) }}"
                                                class="card-img-top" alt="Buku {{ $book->title }}">
                                        @else
                                            <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                                class="card-img-top" alt="Buku {{ $book->title }}">
                                        @endif
                                    </div>
                                    <div class="card-body">
                                        <p class="text-muted category-text">{{ $book->category->category_name }}</p>
                                        <h5>{{ $book->title }}</h5>
                                        <div class="text-dark">
                                            @if ($book->discount > 0)
                                                <p class="mb-0 text-primary font-weight-bold fs-5">Rp. @rupiah(($book->price * (100 - $book->discount)) / 100)
                                                </p>
                                                <p class="fs-6"><s>Rp. @rupiah($book->price)</s></p>
                                            @else
                                                <p class="mb-0 text-primary font-weight-bold fs-5">Rp. @rupiah($book->price)
                                                </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>

            </div>
        </section>

        @if ($favorite_books->count() > 0)
            <section id="favorite-books">
                <div class="d-flex justify-content-between mb-4">
                    <h3>Buku Paling Favorit</h3>
                    <a href="{{ route('books') }}" class="text-primary">Lihat Semua</a>
                </div>

                <div class="swiper favorite-books-swiper">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach ($favorite_books as $book)
                            <div class="swiper-slide">
                                <a
                                    href="{{ route('book', [strtolower($book->book->category->category_name), $book->book->slug]) }}">
                                    <div class="card" style="height: 400px">
                                        <div style="height: 250px; overflow: hidden">
                                            @if ($book->thumbnail_img)
                                                <img src="{{ asset('storage/' . $book->thumbnail_img) }}"
                                                    class="card-img-top" alt="Buku {{ $book->title }}">
                                            @else
                                                <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                                    class="card-img-top" alt="Buku {{ $book->title }}">
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <p class="text-muted category-text">
                                                {{ $book->book->category->category_name }}</p>
                                            <h5>{{ $book->book->title }}</h5>
                                            <div class="text-dark">
                                                @if ($book->book->discount > 0)
                                                    <p class="mb-0 text-primary font-weight-bold fs-5">Rp.
                                                        @rupiah(($book->book->price * (100 - $book->book->discount)) / 100)</p>
                                                    <p class="fs-6"><s>Rp. @rupiah($book->book->price)</s></p>
                                                @else
                                                    <p class="mb-0 text-primary font-weight-bold fs-5">Rp.
                                                        @rupiah($book->book->price)</p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>

                </div>
            </section>
        @endif
    </div>

    @component('components.home-footer')
        @slot('categories')
            @foreach ($categories as $category)
                <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                        class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
                </li>
            @endforeach
        @endslot
    @endcomponent

</div>
