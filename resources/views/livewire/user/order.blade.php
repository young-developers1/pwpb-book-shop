<div>
    @component('components.home-navbar')
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="order-page">
        <div class="row g-4">
            <div class="col-lg-3 col-12">
                @include('partials.order-nav')
            </div>
            <div class="col-lg-9 col-12">
                <h4>Daftar Transaksi</h4>
                @if (session()->has('success'))
                    <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if ($orders->count() > 0)
                    @foreach ($orders as $order)
                        <div class="card mb-3">
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <p>{{ $order->created_at->format('d M Y H:i:s') }}</p>
                                    <span class="mx-4 mb-3">|</span>
                                    <p>No. Pesanan <b>{{ $order->order_code }}</b></p>
                                    @if ($order->status === 'Menunggu pembayaran' ||
                                        $order->status === 'Sedang diproses' ||
                                        $order->status === 'Sedang dikirim' ||
                                        $order->status === 'Pesanan sampai')
                                        <p class="order-processing px-3 ms-3 py-2 rounded font-weight-bold"
                                            style="font-size: 0.9em">
                                            {{ $order->status }}
                                        </p>
                                    @elseif ($order->status === 'Pesanan selesai')
                                        <p class="order-completed px-3 py-2 ms-3 rounded font-weight-bold"
                                            style="font-size: 0.9em">
                                            {{ $order->status }}
                                        </p>
                                    @elseif ($order->status === 'Pesanan dibatalkan')
                                        <p class="px-3 py-2 ms-3 rounded font-weight-bold order-cancelled"
                                            style="font-size: 0.9em">
                                            {{ $order->status }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="card-body"
                                style="border-top: 1px solid rgba(0,0,0,0.1); border-bottom: 1px solid rgba(0,0,0,0.1)">
                                @php
                                    $discount = 0;
                                @endphp
                                @foreach ($order->order_items as $item)
                                    <div class="d-flex">
                                        <label for="book{{ $item->id }}">
                                            @if ($item->book->thumbnail_img)
                                                <img src="{{ asset('storage/' . $item->book->thumbnail_img) }}"
                                                    class="mx-2" width="70" alt="Buku {{ $item->book->title }}">
                                            @else
                                                <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                                    class="mx-2" width="70" alt="Buku {{ $item->book->title }}">
                                            @endif
                                        </label>
                                        <div class="">
                                            <h6>{{ $item->book->title }}</h6>
                                            <p style="margin-top: -10px">{{ $item->quantity }} barang</p>
                                        </div>
                                    </div>

                                    @if ($item->book->discount > 0)
                                        @php
                                            $discount += ($item->book->price * $item->quantity * $item->book->discount) / 100;
                                        @endphp
                                    @endif
                                @endforeach
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-between">
                                    <a href="{{ route('order-detail', $order->order_code) }}"
                                        class="text-primary">Lihat Detail Pesanan</a>
                                    @if ($order->voucher_id != null)
                                        @if ($order->voucher->type == 'Persentase')
                                            <p>Total pesanan <b>Rp. @rupiah(($order->sub_total + $order->shipping_price) * (100 - $order->voucher->nominal) / 100)</b></p>
                                        @else
                                            <p>Total pesanan <b>Rp. @rupiah($order->sub_total + $order->shipping_price - $order->voucher->nominal)</b></p>
                                        @endif
                                    @else
                                        <p>Total pesanan <b>Rp. @rupiah($order->sub_total + $order->shipping_price)</b></p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="d-flex justify-content-end">
                        {{ $orders->links() }}
                    </div>
                @else
                    <p class="text-primary">Belum ada pesanan</p>
                @endif
            </div>
        </div>
    </div>

    @component('components.home-footer')
        @slot('categories')
            @foreach ($categories as $category)
                <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                        class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
                </li>
            @endforeach
        @endslot
    @endcomponent

</div>
