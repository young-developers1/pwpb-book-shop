<div>
    @component('components.home-navbar')
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="checkout-page">
        <h4 class="mb-3">Checkout</h4>
        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
                {{ session('success') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @elseif (session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
                {{ session('error') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="row g-4">
            <div class="col-lg-6 col-12">
                <div class="border px-4 py-3 rounded mb-3">                    
                    @if ($user_addresses->count() > 0)
                        @if ($get_active_user_address)
                            <div class="d-flex justify-content-between">
                                <h6><i class="fa-solid fa-location-dot me-2 mb-3 text-primary"></i>Alamat Tujuan
                                    Pengiriman
                                </h6>
                                <a href="#" data-bs-toggle="modal" data-bs-target="#allAddressModal"
                                    class="text-primary">Ubah alamat</a>
                            </div>
                            <div>
                                <h6 class="text-capitalize">{{ $get_active_user_address->label }}</h6>
                                <p style="font-size: 0.9em; margin-bottom: 0px;">
                                    {{ $get_active_user_address->receipent_name }} |
                                    {{ $get_active_user_address->phone }}
                                </p>
                                <p style="font-size: 0.9em; margin-bottom: 0px;">
                                    {{ $get_active_user_address->address }},
                                    {{ $get_active_user_address->district }}, {{ $get_active_user_address->regency }},
                                    {{ $get_active_user_address->province }}, {{ $get_active_user_address->pos_code }}
                                </p>
                            </div>
                        @else
                            <div class="d-flex justify-content-between">
                                <h6><i class="fa-solid fa-location-dot me-2 mb-3 text-primary"></i>Alamat Tujuan
                                    Pengiriman
                                </h6>
                                <a href="#" data-bs-toggle="modal" data-bs-target="#allAddressModal"
                                    class="text-primary">Ubah alamat</a>
                            </div>
                            <button data-bs-toggle="modal" data-bs-target="#createAddressModal"
                                class="btn btn-outline-primary"><i class="fa-solid fa-plus me-2"></i>Buat Alamat
                                Pengiriman</button>
                        @endif
                    @else
                        <h5><i class="fa-solid fa-location-dot me-2 mb-3 text-primary"></i>Alamat Tujuan Pengiriman
                        </h5>
                        <button data-bs-toggle="modal" data-bs-target="#createAddressModal"
                            class="btn btn-outline-primary"><i class="fa-solid fa-plus me-2"></i>Buat Alamat
                            Pengiriman</button>
                    @endif
                </div>
                <div class="border px-4 py-3 rounded">
                    <h5>Items Summary</h5>
                    @php
                        $subtotal = 0;
                        $discount = 0;
                    @endphp
                    @foreach ($carts as $cart)
                        <div class="mb-3">
                            <div class="d-flex">
                                @if ($cart->book->thumbnail_img)
                                        <img src="{{ asset('storage/' . $cart->book->thumbnail_img) }}" width="80" height="100%"
                                            alt="Buku {{ $cart->book->title }}">
                                    @else
                                        <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                            width="80" height="100%" alt="Buku {{ $cart->book->title }}">
                                    @endif
                                <div class="ms-2">
                                    <h6>{{ $cart->book->title }}</h6>
                                    <p>{{ $cart->quantity }} barang ({{ $cart->book->weight * $cart->quantity }} gr)
                                    </p>
                                    <div class="d-flex" style="margin-top: -5px">
                                        @if ($cart->book->discount > 0)
                                            <p class="text-primary font-weight-bold">Rp. @rupiah(($cart->book->price * (100 - $cart->book->discount)) / 100)</p>
                                            <s class="ms-2">Rp. @rupiah($cart->book->price)</s>
                                        @else
                                            <p class="text-primary font-weight-bold">Rp. @rupiah($cart->book->price)</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="border mt-2 pb-0 pt-2 px-2 rounded d-flex justify-content-between">
                                <h6>Subtotal</h6>
                                @if ($cart->book->discount > 0)
                                    <p>Rp. @rupiah((($cart->book->price * (100 - $cart->book->discount)) / 100) * $cart->quantity)</p>
                                @else
                                    <p>Rp. @rupiah($cart->book->price * $cart->quantity)</p>
                                @endif
                            </div>
                        </div>

                        @php
                            $subtotal += $cart->book->price * $cart->quantity;
                        @endphp
                        @if ($cart->book->discount > 0)
                            @php
                                $discount += ($cart->book->price * $cart->quantity * $cart->book->discount) / 100;
                            @endphp
                        @endif
                    @endforeach

                    <hr>

                    <h6><i class="fa-solid fa-pen-to-square me-1"></i>Tambah Catatan</h6>
                    <textarea rows="4" wire:model="note" class="form-control"></textarea>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="border px-4 py-3 rounded mb-3">
                    <div class="mb-3">
                        <h5>Metode Pengiriman</h5>
                        <select class="form-select @error('shipping_method') is-invalid @enderror" wire:model="shipping_method">
                            <option value="">Pilih metode</option>
                            <option value="j&t">J&T (Rp. 35.000)</option>
                            <option value="sicepat">Sicepat (Rp. 31.500)</option>
                            <option value="jne">JNE (Rp. 28.000)</option>
                        </select>
                        @error('shipping_method')
                            <p class="text-danger">Pilih metode pengiriman terlebih dahulu!</p>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <h5>Metode Pembayaran</h5>
                        <select class="form-select @error('payment_method') is-invalid @enderror"" wire:model="payment_method">
                            <option value="">Pilih metode</option>
                            <option value="credit">Credit Card</option>
                            <option value="gopay">Gopay</option>
                            <option value="cod">Cash On Delivery (COD)</option>
                        </select>
                        @error('payment_method')
                            <p class="text-danger">Pilih metode pembayaran terlebih dahulu!</p>
                        @enderror
                    </div>
                </div>
                <div class="border px-4 py-3 rounded">
                    <h5>Rincian Belanja</h5>
                    <hr class="my-3">
                    <div class="mb-3">
                        @if (session()->has('voucher_found'))
                            <div class="alert alert-success alert-dismissible fade show my-3 py-2 px-3" role="alert">
                                {{ session('voucher_found') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                        @elseif (session()->has('voucher_notfound'))
                            <div class="alert alert-danger alert-dismissible fade show my-3 py-2 px-3" role="alert">
                                {{ session('voucher_notfound') }}
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                    aria-label="Close"></button>
                            </div>
                        @endif
                        <div class="d-flex">
                            <h6 class="me-3">Masukkan kode promo?</h6>
                            <a href="#" class="text-primary" data-bs-toggle="modal" data-bs-target="#allVoucherModal">Voucher yang tersedia</a>
                        </div>
                        <form>
                            <div class="input-group">
                                <input type="text" class="form-control" style="height: 40px"
                                    placeholder="Masukkan kode..." wire:model="voucher_code"
                                    aria-describedby="button-addon2">
                                <button wire:click="addVoucherDiscount" class="btn btn-primary" style="height: 40px"
                                    type="button" id="button-addon2">Gunakan</button>
                            </div>
                        </form>
                    </div>
                    <div class="detail-cost">
                        <h6>Total biaya</h6>
                        <div class="d-flex justify-content-between" style="margin-bottom: 0px">
                            <p>Subtotal</p>
                            <p>Rp. @rupiah($subtotal)</p>
                        </div>
                        <div class="d-flex justify-content-between" style="margin-bottom: 0px">
                            <p>Biaya pengiriman</p>
                            <p>+ Rp. @rupiah($shipping_price)</p>
                        </div>
                        <div class="d-flex justify-content-between" style="margin-bottom: 0px">
                            <p>Diskon</p>
                            <p>- Rp. @rupiah($discount)</p>
                        </div>
                        <div class="d-flex justify-content-between" style="margin-bottom: 0px">
                            <p>Potongan kupon</p>
                            @if ($voucher_type == 'Persentase')
                                <p>- @rupiah($voucher_price)%</p>
                            @elseif ($voucher_type == 'Potongan Tetap')
                                <p>- Rp. @rupiah($voucher_price)</p>
                            @else
                                <p>- Rp. 0</p>
                            @endif
                        </div>
                    </div>

                    <hr>

                    <div class="d-flex justify-content-between">
                        <p>Total</p>
                        @if ($voucher_type == 'Persentase')
                            <p>Rp. @rupiah($subtotal + $shipping_price - ($subtotal * $voucher_price) / 100 - $discount)</p>
                            @php
                                $this->final_total = $subtotal + $shipping_price - ($subtotal * $voucher_price) / 100 - $discount;
                            @endphp
                        @elseif ($voucher_type == 'Potongan Tetap')
                            <p>Rp. @rupiah($subtotal + $shipping_price - $voucher_price - $discount)</p>
                            @php
                                $this->final_total = $subtotal + $shipping_price - $voucher_price - $discount;
                            @endphp
                        @else
                            <p>Rp. @rupiah($subtotal + $shipping_price - $discount)</p>
                            @php
                                $this->final_total = $subtotal + $shipping_price - $discount;
                            @endphp
                        @endif
                    </div>
                    <button wire:click="checkoutOrder" class="btn btn-primary btn-checkout mt-4">Checkout
                        Pesanan</button>
                </div>
            </div>
        </div>
    </div>

    @include('partials.modal-checkout')

    @component('components.home-footer')
        @slot('categories')
            @foreach ($categories as $category)
                <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                        class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
                </li>
            @endforeach
        @endslot
    @endcomponent
</div>

@push('js')
    <script>
        window.livewire.on('addressAdded', () => {
            $("#createAddressModal").modal('hide')
        })
        window.livewire.on('updatedActiveAddress', () => {
            $("#allAddressModal").modal('hide')
        })
        window.livewire.on('addressUpdated', () => {
            $("#editAddressModal").modal('hide')
        })
        window.livewire.on('voucherAdded', () => {
            $("#allVoucherModal").modal('hide')
        })
    </script>
@endpush
