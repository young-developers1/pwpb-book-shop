<div>
    @component('components.home-navbar')
        @slot('search')
            {{ $search }}
        @endslot
        @slot('count_cart')
            {{ $count_cart }}
        @endslot
    @endcomponent
    <div class="container my-container" id="all-books-page">
        <div class="row">
            <div class="col-lg-4 col-12 pe-lg-5">
                <div class="position-lg-fixed position-relative">
                    <div class="d-flex justify-content-between">
                        <h3>Filter</h3>
                        <button class="btn btn-primary" wire:click="resetFilter">Reset</button>
                    </div>
                    <div class="my-3">
                        <h5 class="text-primary">Kategori</h5>
                        @foreach ($categories as $category)
                            <div class="mb-2">
                                <input type="radio" name="category" id="{{ $category->category_name }}"
                                    wire:model="category" value="{{ $category->id }}">
                                <label for="{{ $category->category_name }}">{{ $category->category_name }}</label>
                            </div>
                        @endforeach
                    </div>
                    <div class="my-3">
                        <h5 class="text-primary">Harga</h5>
                        <div class="mb-2">
                            <label for="">Minimum</label>
                            <div class="position-relative filter-price">
                                <p>Rp.</p>
                                <input type="number" class="text-end" wire:model="minprice" placeholder="0">
                            </div>
                        </div>
                        <div class="mb-2">
                            <label for="">Maximum</label>
                            <div class="position-relative filter-price">
                                <p>Rp.</p>
                                <input type="number" class="text-end" wire:model="maxprice" placeholder="1.000.000">
                            </div>
                        </div>
                    </div>
                    <div class="my-3">
                        <h5 class="text-primary">Filter berdasarkan stok</h5>
                        <div class="">
                            <input type="radio" id="all" value="all" wire:model="filter_stock"
                                class="me-2">
                            <label for="all">Semua</label>
                        </div>
                        <div class="">
                            <input type="radio" id="available" value="available" wire:model="filter_stock"
                                class="me-2">
                            <label for="available">Tersedia</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-12">
                <div class="d-flex justify-content-end">
                    <form action="">
                        <select class="form-select" style="width: 150px" wire:model="filter">
                            <option value="latest">Terbaru</option>
                            <option value="oldest">Terlama</option>
                            <option value="bestseller">Terlaris</option>
                        </select>
                    </form>
                </div>
                <div class="mt-3 row g-3">
                    @if ($books->count() > 0)
                        @foreach ($books as $book)
                            <div class="col-xl-4 col-lg-4 col-sm-6">
                                <a
                                    href="{{ route('book', [strtolower($book->category->category_name), $book->slug]) }}">
                                    <div class="card" style="height: 400px">
                                        <div style="height: 250px; overflow: hidden">
                                            @if ($book->thumbnail_img)
                                                <img src="{{ asset('storage/' . $book->thumbnail_img) }}"
                                                    class="card-img-top" alt="Buku {{ $book->title }}">
                                            @else
                                                <img src="{{ asset('assets/img/image-not-available.jpg') }}"
                                                    class="card-img-top" alt="Buku {{ $book->title }}">
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <p class="text-muted category-text">{{ $book->category->category_name }}
                                            </p>
                                            <h5>{{ $book->title }}</h5>
                                            <div class="text-dark">
                                                @if ($book->discount > 0)
                                                    <p class="mb-0 text-primary font-weight-bold fs-5">Rp.
                                                        @rupiah(($book->price * (100 - $book->discount)) / 100)
                                                    </p>
                                                    <p class="fs-6"><s>Rp. @rupiah($book->price)</s></p>
                                                @else
                                                    <p class="mb-0 text-primary font-weight-bold fs-5">Rp.
                                                        @rupiah($book->price)
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        <div class="d-flex justify-content-center">
                            <div>
                                <img src="{{ asset('assets/img/book_notfound.svg') }}" width="500" alt="">
                                @if ($search)
                                    <p class="text-center mt-3">Buku dengan pencarian <span class="text-primary">"{{ $search }}"</span> tidak ditemukan</p>
                                @else
                                    <p class="text-center mt-3">Buku tidak ditemukan</p>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>

                <div class="d-flex justify-content-center mt-4">
                    {{ $books->links() }}
                </div>
            </div>
        </div>
    </div>
    @component('components.home-footer')
        @slot('categories')
            @foreach ($categories as $category)
                <li class="nav-item mb-2"><a href="/books?category={{ $category->id }}"
                        class="nav-link p-0 text-muted">{{ $category->category_name }}</a>
                </li>
            @endforeach
        @endslot
    @endcomponent
</div>
