<div>
    <div class="d-flex justify-content-between">
        <h3>Daftar Voucher</h3>
        <form action="">
            <input type="text" class="form-control" wire:model="query" placeholder="Cari voucher...">
        </form>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @elseif (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
            {{ session('error') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="card my-4">
        <div class="card-header p-4 pb-2">
            <div class="d-flex flex-row justify-content-between mb-2">
                <div>
                    <h5>Tabel Data Voucher</h5>
                </div>
                <a href="#" class="btn btn-create text-white btn-sm mb-0" data-bs-toggle="modal"
                    data-bs-target="#createVoucherModal">+&nbsp; Tambah
                    voucher</a>
            </div>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                No
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">
                                Nama
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Kode Voucher
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Tipe
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Nominal
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Tanggal Berlaku
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Status
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($vouchers->count() > 0)
                            @foreach ($vouchers as $voucher)
                                <tr>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $voucher->voucher_name }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $voucher->voucher_code }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $voucher->type }}
                                        </p>
                                    </td>
                                    <td>
                                        @if ($voucher->type === 'Persentase')
                                            <p class="text-sm font-weight-bold mb-0">{{ $voucher->nominal }}%
                                            </p>
                                        @elseif ($voucher->type === 'Potongan Tetap')
                                            <p class="text-sm font-weight-bold mb-0">Rp @rupiah($voucher->nominal)
                                            </p>
                                        @endif
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">
                                            {{ $voucher->effective_date_start->format('d M Y') }} - {{ $voucher->effective_date_end->format('d M Y') }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ ucfirst($voucher->status) }}
                                        </p>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn btn-warning me-2 "
                                            wire:click.prevent="edit({{ $voucher->id }})" data-bs-toggle="modal"
                                            data-bs-target="#editVoucherModal"><i
                                                class="fa-solid fa-pen-to-square"></i></a>
                                        <a href="#" class="btn btn-danger"
                                            wire:click.prevent="delete({{ $voucher->id }})" data-bs-toggle="modal"
                                            data-bs-target="#deleteVoucherModal"><i class="fa-solid fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td class="text-center text-md font-weight-bold" colspan="8">Data voucher tidak
                                ditemukan</td>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('partials.modal-voucher')
</div>

@push('js')
    <script>
        window.livewire.on('voucherCreated', () => {
            $('#createVoucherModal').modal('hide')
        })

        window.livewire.on('voucherUpdated', () => {
            $('#editVoucherModal').modal('hide')
        })

        window.livewire.on('voucherDeleted', () => {
            $('#deleteVoucherModal').modal('hide')
        })
    </script>
@endpush
