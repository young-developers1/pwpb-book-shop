<div>
    <div class="d-flex justify-content-between">
        <h3>Laporan Pesanan</h3>
        <form action="">
            <input type="text" class="form-control" wire:model="query" placeholder="Cari pesanan...">
        </form>
    </div>

    <div class="card my-4">
        <div class="card-header p-4 pb-2">
            <div class="d-flex flex-row justify-content-between mb-2">
                <div>
                    <h5>Tabel Buku Favorit</h5>
                </div>
                <form action="">
                    <select class="form-select pe-5" wire:model="filterFavoriteBook">
                        <option value="terfavorit">Terfavorit</option>
                        <option value="terlaris">Terlaris</option>
                    </select>
                </form>
            </div>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-2">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                No
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">
                                Nama Buku
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Harga
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Terjual
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Total Pendapatan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($favorite_books->count() > 0)
                            @foreach ($favorite_books as $book)
                                <tr>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-2">{{ $loop->iteration }}</p>
                                    </td>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-2">{{ $book->book->title }}</p>
                                    </td>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-2">Rp. @rupiah($book->book->price)</p>
                                    </td>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-2">@rupiah($book->total_quantity)</p>
                                    </td>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-2">Rp. @rupiah($book->total_price)</p>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td class="text-center text-md font-weight-bold" colspan="8">Data buku tidak
                                ada</td>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card my-4">
        <div class="card-header p-4 pb-2">
            <div class="d-flex flex-row justify-content-between mb-2">
                <div>
                    <h5>Tabel Pesanan</h5>
                </div>
                <form action="">
                    <select class="form-select pe-5" wire:model="filterOrder">
                        <option value="terbaru">Terbaru</option>
                        <option value="terlama">Terlama</option>
                        <option value="terlaris">Terlaris</option>
                    </select>
                </form>
            </div>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-2">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                No
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">
                                No Order
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Nama Pemesan
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Total Harga
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Tanggal Pemesanan
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($orders->count() > 0)
                            @foreach ($orders as $order)
                                <tr>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-2">{{ $loop->iteration }}</p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-2">{{ $order->order_code }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-2">{{ $order->user->username }}</p>
                                    </td>
                                    <td>
                                        @if ($order->voucher_id != null)
                                            @if ($order->voucher->type == 'Persentase')
                                                <p class="text-sm font-weight-bold mb-2">Rp. @rupiah(($order->sub_total + $order->shipping_price) * (100 - $order->voucher->nominal) / 100)</p>
                                            @else
                                                <p class="text-sm font-weight-bold mb-2">Rp. @rupiah($order->sub_total + $order->shipping_price - $order->voucher->nominal)</p>
                                        @endif
                                        @else
                                            <p class="text-sm font-weight-bold mb-2">Rp. @rupiah($order->sub_total + $order->shipping_price)</p>
                                        @endif                                    
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-2">
                                            {{ $order->created_at->format('d M Y') }}</p>
                                    </td>
                                    <td>
                                        <a href="{{ route('order.detail', $order->id) }}" class="btn btn-info me-2"><i
                                                class="fa-solid fa-circle-info"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td class="text-center text-md font-weight-bold" colspan="8">Data pesanan tidak
                                ada</td>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
