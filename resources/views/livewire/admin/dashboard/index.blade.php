@section('css')
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.css" rel="stylesheet" />
@endsection

<div id="dashboard-page">
    <div class="content">
        <div class="row">
            <div class="col-12">
                <h3 class="content-title">Dashboard</h3>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="statistics-card">

                    <div class="d-flex align-items-center">
                        <img src="{{ asset('assets/img/delivery-time.png') }}" width="70" class="me-3 rounded"
                            alt="">
                        <div class="d-flex flex-column justify-content-between align-items-start mt-2">
                            <h5 class="content-desc">Pesanan Diproses</h5>
                            <h4 class="statistics-value">{{ $get_order_processed }} Pesanan</h4>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="statistics-card">

                    <div class="d-flex align-items-center">
                        <img src="{{ asset('assets/img/order-processed.png') }}" width="70" class="me-3 rounded"
                            alt="">
                        <div class="d-flex flex-column justify-content-between align-items-start mt-2">
                            <h5 class="content-desc">Pesanan Selesai</h5>
                            <h4 class="statistics-value">{{ $get_order_completed }} Pesanan</h4>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4">
                <div class="statistics-card">

                    <div class="d-flex align-items-center">
                        <img src="{{ asset('assets/img/income.png') }}" width="70" class="me-3 rounded"
                            alt="">
                        <div class="d-flex flex-column justify-content-between align-items-start mt-2">
                            <h5 class="content-desc">Total Pendapatan</h5>
                            <h4 class="statistics-value">Rp. @rupiah($total_incomes)</h4>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="row mt-5 g-5">
            <div class="col-12 col-lg-6">
                <h3 class="content-title">Grafik Pesanan Buku</h3>

                <div class="chart">
                    <canvas id="chart-bars" class="chart-canvas" height="350"></canvas>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <h3 class="content-title">Notifikasi Pesanan</h3>
                <div>
                    @if ($notifications->count() > 0)
                        @foreach ($notifications as $notification)
                            <a href="{{ route('notification.index') }}"
                                class="mb-2 border border-secondary px-3 pt-3 pb-2 d-flex justify-content-between align-items-center">
                                <h6>{{ $notification->order_code }}</h6>
                                <h6>{{ $notification->created_at->format('H.i') }}
                                    {{ $notification->created_at->format('H.i') > '00.00' && $notification->created_at->format('H.i') < '12.00' ? 'AM' : 'PM' }}
                                </h6>
                            </a>
                        @endforeach
                    @else
                        <div class="border border-secondary px-3 pt-3 pb-0 d-flex align-items-center">
                            <p>Belum ada pesanan masuk</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="row mt-5 g-5">
            <div class="col-12 col-lg-6">
                <h3 class="content-title">Grafik Pendapatan</h3>

                <div class="chart">
                    <canvas id="chart-lines" class="chart-canvas" height="350"></canvas>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <h3 class="content-title">Log Aktifitas</h3>

                <ul class="log-activity">
                    @if ($log_activities->count() > 0)
                        @foreach ($log_activities as $activity)
                            <li class="item d-flex">
                                @if ($activity->event == 'created')
                                    <i class="fa-solid fa-plus"></i>
                                @elseif ($activity->event == 'updated')
                                    <i class="fa-solid fa-pencil"></i>
                                @else
                                    <i class="fa-solid fa-trash"></i>
                                @endif
                                <div>
                                    <h6>
                                        <span>{{ $activity->user->username }}</span> {{ $activity->description }} <span>"{{ $activity->value }}"</span>
                                    </h6>
                                    <p>{{ $activity->created_at->diffForHumans() }}</p>
                                </div>
                            </li>
                        @endforeach
                    @else
                        <p>Belum ada aktivitas dari admin</p>
                    @endif
                </ul>
            </div>
        </div>

    </div>
</div>

@push('js')
    {{-- <script src="{{ asset('assets/js/custom-chart.js') }}"></script> --}}
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.js"></script>

    <script>
        const chartBarsCtx = document.getElementById("chart-bars").getContext("2d");
        let orderDatas = <?= json_encode($orderDatas) ?>;

        new Chart(chartBarsCtx, {
            type: "bar",
            data: {
                labels: ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu'],
                datasets: [{
                    label: "Total pesanan",
                    tension: 0.4,
                    borderWidth: 0,
                    borderRadius: 2,
                    borderSkipped: false,
                    backgroundColor: "#095D8C",
                    data: orderDatas,
                    maxBarThickness: 25
                }, ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5],
                            color: 'rgba(0,0,0,0.2)'
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 500,
                            beginAtZero: true,
                            padding: 10,
                            font: {
                                size: 14,
                                weight: 300,
                                family: "Poppins",
                                style: 'normal',
                                lineHeight: 2
                            },
                            color: "#555"
                        },
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5],
                            color: 'rgba(0,0,0,0.2)'
                        },
                        ticks: {
                            display: true,
                            color: '#555',
                            padding: 10,
                            font: {
                                size: 14,
                                weight: 300,
                                family: "Poppins",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });

        const chartLineCtx = document.getElementById("chart-lines").getContext("2d");
        let incomeDatas = <?= json_encode($incomeDatas) ?>;

        new Chart(chartLineCtx, {
            type: "line",
            data: {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Sep', 'Okt', 'Nov', 'Des'],
                datasets: [{
                    label: "Total pendapatan",
                    tension: 0.4,
                    borderWidth: 2,
                    backgroundColor: "#095D8C",
                    data: incomeDatas,
                    borderColor: "#095D8C",
                }, ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false,
                    }
                },
                interaction: {
                    intersect: false,
                    mode: 'index',
                },
                scales: {
                    y: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5],
                            color: 'rgba(0,0,0,0.2)'
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 500,
                            beginAtZero: true,
                            padding: 10,
                            font: {
                                size: 14,
                                weight: 300,
                                family: "Poppins",
                                style: 'normal',
                                lineHeight: 2
                            },
                            color: "#555"
                        },
                    },
                    x: {
                        grid: {
                            drawBorder: false,
                            display: true,
                            drawOnChartArea: true,
                            drawTicks: false,
                            borderDash: [5, 5],
                            color: 'rgba(0,0,0,0.2)'
                        },
                        ticks: {
                            display: true,
                            color: '#555',
                            padding: 10,
                            font: {
                                size: 14,
                                weight: 300,
                                family: "Poppins",
                                style: 'normal',
                                lineHeight: 2
                            },
                        }
                    },
                },
            },
        });
    </script>
@endpush
