<div id="notification-page">
    <h3 class="text-center my-4">Notifikasi Pesanan</h3>
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @elseif (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
            {{ session('error') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode Pesanan</th>
                <th scope="col">Nama Pemesan</th>
                <th scope="col">Total Harga</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @if ($orders->count() > 0)
                @foreach ($orders as $order)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $order->order_code }}</td>
                        <td>{{ $order->user->name }}</td>
                        <td>
                            @if ($order->voucher_id != null)
                                @if ($order->voucher->type == 'Persentase')
                                    <p>Rp. @rupiah((($order->sub_total + $order->shipping_price) * (100 - $order->voucher->nominal)) / 100)</p>
                                @elseif ($order->voucher->type == 'Potongan Tetap')
                                    <p>Rp. @rupiah($order->sub_total + $order->shipping_price - $order->voucher->nominal)</p>
                                @endif
                            @else
                                <p>Rp. @rupiah($order->sub_total + $order->shipping_price)</p>
                            @endif
                        </td>
                        <td>
                            <a href="#" wire:click.prevent="getDetailOrder({{ $order->id }})"
                                class="btn btn-success me-2" data-bs-toggle="modal"
                                data-bs-target="#detailOrderModal">Konfirmasi</a>
                            <a href="#" class="btn btn-danger" data-bs-toggle="modal"
                                data-bs-target="#deleteOrderModal"
                                wire:click.prevent="delete({{ $order->id }})">Hapus</a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5" class="text-center">Belum ada pesanan masuk</td>
                </tr>
            @endif
        </tbody>
    </table>

    @include('partials.modal-notification')
</div>

@push('js')
    <script>
        window.livewire.on('orderConfirmed', () => {
            $('#detailOrderModal').modal('hide')
        })

        window.livewire.on('orderDeleted', () => {
            $('#deleteOrderModal').modal('hide')
        })
    </script>
@endpush
