<div>
    <div class="d-flex justify-content-between">
        <h3>Daftar Pesanan</h3>
        <form action="">
            <input type="text" class="form-control" wire:model="query" placeholder="Cari pesanan...">
        </form>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @elseif (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
            {{ session('error') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
            <thead>
                <tr>
                    <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                        No
                    </th>
                    <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">
                        No Order
                    </th>
                    <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                        Nama Pemesan
                    </th>
                    <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                        Total Harga
                    </th>
                    <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                        Status
                    </th>
                    <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                        Aksi
                    </th>
                </tr>
            </thead>
            <tbody>
                @if ($orders->count() > 0)
                    @foreach ($orders as $order)
                        <tr>
                            <td class="ps-4">
                                <p class="text-sm font-weight-bold mb-0">{{ $loop->iteration }}</p>
                            </td>
                            <td>
                                <p class="text-sm font-weight-bold mb-0">{{ $order->order_code }}
                                </p>
                            </td>
                            <td>
                                <p class="text-sm font-weight-bold mb-0">{{ $order->user_address->receipent_name }}</p>
                            </td>
                            <td>
                                <p class="text-sm font-weight-bold mb-0">
                                @if ($order->voucher_id != null)
                                    @if ($order->voucher->type == 'Persentase')
                                        Rp. @rupiah((($order->sub_total + $order->shipping_price) * (100 - $order->voucher->nominal)) / 100)
                                    @elseif ($order->voucher->type == 'Potongan Tetap')
                                        Rp. @rupiah($order->sub_total + $order->shipping_price - $order->voucher->nominal)
                                    @endif
                                @else
                                    Rp. @rupiah($order->sub_total + $order->shipping_price)
                                @endif
                                </p>
                            </td>
                            <td>
                                <p class="text-sm font-weight-bold mb-0">{{ $order->status }}</p>
                            </td>
                            <td>
                                <a href="{{ route('order.detail', $order->id) }}" class="btn btn-info me-2"><i
                                        class="fa-solid fa-circle-info"></i></a>
                                <a href="#" class="btn btn-warning me-2"
                                    wire:click.prevent="edit({{ $order->id }})" data-bs-toggle="modal"
                                    data-bs-target="#editOrderModal"><i class="fa-solid fa-pen-to-square"></i></a>
                                <a href="#" class="btn btn-danger"
                                    wire:click.prevent="delete({{ $order->id }})" data-bs-toggle="modal"
                                    data-bs-target="#deleteOrderModal"><i class="fa-solid fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <td class="text-center text-md font-weight-bold" colspan="8">Data pesanan tidak
                        ada</td>
                @endif
            </tbody>
        </table>
    </div>

    @include('partials.modal-order')
</div>

@push('js')
    <script>
        window.livewire.on('orderUpdated', () => {
            $('#editOrderModal').modal('hide')
        })

        window.livewire.on('orderDeleted', () => {
            $('#deleteOrderModal').modal('hide')
        })
    </script>
@endpush
