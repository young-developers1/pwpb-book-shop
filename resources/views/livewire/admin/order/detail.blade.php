<div id="detail-order-page">
    <h3>Pesanan Nomor <span class="text-danger">#{{ $order->order_code }}</span></h3>
    <div class="row row-cols-lg-2 row-cols-1 g-4 mt-2">
        <div>
            <div class="p-4 rounded shadow">
                <h4 class="mb-4">Informasi Pengguna</h4>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Nama</h6>
                    <p>{{ $order->user->name }}</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>No Telp</h6>
                    <p>{{ $order->user_address->phone }}</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Email</h6>
                    <p>{{ $order->user->email }}</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Note</h6>
                    <p>{{ $order->note ?? 'N/A' }}</p>
                </div>
            </div>

            <div class="px-4 py-3 rounded shadow">
                <h4 class="mb-3">Informasi Alamat</h4>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Detail</h6>
                    <p>{{ $order->user_address->district . ', ' . $order->user_address->regency . ', ' . $order->user_address->province }}
                    </p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Kode Pos</h6>
                    <p>{{ $order->user_address->pos_code }}</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Label</h6>
                    <p>{{ $order->user_address->label }}</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Detail Alamat</h6>
                    <p>{{ $order->user_address->address }}</p>
                </div>
            </div>
        </div>

        <div>
            <div class="p-4 shadow rounded">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Items Summary</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order->order_items as $item)
                            <tr>
                                <td>
                                    <div class="d-flex">
                                        @if ($item->book->thumbnail_img)
                                            <img src="{{ asset('storage/' . $item->book->thumbnail_img) }}"
                                                class="me-2" alt="Buku {{ $item->title }}" width="30"
                                                height="100%">
                                            <p>{{ $item->book->title }}</p>
                                        @else
                                            <img src="{{ asset('assets/img/image-not-available.jpg') }}" class="me-2"
                                                alt="Buku" width="30" height="100%">
                                            <p>{{ $item->book->title }}</p>
                                        @endif
                                    </div>
                                </td>
                                <td>{{ $item->quantity }}</td>
                                <td>
                                    <p>Rp. @rupiah($item->price)</p>
                                </td>
                                <td>Rp. @rupiah($item->quantity * $item->price)</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="p-4 shadow rounded">
                <div class="d-flex justify-content-between mb-3">
                    <h4>Order Summary</h4>
                    @if ($order->status === 'Pesanan selesai')
                        <p class="order-completed px-3 py-2 rounded font-weight-bold" style="font-size: 0.9em">
                            {{ $order->status }}
                        </p>
                    @elseif ($order->status === 'Pesanan dibatalkan')
                        <p class="px-3 py-2 rounded font-weight-bold order-cancelled" style="font-size: 0.9em">
                            {{ $order->status }}
                        </p>
                    @else
                        <p class="px-3 py-2 rounded font-weight-bold order-processed" style="font-size: 0.9em">
                            {{ $order->status }}
                        </p>
                    @endif
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Tanggal Pemesanan</h6>
                    <p>{{ $order->created_at->format('d M Y') }}</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Subtotal</h6>
                    <p>Rp. @rupiah($order->sub_total)</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Biaya Pengiriman</h6>
                    <p>+ Rp. @rupiah($order->shipping_price)</p>
                </div>
                <div class="d-flex justify-content-between mb-2">
                    <h6>Potongan Kupon</h6>
                    @if ($order->voucher_id != null)
                        @if ($order->voucher->type == 'Persentase')
                            <p>- {{ $order->voucher->nominal }}%</p>
                        @else
                            <p>- Rp. @rupiah($order->voucher->nominal)</p>
                        @endif
                    @else
                        <p>- Rp. 0</p>
                    @endif
                </div>
                <hr>
                <div class="d-flex justify-content-between">
                    <h6>Total</h6>
                    @if ($order->voucher_id != null)
                        @if ($order->voucher->type == 'Persentase')
                            <p>Rp. @rupiah((($order->sub_total + $order->shipping_price) * (100 - $order->voucher->nominal)) / 100)</p>
                        @elseif ($order->voucher->type == 'Potongan Tetap')
                            <p>Rp. @rupiah($order->sub_total + $order->shipping_price - $order->voucher->nominal)</p>
                        @endif
                    @else
                        <p>Rp. @rupiah($order->sub_total + $order->shipping_price)</p>
                    @endif
                </div>
                <div class="d-flex justify-content-between">
                    <h6>Metode Pembayaran</h6>
                    <p class="text-uppercase">{{ $order->payment_method }}</p>
                </div>
                <div class="d-flex justify-content-between">
                    <h6>Metode Pengiriman</h6>
                    <p class="text-uppercase">{{ $order->shipping_method }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
