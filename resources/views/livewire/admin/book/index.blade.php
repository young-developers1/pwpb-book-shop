<div id="book-container">
    <h3 class="text-center">Daftar Buku</h3>
    <div class="d-flex justify-content-between mb-4">
        <div class="d-flex">
            <a href="#" class="btn btn-create text-white py-2 btn-md" data-bs-toggle="modal"
                data-bs-target="#createBookModal">+&nbsp; Tambah
            </a>
            <form action="">
                <select class="ms-3 form-select py-2" wire:model="filterBook">
                    <option value="">Genre Buku</option>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                    @endforeach
                </select>
            </form>
        </div>
        <form action="">
            <input type="text" class="form-control py-2" wire:model="query" placeholder="Cari buku...">
        </form>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show my-3" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @elseif (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show my-3" role="alert">
            {{ session('error') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    {{-- <div class="card my-4">
        <div class="card-header p-4 pb-2">
            <div class="d-flex flex-row justify-content-between mb-2">
                <div>
                    <h5>Tabel Data Buku</h5>
                </div>
                <a href="#" class="btn btn-create text-white btn-sm mb-0" data-bs-toggle="modal"
                    data-bs-target="#createBookModal">+&nbsp; Tambah
                    Buku</a>
            </div>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                No
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">
                                Nama Buku
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Kategori
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Penulis
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Stok
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Harga Jual
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Status
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($books->count() > 0)
                            @foreach ($books as $book)
                                <tr>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $book->title }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $book->category->category_name }}
                                        </p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $book->writer }}</p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $book->stock }}</p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">
                                            Rp @rupiah(($book->price * (100 - $book->discount)) / 100)</p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $book->status }}</p>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-edit"
                                            wire:click.prevent="edit({{ $book->id }})" data-bs-toggle="modal"
                                            data-bs-target="#editBookModal"><i
                                                class="fa-solid fa-pen-to-square"></i></a>
                                        <a href="#" wire:click.prevent="delete({{ $book->id }})"
                                            data-bs-toggle="modal" data-bs-target="#deleteBookModal"
                                            class="btn btn-delete"><i class="fa-solid fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td class="text-center text-md font-weight-bold" colspan="8">Buku tidak
                                ditemukan</td>
                        @endif
                    </tbody>
                </table>
                <div class="d-flex justify-content-end {{ $books->count() >= 10 ? 'mt-4 me-3' : '' }}">
                    {{ $books->links('livewire::bootstrap') }}
                </div>
            </div>
        </div>
    </div> --}}

    @if ($books->count() > 0)
        <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-3 row-cols-xl-4 row-cols-xxl-5 g-3">
            @foreach ($books as $book)
                <div class="col">
                    <div class="card p-0" style="height: 350px">
                        <div class="card-img-wrapper">
                            @if ($book->thumbnail_img)
                                <img src="{{ asset('storage/' . $book->thumbnail_img) }}" class="card-img-top"
                                    alt="Buku {{ $book->title }}">
                            @else
                                <img src="{{ asset('assets/img/image-not-available.jpg') }}" class="card-img-top"
                                    alt="Buku {{ $book->title }}">
                            @endif
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title">{{ $book->title }}</h5>
                            <div class="d-flex justify-content-center align-items-center">
                                <div class="mt-2">
                                    <a href="#" class="btn text-warning btn-edit p-1 me-3 border-0"
                                        wire:click.prevent="edit({{ $book->id }})" data-bs-toggle="modal"
                                        data-bs-target="#editBookModal">Edit</a>
                                    <a href="#" wire:click.prevent="delete({{ $book->id }})"
                                        data-bs-toggle="modal" data-bs-target="#deleteBookModal"
                                        class="btn text-danger btn-delete p-1 border-0">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        @if ($query)
            <p class="text-center">Buku <span class="text-primary font-weight-bold">"{{ $query }}"</span> tidak ditemukan</p>
        @else
            <p class="text-center">Buku tidak ada</p>
        @endif
    @endif

    @include('partials.modal-book')
</div>

@push('js')
    <script>
        window.livewire.on('bookCreated', () => {
            $('#createBookModal').modal('hide')
        })

        window.livewire.on('bookUpdated', () => {
            $('#editBookModal').modal('hide')
        })

        window.livewire.on('bookDeleted', () => {
            $('#deleteBookModal').modal('hide')
        })

        const switchInput = document.querySelector('.switch-input')
        const statusText = document.querySelector('.status-text')

        // if ($('.switch-input').attr('checked', true)) {
        //     // $('.status-text')
        //     console.log('true')
        // } else {
        //     console.log('false')
        // }
    </script>

    <script>
        // Drag & Drop file
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    var htmlPreview =
                        '<img width="200" src="' + e.target.result + '" />' +
                        '<p>' + input.files[0].name + '</p>';
                    var wrapperZone = $(input).parent();
                    var previewZone = $(input).parent().parent().find('.preview-zone');
                    var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

                    wrapperZone.removeClass('dragover');
                    previewZone.removeClass('hidden');
                    boxZone.empty();
                    boxZone.append(htmlPreview);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function reset(e) {
            e.wrap('<form>').closest('form').get(0).reset();
            e.unwrap();
        }
        $(".dropzone").change(function() {
            readFile(this);
        });
        $('.dropzone-wrapper').on('dragover', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).addClass('dragover');
        });
        $('.dropzone-wrapper').on('dragleave', function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).removeClass('dragover');
        });
        $('.remove-preview').on('click', function() {
            var boxZone = $(this).parents('.preview-zone').find('.box-body');
            var previewZone = $(this).parents('.preview-zone');
            var dropzone = $(this).parents('.form-group').find('.dropzone');
            boxZone.empty();
            previewZone.addClass('hidden');
            reset(dropzone);
        });

        // Multiple file upload
        let imgUpload = document.getElementById('upload_imgs'),
            imgPreview = document.getElementById('img_preview'),
            imgTitleText = document.getElementById('preview_title_text'),
            totalFiles, previewTitle, previewTitleText, img, closeBtn;

        imgUpload.addEventListener('change', previewImgs, false);

        function previewImgs(event) {

            totalFiles = imgUpload.files.length;

            if (!!totalFiles) {
                imgPreview.classList.remove('quote-imgs-thumbs--hidden');
                imgTitleText.style.display = 'none'

                previewTitle = document.createElement('p');
                previewTitle.style.marginTop = '10px';
                previewTitle.style.fontWeight = '500';
                previewTitleText = document.createTextNode(totalFiles + ' Total Images Selected');
                previewTitle.appendChild(previewTitleText);
                imgPreview.appendChild(previewTitle);
            }

            for (var i = 0; i < totalFiles; i++) {
                img = document.createElement('img');
                img.src = URL.createObjectURL(event.target.files[i]);
                img.classList.add('img-preview-thumb');
                imgPreview.appendChild(img);
            }
        }

        // Multiple file upload (Edit)
        let editImgUpload = document.getElementById('edit_upload_imgs'),
            editImgPreview = document.getElementById('edit_img_preview'),
            editImgTitleText = document.getElementById('edit_preview_title_text'),
            editTotalFiles, editPreviewTitle, editPreviewTitleText, editImg, editCloseBtn;

        editImgUpload.addEventListener('change', editPreviewImgs, false);

        function editPreviewImgs(event) {

            editTotalFiles = editImgUpload.files.length;

            if (!!editTotalFiles) {
                editImgPreview.classList.remove('quote-imgs-thumbs--hidden');
                editImgTitleText.style.display = 'none'

                editPreviewTitle = document.createElement('p');
                editPreviewTitle.style.marginTop = '10px';
                editPreviewTitle.style.fontWeight = '500';
                editPreviewTitleText = document.createTextNode(editTotalFiles + ' Total Images Selected');
                editPreviewTitle.appendChild(editPreviewTitleText);
                editImgPreview.appendChild(editPreviewTitle);
            }

            for (var i = 0; i < editTotalFiles; i++) {
                editImg = document.createElement('img');
                editImg.src = URL.createObjectURL(event.target.files[i]);
                editImg.classList.add('img-preview-thumb');
                editImgPreview.appendChild(editImg);
            }
        }
    </script>
@endpush
