<div>
    <div class="d-flex justify-content-between">
        <h3>Daftar Akun Admin</h3>
        <form action="">
            <input type="text" class="form-control" wire:model="query" placeholder="Cari akun admin...">
        </form>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @elseif (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
            {{ session('error') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="card my-4">
        <div class="card-header p-4 pb-2">
            <div class="d-flex flex-row justify-content-between mb-2">
                <div>
                    <h5>Tabel Data Kategori</h5>
                </div>
                <a href="#" class="btn btn-create text-white btn-sm mb-0" data-bs-toggle="modal"
                    data-bs-target="#createAccountModal">+&nbsp; Tambah
                    akun</a>
            </div>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                No
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">
                                Nama
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Username
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Email
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($admins->count() > 0)
                    @foreach ($admins as $admin)
                        <tr>
                            <td class="ps-4">
                                <p class="text-sm font-weight-bold mb-0">{{ $loop->iteration }}</p>
                            </td>
                            <td>
                                <p class="text-sm font-weight-bold mb-0">{{ $admin->name }}
                                </p>
                            </td>
                            <td>
                                <p class="text-sm font-weight-bold mb-0">{{ $admin->username }}</p>
                            </td>
                            <td>
                                <p class="text-sm font-weight-bold mb-0">
                                    {{ $admin->email }}</p>
                            </td>
                            <td>
                                <a href="#" class="btn btn-warning me-2" data-bs-toggle="modal" data-bs-target="#editAccountModal" wire:click.prevent="edit({{ $admin->id }})"><i
                                        class="fa-solid fa-pen-to-square"></i></a>
                                {{-- <a href="#" class="btn btn-warning"><i class="fa-solid fa-pen-to-square"></i></a> --}}
                                <a href="#" class="btn btn-danger"
                                    wire:click.prevent="delete({{ $admin->id }})" data-bs-toggle="modal"
                                    data-bs-target="#deleteAccountModal"><i class="fa-solid fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <td class="text-center text-md font-weight-bold" colspan="8">Data akun admin tidak
                        ada</td>
                @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('partials.modal-account-admin')
</div>

@push('js')
    <script>
        window.livewire.on('userCreated', () => {
            $('#createAccountModal').modal('hide')
        })

        window.livewire.on('userUpdated', () => {
            $('#editAccountModal').modal('hide')
        })

        window.livewire.on('userDeleted', () => {
            $('#deleteAccountModal').modal('hide')
        })
    </script>
@endpush