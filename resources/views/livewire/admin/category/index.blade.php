<div>
    <div class="d-flex justify-content-between">
        <h3>Daftar Kategori</h3>
        <form action="">
            <input type="text" class="form-control" wire:model="query" placeholder="Cari kategori...">
        </form>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @elseif (session()->has('error'))
        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
            {{ session('error') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="card my-4">
        <div class="card-header p-4 pb-2">
            <div class="d-flex flex-row justify-content-between mb-2">
                <div>
                    <h5>Tabel Data Kategori</h5>
                </div>
                <a href="#" class="btn btn-create text-white btn-sm mb-0" data-bs-toggle="modal"
                    data-bs-target="#createCategoryModal">+&nbsp; Tambah
                    kategori</a>
            </div>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
            <div class="table-responsive p-0">
                <table class="table align-items-center mb-0">
                    <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                No
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7 ps-2">
                                Nama Kategori
                            </th>
                            <th class="text-uppercase text-secondary text-sm font-weight-bolder opacity-7">
                                Aksi
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($categories->count() > 0)
                            @foreach ($categories as $category)
                                <tr>
                                    <td class="ps-4">
                                        <p class="text-sm font-weight-bold mb-0">{{ $loop->iteration }}</p>
                                    </td>
                                    <td>
                                        <p class="text-sm font-weight-bold mb-0">{{ $category->category_name }}
                                        </p>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn btn-warning me-2"
                                            wire:click.prevent="edit({{ $category->id }})" data-bs-toggle="modal"
                                            data-bs-target="#editCategoryModal"><i
                                                class="fa-solid fa-pen-to-square"></i></a>
                                        <a href="#" class="btn btn-danger"
                                            wire:click.prevent="delete({{ $category->id }})" data-bs-toggle="modal"
                                            data-bs-target="#deleteCategoryModal"><i class="fa-solid fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td class="text-center text-md font-weight-bold" colspan="8">Data kategori tidak
                                ditemukan</td>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('partials.modal-category')
</div>

@push('js')
    <script>
        window.livewire.on('categoryCreated', () => {
            $('#createCategoryModal').modal('hide')
        })

        window.livewire.on('categoryUpdated', () => {
            $('#editCategoryModal').modal('hide')
        })

        window.livewire.on('categoryDeleted', () => {
            $('#deleteCategoryModal').modal('hide')
        })
    </script>
@endpush
