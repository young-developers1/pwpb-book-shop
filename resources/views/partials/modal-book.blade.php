{{-- Create Book Modal --}}
@component('components.modal')
    @slot('modalId')
        createBookModal
    @endslot
    @slot('modalTitle')
        Create Book
    @endslot
    @slot('modalBody')
        <div class="me-5">

            <div class="form-group">
                <label class="control-label fw-bold">Upload Thumbnail Image</label>
                <div class="preview-zone hidden">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <div>Preview</div>
                            {{-- <div class="box-tools pull-right">
                                <button type="button" class="btn btn-danger btn-xs remove-preview">
                                    <i class="fa fa-times"></i> Reset This Form
                                </button>
                            </div> --}}
                        </div>
                        <div class="box-body" wire:ignore></div>
                    </div>
                </div>
                <div class="dropzone-wrapper">
                    <div class="dropzone-desc">
                        <i class="glyphicon glyphicon-download-alt"></i>
                        <p>Choose an image file or drag it here.</p>
                    </div>
                    <input type="file" wire:model="thumbnail_img" name="img_logo" class="dropzone">
                </div>

                @error('thumbnail_img')
                    <p class="invalid-feedback block">{{ $message }}</p>
                @enderror
            </div>

            <div class="mt-5">
                <div class="d-flex align-items-center justify-content-between">
                    <h6 class="fw-bold">Upload More Images</h6>
                    <label for="upload_imgs" class="button hollow">Select Your Images +</label>
                </div>
                <input class="show-for-sr" wire:model="book_images" type="file" id="upload_imgs" hidden multiple />
                </p>
                <div class="quote-imgs-thumbs" wire:ignore id="img_preview" aria-live="polite">
                    <p id="preview_title_text">0 Total Images Selected</p>
                </div>
            </div>
        </div>
        <div class="row row-cols-2 mt-5">
            <div class="col mb-3">
                <label for="title" class="h6 mt-2 me-3">Judul</label>
                <div>
                    <input type="text" wire:model="title" id="title"
                        class="form-control @error('title') is-invalid @enderror" placeholder="Judul buku...">
                    @error('title')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="genre_id" class="h6 mt-2 me-3">Kategori</label>
                <div>
                    <select class="form-select @error('category_id') is-invalid @enderror" wire:model="category_id">
                        <option value="">Pilih Kategori</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="writer" class="h6 mt-2 me-3">Penulis</label>
                <div>
                    <input type="text" wire:model="writer" id="writer"
                        class="form-control @error('writer') is-invalid @enderror" placeholder="Penulis buku...">
                    @error('writer')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="publisher" class="h6 mt-2 me-3">Penerbit</label>
                <div>
                    <input type="text" wire:model="publisher" id="publisher"
                        class="form-control @error('publisher') is-invalid @enderror" placeholder="Penerbit buku...">
                    @error('publisher')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="weight" class="h6 mt-2 me-3">Berat</label>
                <div>
                    <input type="text" wire:model="weight" id="weight"
                        class="form-control @error('weight') is-invalid @enderror" placeholder="Berat buku (gram)...">
                    @error('weight')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="total_page" class="h6 mt-2 me-3">Halaman</label>
                <div>
                    <input type="number" wire:model="total_page" id="total_page"
                        class="form-control @error('total_page') is-invalid @enderror" placeholder="Total halaman buku...">
                    @error('total_page')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="issue_date" class="h6 mt-2 me-3">Tanggal Terbit</label>
                <div>
                    <input type="date" wire:model="issue_date" id="issue_date"
                        class="form-control @error('issue_date') is-invalid @enderror" placeholder="Tanggal terbit buku...">
                    @error('issue_date')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="language" class="h6 mt-2 me-3">Bahasa</label>
                <div>
                    <input type="text" wire:model="language" id="language"
                        class="form-control @error('language') is-invalid @enderror" placeholder="Bahasa buku...">
                    @error('language')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="isbn" class="h6 mt-2 me-3">ISBN</label>
                <div>
                    <input type="text" wire:model="isbn" id="isbn"
                        class="form-control @error('isbn') is-invalid @enderror" placeholder="ISBN buku...">
                    @error('isbn')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="stock" class="h6 mt-2 me-3">Stok</label>
                <div>
                    <input type="number" wire:model="stock" id="stock"
                        class="form-control @error('stock') is-invalid @enderror" placeholder="Stok buku...">
                    @error('stock')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="price" class="h6 mt-2 me-3">Harga</label>
                <div>
                    <input type="number" wire:model="price" id="price"
                        class="form-control @error('price') is-invalid @enderror" placeholder="Harga buku...">
                    @error('price')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="discount" class="h6 mt-2 me-3">Diskon (%)</label>
                <div>
                    <input type="number" wire:model="discount" id="discount"
                        class="form-control discount-input @error('discount') is-invalid @enderror"
                        placeholder="Diskon buku (%)...">
                    @error('discount')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col-12 mb-3">
                <label for="description" class="h6 mt-2 me-3">Deskripsi</label>
                <textarea id="description" wire:model="description" class="form-control @error('description') is-invalid @enderror"
                    cols="30" rows="10" placeholder="Deskripsi.."></textarea>
                @error('description')
                    <p class="invalid-feedback">{{ $message }}</p>
                @enderror
            </div>

            <div class="col mb-3 d-flex" wire:ignore>
                <input class="switch-input" type="checkbox" wire:model="status" hidden="hidden"
                    checked="{{ $status ? 'checked' : '' }}" id="status">
                <label class="switch" for="status"></label>

                <label for="status" class="ms-2 text-secondary">Status? <span class="status-text">active</span></label>
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="store" class="btn btn-submit">Create New Book</button>
    @endslot
@endcomponent


{{-- Edit Book Modal --}}
@component('components.modal')
    @slot('modalId')
        editBookModal
    @endslot
    @slot('modalTitle')
        Edit Book
    @endslot
    @slot('modalBody')
        <div class="me-5">

            <div class="form-group">
                <label class="control-label fw-bold d-block">Upload Thumbnail Image</label>
                <img src="{{ asset('storage/' . $this->edit_thumbnail_img) }}" id="edit_thumbnail_img" width="200"
                    alt="">
                <div class="preview-zone hidden">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <div>Preview</div>
                            {{-- <div class="box-tools pull-right">
                                <button type="button" class="btn btn-danger btn-xs remove-preview">
                                    <i class="fa fa-times"></i> Reset This Form
                                </button>
                            </div> --}}
                        </div>
                        <div class="box-body" wire:ignore></div>
                    </div>
                </div>
                <div class="dropzone-wrapper">
                    <div class="dropzone-desc">
                        <i class="glyphicon glyphicon-download-alt"></i>
                        <p>Choose an image file or drag it here.</p>
                    </div>
                    <input type="file" wire:model="thumbnail_img" name="img_logo" class="dropzone">
                </div>

                @error('thumbnail_img')
                    <p class="invalid-feedback block">{{ $message }}</p>
                @enderror
            </div>

            <div class="mt-5">
                <div class="d-flex align-items-center justify-content-between">
                    <h6 class="fw-bold">Upload More Images</h6>
                    <label for="edit_upload_imgs" class="button hollow">Select Your Images +</label>
                </div>
                <input class="show-for-sr" wire:model="book_images" type="file" id="edit_upload_imgs" hidden multiple />
                <div class="d-flex me-3 edit_sub_images_book">
                    @foreach ($edit_book_images as $image)
                        <div class="item">
                            <img src="{{ asset('storage/' . $image->image) }}" alt="" class="rounded edit_sub_image"
                                width="100">
                            <i class="fa-solid fa-xmark" wire:click.prevent="deleteSubImageBook({{ $image->id }})"></i>
                        </div>
                    @endforeach
                </div>
                <div class="quote-imgs-thumbs" wire:ignore id="edit_img_preview" aria-live="polite">
                    <p id="edit_preview_title_text">0 Total Images Selected</p>
                </div>
            </div>
        </div>
        <div class="row row-cols-2 mt-5">
            <div class="col mb-3">
                <label for="title" class="h6 mt-2 me-3">Judul</label>
                <div>
                    <input type="text" wire:model="title" id="title"
                        class="form-control @error('title') is-invalid @enderror" placeholder="Judul buku...">
                    @error('title')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="genre_id" class="h6 mt-2 me-3">Kategori</label>
                <div>
                    <select class="form-select @error('category_id') is-invalid @enderror" wire:model="category_id">
                        <option value="">Pilih Kategori</option>
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="writer" class="h6 mt-2 me-3">Penulis</label>
                <div>
                    <input type="text" wire:model="writer" id="writer"
                        class="form-control @error('writer') is-invalid @enderror" placeholder="Penulis buku...">
                    @error('writer')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="publisher" class="h6 mt-2 me-3">Penerbit</label>
                <div>
                    <input type="text" wire:model="publisher" id="publisher"
                        class="form-control @error('publisher') is-invalid @enderror" placeholder="Penerbit buku...">
                    @error('publisher')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="weight" class="h6 mt-2 me-3">Berat</label>
                <div>
                    <input type="text" wire:model="weight" id="weight"
                        class="form-control @error('weight') is-invalid @enderror" placeholder="Berat buku (gram)...">
                    @error('weight')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="total_page" class="h6 mt-2 me-3">Halaman</label>
                <div>
                    <input type="number" wire:model="total_page" id="total_page"
                        class="form-control @error('total_page') is-invalid @enderror" placeholder="Total halaman buku...">
                    @error('total_page')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="issue_date" class="h6 mt-2 me-3">Tanggal Terbit</label>
                <div>
                    <input type="date" wire:model="issue_date" id="issue_date"
                        class="form-control @error('issue_date') is-invalid @enderror" placeholder="Tanggal terbit buku...">
                    @error('issue_date')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="language" class="h6 mt-2 me-3">Bahasa</label>
                <div>
                    <input type="text" wire:model="language" id="language"
                        class="form-control @error('language') is-invalid @enderror" placeholder="Bahasa buku...">
                    @error('language')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
            <div class="col mb-3">
                <label for="isbn" class="h6 mt-2 me-3">ISBN</label>
                <div>
                    <input type="text" wire:model="isbn" id="isbn"
                        class="form-control @error('isbn') is-invalid @enderror" placeholder="ISBN buku...">
                    @error('isbn')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="stock" class="h6 mt-2 me-3">Stok</label>
                <div>
                    <input type="number" wire:model="stock" id="stock"
                        class="form-control @error('stock') is-invalid @enderror" placeholder="Stok buku...">
                    @error('stock')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="price" class="h6 mt-2 me-3">Harga</label>
                <div>
                    <input type="number" wire:model="price" id="price"
                        class="form-control @error('price') is-invalid @enderror" placeholder="Harga buku...">
                    @error('price')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col mb-3">
                <label for="discount" class="h6 mt-2 me-3">Diskon (%)</label>
                <div>
                    <input type="number" wire:model="discount" id="discount"
                        class="form-control discount-input @error('discount') is-invalid @enderror"
                        placeholder="Diskon buku (%)...">
                    @error('discount')
                        <p class="invalid-feedback">{{ $message }}</p>
                    @enderror
                </div>
            </div>

            <div class="col-12 mb-3">
                <label for="description" class="h6 mt-2 me-3">Deskripsi</label>
                <textarea id="description" wire:model="description" class="form-control @error('description') is-invalid @enderror"
                    cols="30" rows="10" placeholder="Deskripsi.."></textarea>
                @error('description')
                    <p class="invalid-feedback">{{ $message }}</p>
                @enderror
            </div>

            <div class="col mb-3 d-flex">
                <input class="switch-input" type="checkbox" wire:model="status" hidden="hidden"
                    checked="{{ $status ? 'checked' : '' }}" id="status">
                <label class="switch" for="status"></label>

                <label for="status" class="ms-2 text-secondary">Status? <span class="status-text">active</span></label>
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="update" class="btn btn-submit">Save Changes</button>
    @endslot
@endcomponent

{{-- Delete Book Modal --}}
@component('components.modal-delete')
    @slot('modalId')
        deleteBookModal
    @endslot
    @slot('label')
        buku
    @endslot
    @slot('name')
        {{ $title }}
    @endslot
@endcomponent
