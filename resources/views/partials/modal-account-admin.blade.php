{{-- Create Category Modal --}}
@component('components.modal')
    @slot('modalId')
        createAccountModal
    @endslot
    @slot('modalTitle')
        Create Account Admin
    @endslot
    @slot('modalBody')
        <div class="mb-3">
            <label for="name" class="h6 mt-2 me-3">Nama</label>
            <div>
                <input type="text" wire:model="name" id="name" class="form-control @error('name') is-invalid @enderror"
                    placeholder="Masukkan nama...">
                @error('name')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="username" class="h6 mt-2 me-3">Username</label>
            <div>
                <input type="text" wire:model="username" id="username"
                    class="form-control @error('username') is-invalid @enderror" placeholder="Masukkan username...">
                @error('username')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="email" class="h6 mt-2 me-3">Email</label>
            <div>
                <input type="email" wire:model="email" id="email"
                    class="form-control @error('email') is-invalid @enderror" placeholder="Masukkan email...">
                @error('email')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="password" class="h6 mt-2 me-3">Password</label>
            <div>
                <input type="password" wire:model="password" id="password"
                    class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan password...">
                @error('password')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="confirm_password" class="h6 mt-2 me-3">Confirm Password</label>
            <div>
                <input type="password" wire:model="confirm_password" id="confirm_password"
                    class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan konfirmasi password...">                
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="store" class="btn btn-submit">Create New Account</button>
    @endslot
@endcomponent


{{-- Edit Category Modal --}}
@component('components.modal')
    @slot('modalId')
        editAccountModal
    @endslot
    @slot('modalTitle')
        Edit Account Admin
    @endslot
    @slot('modalBody')
    <div class="mb-3">
        <label for="name" class="h6 mt-2 me-3">Nama</label>
        <div>
            <input type="text" wire:model="name" id="name" class="form-control @error('name') is-invalid @enderror"
                placeholder="Masukkan nama...">
            @error('name')
                <p class="invalid-feedback mb-0">{{ $message }}</p>
            @enderror
        </div>
    </div>
    <div class="mb-3">
        <label for="username" class="h6 mt-2 me-3">Username</label>
        <div>
            <input type="text" wire:model="username" id="username"
                class="form-control @error('username') is-invalid @enderror" placeholder="Masukkan username...">
            @error('username')
                <p class="invalid-feedback mb-0">{{ $message }}</p>
            @enderror
        </div>
    </div>
    <div class="mb-3">
        <label for="email" class="h6 mt-2 me-3">Email</label>
        <div>
            <input type="email" wire:model="email" id="email"
                class="form-control @error('email') is-invalid @enderror" placeholder="Masukkan email...">
            @error('email')
                <p class="invalid-feedback mb-0">{{ $message }}</p>
            @enderror
        </div>
    </div>
    <div class="mb-3">
        <label for="password" class="h6 mt-2 me-3">Password</label>
        <div>
            <input type="password" wire:model="password" id="password"
                class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan password...">
            @error('password')
                <p class="invalid-feedback mb-0">{{ $message }}</p>
            @enderror
        </div>
    </div>
    <div class="mb-3">
        <label for="confirm_password" class="h6 mt-2 me-3">Confirm Password</label>
        <div>
            <input type="password" wire:model="confirm_password" id="confirm_password"
                class="form-control @error('password') is-invalid @enderror" placeholder="Masukkan konfirmasi password...">                
        </div>
    </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="update" class="btn btn-submit">Save Changes</button>
    @endslot
@endcomponent

@component('components.modal-delete')
    @slot('modalId')
        deleteAccountModal
    @endslot
    @slot('label')
        akun
    @endslot
    @slot('name')
        {{ $name }}
    @endslot
@endcomponent
