{{-- Create Voucher Modal --}}
@component('components.modal')
    @slot('modalId')
        createVoucherModal
    @endslot
    @slot('modalTitle')
        Create Voucher
    @endslot
    @slot('modalSize')
        modal-md
    @endslot
    @slot('modalBody')
        <div class="mb-3">
            <label for="voucher_name" class="h6 mt-2 me-3">Nama Voucher</label>
            <div>
                <input type="text" wire:model="voucher_name" id="voucher_name"
                    class="form-control @error('voucher_name') is-invalid @enderror" placeholder="Nama voucher...">
                @error('voucher_name')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="voucher_code" class="h6 mt-2 me-3">Kode Voucher</label>
            <div>
                <input type="text" wire:model="voucher_code" id="voucher_code"
                    class="form-control @error('voucher_code') is-invalid @enderror" placeholder="Kode voucher...">
                @error('voucher_code')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="type" class="h6 mt-2 me-3">Tipe</label>
            <div>
                <select class="form-select @error('type') is-invalid @enderror" wire:model="type">
                    <option value="Persentase">Persentase</option>
                    <option value="Potongan Tetap">Potongan Tetap</option>
                </select>
                @error('type')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="nominal" class="h6 mt-2 me-3">Nominal</label>
            <div>
                <input type="number" wire:model="nominal" id="nominal"
                    class="form-control @error('nominal') is-invalid @enderror" placeholder="Nominal voucher...">
                @error('nominal')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label class="h6 mt-2 me-3">Tanggal Berlaku</label>
            <div class="row g-4">
                <div class="col-6">
                    <input type="date" wire:model="effective_date_start" id="effective_date_start"
                        class="form-control @error('effective_date_start') is-invalid @enderror">
                    @error('effective_date_start')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="col-6">
                    <input type="date" wire:model="effective_date_end" id="effective_date_end"
                        class="form-control @error('effective_date_end') is-invalid @enderror">
                    @error('effective_date_end')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label for="effective_date" class="h6 mt-2 me-3">Status</label>
            <div>
                <select wire:model="status" class="form-select">
                    <option value="public">Public</option>
                    <option value="private">Private</option>
                </select>
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="store" class="btn btn-submit">Create New Voucher</button>
    @endslot
@endcomponent


{{-- Edit Voucher Modal --}}
@component('components.modal')
    @slot('modalId')
        editVoucherModal
    @endslot
    @slot('modalTitle')
        Edit Voucher
    @endslot
    @slot('modalSize')
        modal-md
    @endslot
    @slot('modalBody')
        <div class="mb-3">
            <label for="voucher_name" class="h6 mt-2 me-3">Nama Voucher</label>
            <div>
                <input type="text" wire:model="voucher_name" id="voucher_name"
                    class="form-control @error('voucher_name') is-invalid @enderror" placeholder="Nama voucher...">
                @error('voucher_name')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="voucher_code" class="h6 mt-2 me-3">Kode Voucher</label>
            <div>
                <input type="text" wire:model="voucher_code" id="voucher_code"
                    class="form-control @error('voucher_code') is-invalid @enderror" placeholder="Kode voucher...">
                @error('voucher_code')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="type" class="h6 mt-2 me-3">Tipe</label>
            <div>
                <select class="form-select @error('type') is-invalid @enderror" wire:model="type">
                    <option value="Persentase">Persentase</option>
                    <option value="Potongan Tetap">Potongan Tetap</option>
                </select>
                @error('type')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="nominal" class="h6 mt-2 me-3">Nominal</label>
            <div>
                <input type="number" wire:model="nominal" id="nominal"
                    class="form-control @error('nominal') is-invalid @enderror" placeholder="Nominal voucher...">
                @error('nominal')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
        <div class="mb-3">
            <label for="effective_date" class="h6 mt-2 me-3">Tanggal Berlaku</label>
            <div class="row g-4">
                <div class="col-6">
                    <input type="date" wire:model="effective_date_start" id="effective_date_start"
                        class="form-control @error('effective_date_start') is-invalid @enderror">
                    @error('effective_date_start')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
                <div class="col-6">
                    <input type="date" wire:model="effective_date_end" id="effective_date_end"
                        class="form-control @error('effective_date_end') is-invalid @enderror">
                    @error('effective_date_end')
                        <p class="invalid-feedback mb-0">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>
        <div class="mb-3">
            <label for="effective_date" class="h6 mt-2 me-3">Status</label>
            <div>
                <select wire:model="status" class="form-select">
                    <option value="public">Public</option>
                    <option value="private">Private</option>
                </select>
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="update" class="btn btn-submit">Save Changes</button>
    @endslot
@endcomponent

@component('components.modal-delete')
    @slot('modalId')
        deleteVoucherModal
    @endslot
    @slot('label')
        voucher
    @endslot
    @slot('name')
        {{ $voucher_name }}
    @endslot
@endcomponent
