{{-- Detail Order Modal --}}
@component('components.modal')
    @slot('modalId')
        detailOrderModal
    @endslot
    @slot('modalTitle')
        Detail Pesanan <span class="text-danger">#{{ $order_id ? $order_code : '' }}</span>
    @endslot
    @slot('modalSize')
        modal-xl
    @endslot
    @slot('modalBody')
        @if ($order_id)
            <div class="row g-4">
                <div class="left-panel col-lg-6 col-12 pe-4">
                    <div>
                        <div class="d-flex justify-content-between mb-3">
                            <p>Nama Pemesan</p>
                            <p>{{ $name }}</p>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <p>Email</p>
                            <p>{{ $email }}</p>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <p>Nomor Hp</p>
                            <p>{{ $phone }}</p>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <p>Alamat</p>
                            <p>{{ $address }}</p>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <p>Label</p>
                            <p>{{ $label }}</p>
                        </div>
                        <div class="d-flex justify-content-between mb-3">
                            <p>Note</p>
                            <p>{{ $note ?? 'N/A' }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 ps-4">
                    <div class="items-summary pb-3 mb-3">
                        <h4 class="mb-3">Items Summary</h4>
                        @foreach ($order_items as $item)
                            <div>
                                <h6>{{ $item->book->title }}</h6>
                                <div class="d-flex justify-content-between">
                                    <p>{{ $item->quantity }} x Rp. @rupiah($item->price)</p>
                                    <p>Rp. @rupiah($item->quantity * $item->price)</p>                                    
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div>
                        <div class="d-flex justify-content-between">
                            <p>Sub total</p>
                            <p>Rp. @rupiah($sub_total)</p>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Biaya Pengiriman</p>
                            <p>+ Rp. @rupiah($shipping_price)</p>
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Potongan kupon</p>
                            @if ($voucher_id != null)
                                @if ($voucher_type == 'Persentase')
                                    <p>- @rupiah($voucher_price)%</p>
                                @elseif ($voucher_type == 'Potongan Tetap')
                                    <p>- Rp. @rupiah($voucher_price)</p>
                                @endif
                            @else
                                <p>- Rp. 0</p>
                            @endif
                        </div>
                        <div class="d-flex justify-content-between">
                            <p>Total</p>
                            @if ($voucher_id != null)
                                @if ($voucher_type == 'Persentase')
                                    <p>Rp. @rupiah((($sub_total + $shipping_price) * (100 - $voucher_price)) / 100)</p>
                                @elseif ($voucher->type == 'Potongan Tetap')
                                    <p>Rp. @rupiah($sub_total + $shipping_price - $voucher_price)</p>
                                @endif
                            @else
                                <p>Rp. @rupiah($sub_total + $shipping_price)</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="confirmation" class="btn btn-submit bg-success">Konfirmasi</button>
    @endslot
@endcomponent

{{-- Delete Order Modal --}}
@component('components.modal-delete')
    @slot('modalId')
        deleteOrderModal
    @endslot
    @slot('label')
        pesanan
    @endslot
    @slot('name')
        {{ $title }}
    @endslot
@endcomponent
