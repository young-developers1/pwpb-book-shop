{{-- Edit Order Modal --}}
@component('components.modal')
    @slot('modalId')
        editOrderModal
    @endslot
    @slot('modalTitle')
        Edit Order
    @endslot
    @slot('modalSize')
        modal-md
    @endslot
    @slot('modalBody')
        <div class="mb-3">
            <label for="effective_date" class="h6 mt-2 me-3">Status</label>
            <div>
                <select wire:model="status" class="form-select">
                    <option value="Sedang diproses">Sedang diproses</option>
                    <option value="Sedang dikirim">Sedang dikirim</option>
                    <option value="Pesanan sampai">Pesanan sampai</option>
                </select>
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="update" class="btn btn-submit">Save Changes</button>
    @endslot
@endcomponent

{{-- Delete order modal --}}
@component('components.modal-delete')
    @slot('modalId')
        deleteOrderModal
    @endslot
    @slot('label')
        order
    @endslot
    @slot('name')
        {{ $order_code }}
    @endslot
@endcomponent
