{{-- Create Category Modal --}}
@component('components.modal')
    @slot('modalId')
        createCategoryModal
    @endslot
    @slot('modalTitle')
        Create Category
    @endslot
    @slot('modalBody')
        <div class="mb-3">
            <label for="category_name" class="h6 mt-2 me-3">Nama Kategori</label>
            <div>
                <input type="text" wire:model="category_name" id="category_name"
                    class="form-control @error('category_name') is-invalid @enderror" placeholder="Nama kategori...">
                @error('category_name')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="store" class="btn btn-submit">Create New Category</button>
    @endslot
@endcomponent


{{-- Edit Category Modal --}}
@component('components.modal')
    @slot('modalId')
        editCategoryModal
    @endslot
    @slot('modalTitle')
        Edit Category
    @endslot
    @slot('modalBody')
        <div class="mb-3">
            <label for="category_name" class="h6 mt-2 me-3">Nama Kategori</label>
            <div>
                <input type="text" wire:model="category_name" id="category_name"
                    class="form-control @error('category_name') is-invalid @enderror" placeholder="Nama kategori...">
                @error('category_name')
                    <p class="invalid-feedback mb-0">{{ $message }}</p>
                @enderror
            </div>
        </div>
    @endslot
    @slot('modalBtnSubmit')
        <button wire:click.prevent="update" class="btn btn-submit">Save Changes</button>
    @endslot
@endcomponent

@component('components.modal-delete')
    @slot('modalId')
        deleteCategoryModal
    @endslot
    @slot('label')
        kategori
    @endslot
    @slot('name')
        {{ $category_name }}
    @endslot
@endcomponent
