    <!-- Modal create address -->
    <div class="modal fade" wire:ignore.self id="createAddressModal" data-bs-backdrop="static" data-bs-keyboard="false"
        tabindex="-1" aria-labelledby="createAddressModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="createAddressModalLabel">Tambah Alamat Pengiriman</h1>
                    <a href="#" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-xmark"></i></a>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="mb-3">
                            <label for="receipent_name">Nama Penerima</label>
                            <input type="text"
                                class="form-control d-block @error('receipent_name') is-invalid @enderror"
                                placeholder="Masukkan nama penerima" wire:model="receipent_name">
                            @error('receipent_name')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="phone">No Telephone</label>
                            <input type="text" class="form-control d-block @error('phone') is-invalid @enderror"
                                placeholder="Masukkan nomor telephone" wire:model="phone">
                            @error('phone')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="province">Provinsi</label>
                            <input type="text" class="form-control d-block @error('province') is-invalid @enderror"
                                placeholder="Masukkan provinsi tujuan" wire:model="province">
                            @error('province')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="regency">Kabupaten / Kota</label>
                            <input type="text" class="form-control d-block @error('regency') is-invalid @enderror"
                                placeholder="Masukkan kabupaten / kota tujuan" wire:model="regency">
                            @error('regency')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="district">Kecamatan</label>
                            <input type="text" class="form-control d-block @error('district') is-invalid @enderror"
                                placeholder="Masukkan kecamatan tujuan" wire:model="district">
                            @error('district')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="pos_code">Kode Pos</label>
                            <input type="text" class="form-control d-block @error('pos_code') is-invalid @enderror"
                                placeholder="Masukkan kode pos" wire:model="pos_code">
                            @error('pos_code')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="address">Alamat Lengkap</label>
                            <input type="text" class="form-control d-block @error('address') is-invalid @enderror"
                                placeholder="Masukkan alamat lengkap" wire:model="address">
                            @error('address')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="receipent_name">Label Alamat</label>
                            <div class="d-flex">
                                <div class="me-3">
                                    <input type="radio" id="home" wire:model="label" value="home">
                                    <label for="home">Home</label>
                                </div>
                                <div>
                                    <input type="radio" id="office" wire:model="label" value="office">
                                    <label for="office">Office</label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="d-flex mt-4 justify-content-end">
                        <button wire:click="storeAddress" type="button" class="btn btn-primary">Tambah
                            alamat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal all addresses -->
    <div class="modal fade" wire:ignore.self id="allAddressModal" data-bs-backdrop="static" data-bs-keyboard="false"
        tabindex="-1" aria-labelledby="allAddressModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="allAddressModalLabel">Alamat Pengiriman</h1>
                    <a href="#" data-bs-toggle="modal" data-bs-target="#createAddressModal"
                        aria-label="Close"><i class="fa-solid fa-plus"></i></a>
                </div>
                <div class="modal-body">
                    @foreach ($user_addresses as $address)
                        <div class="card border mb-4">
                            <div class="card-body">
                                <div class="d-flex justify-content-between">
                                    <p>Alamat pengiriman</p>
                                    <a href="#" class="text-primary" data-bs-toggle="modal"
                                        data-bs-target="#editAddressModal"
                                        wire:click="editAddress({{ $address->id }})">Edit</a>
                                </div>
                                <h6 class="text-capitalize">{{ $address->label }} @if ($address->is_active)
                                        <span class="badge text-bg-primary ms-1">Utama</span>
                                    @endif
                                </h6>
                                <div>
                                    <p style="font-size: 0.9em; margin-bottom: 0px;">
                                        {{ $address->receipent_name }} | {{ $address->phone }}
                                    </p>
                                    <p style="font-size: 0.9em; margin-bottom: 0px;">{{ $address->address }},
                                        {{ $address->district }}, {{ $address->regency }},
                                        {{ $address->province }}, {{ $address->pos_code }}</p>
                                </div>
                                <button wire:click="updateActiveAddress({{ $address->id }})" style="width: 100%"
                                    class="mt-3 btn btn-secondary">Pilih Alamat</button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- Modal edit address -->
    <div class="modal fade" wire:ignore.self id="editAddressModal" data-bs-backdrop="static"
        data-bs-keyboard="false" tabindex="-1" aria-labelledby="editAddressModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="editAddressModalLabel">Ubah Alamat Pengiriman</h1>
                    <a href="#" wire:click="resetAll" data-bs-dismiss="modal" aria-label="Close"><i
                            class="fa-solid fa-xmark"></i></a>
                </div>
                <div class="modal-body">
                    <form action="">
                        <div class="mb-3">
                            <label for="receipent_name">Nama Penerima</label>
                            <input type="text"
                                class="form-control d-block @error('receipent_name') is-invalid @enderror"
                                placeholder="Masukkan nama penerima" wire:model="receipent_name">
                            @error('receipent_name')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="phone">No Telephone</label>
                            <input type="text" class="form-control d-block @error('phone') is-invalid @enderror"
                                placeholder="Masukkan nomor telephone" wire:model="phone">
                            @error('phone')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="province">Provinsi</label>
                            <input type="text" class="form-control d-block @error('province') is-invalid @enderror"
                                placeholder="Masukkan provinsi tujuan" wire:model="province">
                            @error('province')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="regency">Kabupaten / Kota</label>
                            <input type="text" class="form-control d-block @error('regency') is-invalid @enderror"
                                placeholder="Masukkan kabupaten / kota tujuan" wire:model="regency">
                            @error('regency')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="district">Kecamatan</label>
                            <input type="text"
                                class="form-control d-block @error('district') is-invalid @enderror"
                                placeholder="Masukkan kecamatan tujuan" wire:model="district">
                            @error('district')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="pos_code">Kode Pos</label>
                            <input type="text"
                                class="form-control d-block @error('pos_code') is-invalid @enderror"
                                placeholder="Masukkan kode pos" wire:model="pos_code">
                            @error('pos_code')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="address">Alamat Lengkap</label>
                            <input type="text" class="form-control d-block @error('address') is-invalid @enderror"
                                placeholder="Masukkan alamat lengkap" wire:model="address">
                            @error('address')
                                <p class="text-danger" style="font-size: 0.8em">{{ $message }}</p>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="receipent_name">Label Alamat</label>
                            <div class="d-flex">
                                <div class="me-3">
                                    <input type="radio" id="home" wire:model="label" value="home">
                                    <label for="home">Home</label>
                                </div>
                                <div>
                                    <input type="radio" id="office" wire:model="label" value="office">
                                    <label for="office">Office</label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="d-flex mt-4 justify-content-end">
                        <button wire:click="updateAddress" type="button" class="btn btn-primary">Simpan
                            alamat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Delete Address --}}
    <div class="modal fade" wire:ignore.self id="deleteAddressModal" data-bs-backdrop="static"
        data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteAddressModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="deleteAddressModalLabel">Hapus Alamat Pengiriman</h1>
                    <a href="#" wire:click="resetAll" data-bs-dismiss="modal" aria-label="Close"><i
                            class="fa-solid fa-xmark"></i></a>
                </div>
                <div class="modal-body text-center">
                    <p>Apakah anda yakin ingin menghapus alamat :</p>
                    <h3 class="mb-3">{{ $receipent_name }}</h3>
                    <div class="d-flex mt-4 justify-content-center">
                        <button data-bs-dismiss="modal" wire:click="resetAll" type="button"
                            class="btn btn-secondary me-3">Batal</button>
                        <button wire:click="destroyAddress" type="button" class="btn btn-primary">Hapus
                            alamat</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Edit Active Address --}}
    <div class="modal fade" wire:ignore.self id="editActiveAddressModal" data-bs-backdrop="static"
        data-bs-keyboard="false" tabindex="-1" aria-labelledby="editActiveAddressModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="editActiveAddressModalLabel">Ubah Alamat Utama</h1>
                    <a href="#" wire:click="resetAll" data-bs-dismiss="modal" aria-label="Close"><i
                            class="fa-solid fa-xmark"></i></a>
                </div>
                <div class="modal-body text-center">
                    <p>Apakah anda yakin ingin merubah alamat utama</p>
                    <div class="d-flex mt-4 justify-content-center">
                        <button data-bs-dismiss="modal" wire:click="resetAll" type="button"
                            class="btn btn-secondary me-3">Batal</button>
                        <button wire:click="updateActiveAddress" type="button" class="btn btn-primary">Ubah
                            alamat utama</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (Request::is('checkout'))
        {{-- Modal all voucher --}}
        <div class="modal fade" wire:ignore.self id="allVoucherModal" data-bs-backdrop="static"
            data-bs-keyboard="false" tabindex="-1" aria-labelledby="allVoucherModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="allVoucherModalLabel">Semua voucher yang tersedia</h1>
                        <a href="#" data-bs-dismiss="modal" aria-label="Close"><i
                                class="fa-solid fa-xmark"></i></a>
                    </div>
                    <div class="modal-body">
                        @foreach ($vouchers as $voucher)
                            <div class="d-flex justify-content-between align-items-center px-3 py-2 mb-4 shadow-md">
                                <div>
                                    <h6>{{ $voucher->voucher_name }}</h6>
                                    @if ($voucher->type == 'Persentase')
                                        <p style="font-size: 0.85em">Nominal : {{ $voucher->nominal }}%</p>
                                    @else
                                        <p style="font-size: 0.85em">Nominal : @rupiah($voucher->nominal)</p>
                                    @endif
                                    <p style="font-size: 0.85em">Tanggal berlaku :
                                        {{ $voucher->effective_date_start->format('d M Y') }} -
                                        {{ $voucher->effective_date_end->format('d M Y') }}</p>
                                </div>
                                <button class="btn btn-success"
                                    wire:click="addVoucher({{ $voucher->id }})">Gunakan</button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
