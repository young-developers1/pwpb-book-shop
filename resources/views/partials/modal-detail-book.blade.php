{{-- Modal Edit Review Book --}}
<div class="modal fade" wire:ignore.self id="editReviewBookModal" data-bs-backdrop="static" data-bs-keyboard="false"
    tabindex="-1" aria-labelledby="editReviewBookModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="editReviewBookModalLabel">Review Buku {{ $book_title ?? '' }}</h1>
                <a href="#" wire:click="resetAll" data-bs-dismiss="modal" aria-label="Close"><i
                        class="fa-solid fa-xmark"></i></a>
            </div>
            <div class="modal-body">
                <div>
                    <div class="outer text-center">
                        <div class="ratings-box">
                            <div class="ratings-box__item">
                                <label>
                                    <input wire:model="rating" value="1" id="rate-1" class="rating-star-button"
                                        type="radio" name="rating-star-button">
                                    <div class="star-line-box">
                                        <span class="rating-star"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                    </div>
                                </label>
                                <p>😒 Kecewa Berat</p>
                            </div>
                            <div class="ratings-box__item">
                                <label>
                                    <input wire:model="rating" value="2" id="rate-2" class="rating-star-button"
                                        type="radio" name="rating-star-button">
                                    <div class="star-line-box">
                                        <span class="rating-star"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                    </div>
                                </label>
                                <p>😔 Tidak Puas</p>
                            </div>
                            <div class="ratings-box__item">
                                <label>
                                    <input wire:model="rating" value="3" id="rate-3" class="rating-star-button"
                                        type="radio" name="rating-star-button">
                                    <div class="star-line-box">
                                        <span class="rating-star"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                    </div>
                                </label>
                                <p>😐 Kurang Puas</p>
                            </div>
                            <div class="ratings-box__item">
                                <label>
                                    <input wire:model="rating" value="4" id="rate-4" class="rating-star-button"
                                        type="radio" name="rating-star-button">
                                    <div class="star-line-box">
                                        <span class="rating-star"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                    </div>
                                </label>
                                <p>😊 Puas</p>
                            </div>
                            <div class="ratings-box__item">
                                <label>
                                    <input wire:model="rating" value="5" id="rate-5" class="rating-star-button"
                                        type="radio" name="rating-star-button">
                                    <div class="star-line-box">
                                        <span class="rating-star"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                        <span class="rating-star-line"></span>
                                    </div>
                                </label>
                                <p>😁 Sangat Puas</p>
                            </div>
                        </div>
                    </div>
                    <div class="mt-5">
                        <label for="review">Berikan penilaianmu tentang buku <span
                                class="text-lowercase">{{ $book_title ?? '' }}</span></label>
                        <textarea rows="6" id="review" wire:model="review" class="form-control"
                            placeholder="Yuk, ceritakan kepuasanmu tentang kualitas buku & pelayanan kami"></textarea>
                    </div>
                    <div class="mt-3">
                        <input type="checkbox" wire:model="is_hidden" id="is_hidden">
                        <label for="is_hidden">Sembunyikan nama</label>
                    </div>
                </div>
                <div class="d-flex mt-5 justify-content-center">
                    <button data-bs-dismiss="modal" wire:click="resetAll" type="button"
                        class="btn btn-secondary me-3">Review nanti</button>
                    <button wire:click="updateReviewBook" type="button" class="btn btn-info">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Modal Delete Review Book --}}
<div class="modal fade" wire:ignore.self id="deleteReviewBookModal" data-bs-backdrop="static"
    data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteReviewBookModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="deleteReviewBookModalLabel">Hapus Penilaian</h1>
                <a href="#" wire:click="resetAll" data-bs-dismiss="modal" aria-label="Close"><i
                        class="fa-solid fa-xmark"></i></a>
            </div>
            <div class="modal-body text-center">
                <p>Apakah anda yakin ingin menghapus penilaian anda?</p>
                <div class="d-flex mt-4 justify-content-center">
                    <button data-bs-dismiss="modal" wire:click="resetAll" type="button"
                        class="btn btn-secondary me-3">Batal</button>
                    <button wire:click="destroyReview" type="button" class="btn btn-primary">Hapus
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
