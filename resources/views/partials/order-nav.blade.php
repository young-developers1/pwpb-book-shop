<div class="position-fixed shadow p-3 mt-5" style="width: 300px" wire:ignore>
    <div><a href="{{ route('orders') }}" class="order-nav {{ Request::is('orders*') ? 'active' : '' }} mb-3">Pesanan
            Saya</a></div>
    <div><a href="{{ route('profile') }}" class="order-nav {{ Request::is('profile*') ? 'active' : '' }} mb-3">Akun Saya</a>
    </div>
    <hr>
    <div>@livewire('auth.logout')</div>
</div>
