<div class="modal fade" wire:ignore.self id="{{ $modalId }}" data-bs-backdrop="static" data-bs-keyboard="false"
    tabindex="-1" aria-labelledby="{{ $modalId }}Label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable {{ $modalSize ?? 'modal-lg' }}">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="{{ $modalId }}Label">{{ $modalTitle }}</h4>
                <button type="button" wire:click="resetAll" class="btn-close" data-bs-dismiss="modal"
                    aria-label="Close"></button>
            </div>
            <div class="modal-body">
                {{ $modalBody }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" wire:click="resetAll"
                    data-bs-dismiss="modal">Close</button>
                {{ $modalBtnSubmit }}
            </div>
        </div>
    </div>
</div>
