<div class="modal fade" wire:ignore.self id="{{ $modalId }}" data-bs-backdrop="static" data-bs-keyboard="false"
    tabindex="-1" aria-labelledby="{{ $modalId }}Label" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div>
                    <p>Apakah kamu yakin ingin menghapus {{ $label }}:</p>
                    <h2 class="fw-bold">{{ $name }}</h2>
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <button type="button" class="btn btn-secondary me-3" data-bs-dismiss="modal">Cancel</button>
                    <button wire:click.prevent="destroy" class="btn btn-submit  bg-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>
</div>
