<nav class="navbar navbar-main navbar-expand-lg mx-5 px-0 shadow-none rounded" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-3 px-2">
        {{--  --}}
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
            <ul class="ms-md-auto navbar-nav justify-content-end">
                <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
                    <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </a>
                </li>
                <li class="nav-item dropdown ps-3 d-flex align-items-center">
                    <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton"
                        data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa-regular fa-xl fa-circle-user me-1"></i>
                        <span>{{ auth()->user()->username }}</span>
                    </a>
                    <ul class="dropdown-menu  dropdown-menu-end px-2 pt-2 pb-1 me-sm-n4"
                        aria-labelledby="dropdownMenuButton">
                        {{-- <li class="mb-2">
                            <a class="dropdown-item border-radius-md" href="#">
                                My Profile
                            </a>
                        </li> --}}
                        @livewire('auth.logout')
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
