<footer class="pt-5">
    <div class="container">
        <div class="d-flex justify-content-between">
            <div class="col-5 mb-3">
                <div class="d-flex mb-3">
                    <img class="rounded-circle" src="{{ asset('assets/img/logo-skenbooks.png') }}" width="50"
                        height="100%" alt="">
                    <h5 class="ms-2 mt-2 font-weight-bold text-primary">SkenBooks</h5>
                </div>
                <p class="text-secondary">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nostrum illum
                    vitae dolor, culpa nobis
                    impedit laudantium temporibus provident assumenda.</p>
            </div>

            <div class="d-flex footer-nav-group">
                <div class="me-5">
                    <h5 class="text-primary">Quick Link</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Home</a></li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">All Books</a></li>
                        <li class="nav-item mb-2"><a href="/books?filter=latest" class="nav-link p-0 text-muted">Buku
                                Terbaru</a>
                        </li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Buku Terpopuler</a>
                        </li>
                        <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-muted">Buku Terlaris</a>
                        </li>
                    </ul>
                </div>

                <div class="me-5">
                    <h5 class="text-primary">Kategori buku</h5>
                    <ul class="nav flex-column">
                        {{ $categories }}
                    </ul>
                </div>

                <div class="me-5">
                    <h5 class="text-primary">Kontak kami</h5>
                    <ul class="nav flex-column">
                        <li class="nav-item mb-2 text-muted"><i class="fa-solid fa-envelope me-2"></i> sample@gmail.com
                        </li>
                        <li class="nav-item mb-2 text-muted"><i class="fa-solid fa-location-dot me-2"></i>Denpasar Barat
                        </li>
                        <li class="nav-item mb-2 text-muted"><i class="fa-solid fa-phone me-2"></i>+62 813 3735 9813
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex flex-column copyright-group flex-sm-row justify-content-center pt-4 pb-2 mt-4">
        <p class="fs-6">&copy; 2022 Copyright, YoungDev. All rights reserved.</p>       
    </div>
</footer>
