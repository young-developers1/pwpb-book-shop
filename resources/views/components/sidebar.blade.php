<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 fixed-start " id="sidenav-main">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
            aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand d-flex align-items-center m-0 justify-content-center" href=" #">
            <img src="{{ asset('assets/img/logo-skenbooks.png') }}" alt="">
        </a>
    </div>
    <div class="collapse navbar-collapse px-4  w-auto " id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/dashboard*') ? 'active' : '' }}"
                    href="{{ route('dashboard.index') }}">
                    <div
                        class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-gauge"></i>
                    </div>
                    <span class="nav-link-text ms-1">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/notifications*') ? 'active' : '' }}"
                    href="{{ route('notification.index') }}">
                    <div
                        class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                        <i class="fa-regular fa-bell"></i>
                    </div>
                    <span class="nav-link-text ms-1">Notification</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/books*') ? 'active' : '' }}" href="{{ route('book.index') }}">
                    <div
                        class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-book"></i>
                    </div>
                    <span class="nav-link-text ms-1">Book</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/categories*') ? 'active' : '' }}"
                    href="{{ route('category.index') }}">
                    <div
                        class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                        {{-- <i class="fa-solid fa-table-cells-large"></i> --}}
                        <i class="fa-solid fa-border-all"></i>
                    </div>
                    <span class="nav-link-text ms-1">Category</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/vouchers*') ? 'active' : '' }}"
                    href="{{ route('voucher.index') }}">
                    <div
                        class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-rug"></i>
                    </div>
                    <span class="nav-link-text ms-1">Voucher</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/orders*') ? 'active' : '' }}"
                    href="{{ route('order.index') }}">
                    <div
                        class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-cart-shopping"></i>
                    </div>
                    <span class="nav-link-text ms-1">Order</span>
                </a>
            </li>
            @if (auth()->user()->role_as == 'Super admin')
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('admin/admin-accounts*') ? 'active' : '' }}"
                        href="{{ route('admin.index') }}">
                        <div
                            class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                            <i class="fa-solid fa-user-gear"></i>
                        </div>
                        <span class="nav-link-text ms-1">Admin Account</span>
                    </a>
                </li>
            @endif
            <li class="nav-item">
                <a class="nav-link {{ Request::is('admin/reports*') ? 'active' : '' }}"
                    href="{{ route('report.index') }}">
                    <div
                        class="icon icon-shape icon-sm px-0 text-center d-flex align-items-center justify-content-center">
                        <i class="fa-solid fa-file-signature"></i>
                    </div>
                    <span class="nav-link-text ms-1">Report</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
