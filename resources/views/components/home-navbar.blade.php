<nav class="navbar navbar-expand-lg bg-dark shadow-none position-fixed w-100" id="navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{ asset('assets/img/logo-skenbooks.png') }}" width="40" class="rounded-circle me-2"
                alt="">
            <span class="font-weight-bold text-primary h6">SkenBooks</span>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <i class="fa-solid fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a wire:ignore class="nav-link {{ Request::is('/') ? 'active' : '' }}" aria-current="page"
                        href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a wire:ignore class="nav-link {{ Request::is('books') ? 'active' : '' }}" aria-current="page"
                        href="{{ route('books') }}">All Books</a>
                </li>
            </ul>
            <div class="d-flex">
                <form class="d-flex" role="search">
                    <input class="form-control rounded-pill px-4" type="search" placeholder="Search"
                        aria-label="Search" wire:model="search">
                </form>
                <div class="navbar-nav">
                    @auth
                        <li class="nav-item dropdown profile-group">
                            <a class="nav-link" href="#" role="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="fa-solid fa-circle-user ms-3 text-dark" style="font-size: 1.5em"></i>
                            </a>
                            <ul class="dropdown-menu" style="top: 10px;">
                                <li>
                                    <div class="dropdown-item">
                                        <h6>Halo, {{ auth()->user()->name }}</h6>
                                    </div>
                                </li>
                                @if (auth()->user()->role_as == 'user')
                                    <li><a class="dropdown-item" href="{{ route('orders') }}">Pesanan saya</a></li>
                                    <li><a class="dropdown-item" href="{{ route('profile') }}">Akun saya</a></li>
                                    <li>
                                        <hr class="dropdown-divider">
                                    </li>
                                @endif
                                @livewire('auth.logout')
                            </ul>
                        </li>
                        @if (auth()->user()->role_as == 'user')
                            <a class="nav-link position-relative cart-group" href="/cart" role="button">
                                <i class="fa-solid text-dark fa-cart-shopping" style="font-size: 1.5em"></i>
                                <p class="count-text">{{ $count_cart }}</p>
                            </a>
                        @endif
                    @else
                        <a class="btn btn-primary position-relative ms-3" style="top: 7px" href="/cart" role="button">
                            Login
                        </a>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</nav>
