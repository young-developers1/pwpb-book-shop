<?php

namespace App\Http\Livewire\Admin\Book;

use App\Models\Book;
use App\Models\Category;
use App\Models\LogActivity;
use App\Models\SubImageBook;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $book_id;

    public $title;
    public $category_id;
    public $thumbnail_img;
    public $description;
    public $total_page;
    public $publisher;
    public $issue_date;
    public $weight;
    public $isbn;
    public $language;
    public $writer;
    public $stock;
    public $price;
    public $discount;
    public $status = true;
    
    public $book_images = [];
    public $edit_book_images = [];

    public $edit_thumbnail_img;

    public $query = '';
    public $filterBook = '';

    protected $rules = [
        'title' => 'required',
        'category_id' => 'required',
        'description' => 'required',
        'total_page' => 'required|numeric',
        'publisher' => 'required',
        'issue_date' => 'required',
        'weight' => 'required|numeric',
        'isbn' => 'required',
        'language' => 'required',
        'writer' => 'required',
        'stock' => 'required|numeric',
        'price' => 'required|numeric',
    ];

    protected $validationAttributes = [
        'category_id' => 'category'
    ];

    // public function updated()
    // {
    //     $this->validate();
    // }

    public function resetAll()
    {
        $this->reset();
    }
    
    public function store()
    {
        $this->validate([
            'title' => 'required',
            'thumbnail_img' => 'required|image|file|max:3000',
            'category_id' => 'required',
            'description' => 'required',
            'total_page' => 'required|numeric',
            'publisher' => 'required',
            'issue_date' => 'required',
            'weight' => 'required|numeric',
            'isbn' => 'required',
            'language' => 'required',
            'writer' => 'required',
            'stock' => 'required|numeric',
            'price' => 'required|numeric',
        ]);

        DB::beginTransaction();
        try {
            $fileName = $this->thumbnail_img->store('uploads/thumbnails', 'public');

            $book = Book::create([
                'title' => $this->title,
                'category_id' => $this->category_id,
                'slug' => Str::slug($this->title),
                'thumbnail_img' => $fileName,
                'description' => $this->description,
                'total_page' => $this->total_page,
                'publisher' => $this->publisher,
                'issue_date' => $this->issue_date,
                'weight' => $this->weight,
                'isbn' => $this->isbn,
                'language' => $this->language,
                'writer' => $this->writer,
                'stock' => $this->stock,
                'price' => $this->price,
                'discount' => $this->discount ?? 0,
                'status' => $this->status ? 'active' : 'inactive',
            ]);

            foreach($this->book_images as $image) {
                $subImgFileName = $image->store('uploads/sub_images', 'public');

                SubImageBook::create([
                    'book_id' => $book->id,
                    'image' => $subImgFileName
                ]);
            }

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'book',
                'description' => 'menambahkan buku',
                'value' => $book->title,
                'event' => 'created'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Buku berhasil ditambahkan');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('bookCreated');
    }

    public function edit($book_id)
    {
        $book = Book::find($book_id);
        $book_images = SubImageBook::where('book_id', $book->id)->get();

        $this->book_id = $book->id;
        $this->title = $book->title;
        $this->category_id = $book->category_id;
        $this->description = $book->description;
        $this->total_page = $book->total_page;
        $this->publisher = $book->publisher;
        $this->issue_date = $book->issue_date;
        $this->weight = $book->weight;
        $this->isbn = $book->isbn;
        $this->language = $book->language;
        $this->writer = $book->writer;
        $this->stock = $book->stock;
        $this->price = $book->price;
        $this->discount = $book->discount;
        $this->status = $book->status;

        $this->edit_thumbnail_img = $book->thumbnail_img;
        $this->edit_book_images = $book_images;
    }

    public function update()
    {
        $this->validate();

        DB::beginTransaction();
        try {
            $book = Book::find($this->book_id);
            
            $fileName = $book->thumbnail_img;

            if ($this->thumbnail_img) {
                $path = 'storage/' . $fileName;
                if (File::exists($path)) {
                    File::delete($path);
                }
                $fileName = $this->thumbnail_img->store('uploads/thumbnails', 'public');
            }

            if($this->book_images) {
                foreach($this->book_images as $image) {
                    $subImgFileName = $image->store('uploads/sub_images', 'public');

                    SubImageBook::create([
                        'book_id' => $book->id,
                        'image' => $subImgFileName
                    ]);
                }
            }

            $book->title = $this->title;
            $book->category_id = $this->category_id;
            $book->slug = Str::slug($this->title);
            $book->thumbnail_img = $fileName;
            $book->description = $this->description;
            $book->title = $this->title;
            $book->total_page = $this->total_page;
            $book->publisher = $this->publisher;
            $book->issue_date = $this->issue_date;
            $book->weight = $this->weight;
            $book->language = $this->language;
            $book->writer = $this->writer;
            $book->stock = $this->stock;
            $book->price = $this->price;
            $book->discount = $this->discount;
            $book->status = $this->status ? 'active' : 'inactive';

            $book->update();  
            
            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'book',
                'description' => 'mengubah buku',
                'value' => $book->title,
                'event' => 'updated'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Buku berhasil disimpan');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('bookUpdated');
    }

    public function delete($book_id)
    {
        $book = Book::find($book_id);

        $this->book_id = $book->id;
        $this->title = $book->title;
    }

    public function deleteSubImageBook($id)
    {
        $sub_image_book = SubImageBook::find($id);

        $path = 'storage/' . $sub_image_book->image;
        File::delete($path);
        
        $sub_image_book->delete();
    }

    public function destroy()
    {
        try {
            $book = Book::find($this->book_id);
            
            $path = 'storage/' . $book->thumbnail_img;
            if (File::exists($path)) {
                File::delete($path);
            }

            $subImageBook = SubImageBook::where('book_id', $this->book_id)->get();
            foreach($subImageBook as $image) {
                $path = 'storage/' . $image->image;
                if (File::exists($path)) {
                    File::delete($path);
                }
            }

            $book->delete();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'book',
                'description' => 'menghapus buku',
                'value' => $book->title,
                'event' => 'deleted'
            ]);

            $this->reset();
            session()->flash('success', 'Buku berhasil dihapus');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('bookDeleted');
    }

    public function render()
    {
        $books = Book::latest()
                    ->with('category')
                    ->where('title', 'like', "%$this->query%")
                    ->where('category_id', 'like', "%$this->filterBook%")                    
                    ->paginate(15);
        $categories = Category::latest()->get();

        // if($this->book_id) {
        //     $book = Book::find($this->book_id);
        // }

        return view('livewire.admin.book.index', compact('books', 'categories'));
    }
}
