<?php

namespace App\Http\Livewire\Admin\Voucher;

use App\Models\LogActivity;
use App\Models\Voucher;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $voucher_id;

    public $voucher_name;
    public $voucher_code;
    public $type = 'Persentase';
    public $nominal;
    public $effective_date_start;
    public $effective_date_end;
    public $status = 'public';

    public $query = '';

    // protected $rules = [
    //     'voucher_name' => 'required|max:30',
    //     'voucher_code' => 'required|max:10',
    //     'type' => 'required',
    //     'nominal' => 'required|numeric',
    //     'effective_date' => 'required',
    // ];

    // public function updated()
    // {
    //     $this->validate();
    // }
    
    public function resetAll()
    {
        $this->reset();
    }

    public function store()
    {
        $this->validate([
            'voucher_name' => 'required|max:30|unique:vouchers,voucher_name',
            'voucher_code' => 'required|max:20|unique:vouchers,voucher_code',
            'type' => 'required',
            'nominal' => 'required|numeric',
            'effective_date_start' => 'required',
            'effective_date_end' => 'required',
        ]);

        DB::beginTransaction();
        try {

            $voucher = Voucher::create([
                'voucher_name' => $this->voucher_name,
                'voucher_code' => $this->voucher_code,
                'type' => $this->type,
                'nominal' => $this->nominal,
                'effective_date_start' => $this->effective_date_start,
                'effective_date_end' => $this->effective_date_end,
                'status' => $this->status
            ]);

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'voucher',
                'description' => 'menambahkan voucher',
                'value' => $voucher->voucher_name,
                'event' => 'created'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Voucher berhasil ditambahkan');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('voucherCreated');
    }

    public function edit($voucher_id)
    {
        $voucher = Voucher::find($voucher_id);

        $this->voucher_id = $voucher->id;
        $this->voucher_name = $voucher->voucher_name;      
        $this->voucher_code = $voucher->voucher_code;      
        $this->type = $voucher->type;      
        $this->nominal = $voucher->nominal;      
        $this->effective_date_start = $voucher->effective_date_start->format('Y-m-d');  
        $this->effective_date_end = $voucher->effective_date_end->format('Y-m-d');  
        $this->status = $voucher->status;  
    }

    public function update()
    {
        $this->validate([
            'voucher_name' => 'required|max:30',
            'voucher_code' => 'required|max:20',
            'type' => 'required',
            'nominal' => 'required|numeric',
            'effective_date_start' => 'required',
            'effective_date_end' => 'required',
        ]);

        DB::beginTransaction();
        try {
            $voucher = Voucher::find($this->voucher_id);

            $voucher->voucher_name = $this->voucher_name;            
            $voucher->voucher_code = $this->voucher_code;            
            $voucher->type = $this->type;            
            $voucher->nominal = $this->nominal;            
            $voucher->effective_date_start = $this->effective_date_start;          
            $voucher->effective_date_end = $this->effective_date_end;          
            $voucher->status = $this->status;            

            $voucher->update();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'voucher',
                'description' => 'mengubah voucher',
                'value' => $voucher->voucher_name,
                'event' => 'updated'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Voucher berhasil disimpan');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('voucherUpdated');
    }

    public function delete($voucher_id)
    {
        $voucher = Voucher::find($voucher_id);

        $this->voucher_id = $voucher->id;
        $this->voucher_name = $voucher->voucher_name;
    }

    public function destroy()
    {
        try {
            $voucher = Voucher::find($this->voucher_id);

            $voucher->delete();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'voucher',
                'description' => 'menghapus voucher',
                'value' => $voucher->voucher_name,
                'event' => 'deleted'
            ]);

            $this->reset();
            session()->flash('success', 'Voucher berhasil dihapus');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('voucherDeleted');
    }

    public function render()
    {
        $vouchers = Voucher::latest()
                        ->where('voucher_name', 'LIKE', "%$this->query%")
                        ->orWhere('voucher_code', 'LIKE', "%$this->query%")
                        ->get();

        return view('livewire.admin.voucher.index' , compact('vouchers'));
    }
}
