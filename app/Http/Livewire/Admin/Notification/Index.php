<?php

namespace App\Http\Livewire\Admin\Notification;

use App\Models\LogActivity;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Index extends Component
{
    public $order_id;
    public $title;

    public $order_code;
    public $name;
    public $email;
    public $phone;
    public $address;
    public $label;
    public $note;
    public $shipping_price;
    public $sub_total;

    public $voucher_id;
    public $voucher_type;
    public $voucher_price;

    public function resetAll()
    {
        $this->reset();
    }

    public function getDetailOrder($order_id)
    {
        $order = Order::find($order_id);

        $this->order_id = $order->id;
        $this->order_code = $order->order_code;
        $this->name = $order->user->name;
        $this->email = $order->user->email;
        $this->phone = $order->user_address->phone;
        $this->address = $order->user_address->address;
        $this->label = $order->user_address->label;
        $this->note = $order->user_address->note;
        $this->shipping_price = $order->shipping_price;
        $this->sub_total = $order->sub_total;

        if($order->voucher_id != null) {
            $this->voucher_id = $order->voucher_id;
            $this->voucher_type = $order->voucher->type;
            $this->voucher_price = $order->voucher->nominal;
        }
    }

    public function confirmation()
    {
        try {
            DB::beginTransaction();

            $order = Order::find($this->order_id);
            $order->status = 'Sedang diproses';
            $order->update();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'order',
                'description' => 'mengkonfirmasi pesanan',
                'value' => $order->order_code,
                'event' => 'created'
            ]);

            DB::commit();
            $this->reset();

            session()->flash('success', 'Pesanan berhasil dikonfirmasi');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('orderConfirmed');
    }

    public function delete($order_id)
    {
        $order = Order::find($order_id);

        $this->order_id = $order->id;
        $this->title = $order->order_code;
    }

    public function destroy()
    {
        try {
            $order = Order::find($this->order_id);        
            $order->delete();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'order',
                'description' => 'menghapus pesanan',
                'value' => $order->order_code,
                'event' => 'deleted'
            ]);

            $this->reset();
            session()->flash('success', 'Pesanan berhasil dihapus');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('orderDeleted');
    }

    public function render()
    {
        $orders = Order::latest()->where('status', 'Menunggu pembayaran')->get();

        $order_items = null;
        if($this->order_id) {
            $order_items = OrderItem::where('order_id', $this->order_id)->get();
        }

        return view('livewire.admin.notification.index', compact('orders', 'order_items'));
    }
}
