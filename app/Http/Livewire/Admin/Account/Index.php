<?php

namespace App\Http\Livewire\Admin\Account;

use App\Models\LogActivity;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Index extends Component
{
    public $user_id;

    public $name;
    public $username;
    public $email;
    public $password;
    public $confirm_password;

    public $query = "";

    protected $rules = [
        'name' => 'required',
        'username' => 'required',
        'email' => 'required|email:dns',
    ];

    public function resetAll()
    {
        $this->reset();
    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|email:dns|unique:users,email',
            'password' => 'required|min:6|same:confirm_password'
        ]);

        DB::beginTransaction();
        try {

            $user = User::create([
                'name' => $this->name,
                'username' => $this->username,
                'email' => $this->email,
                'password' => bcrypt($this->password),
                'role_as' => 'admin'
            ]);

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'user account',
                'description' => 'menambahkan akun admin',
                'value' => $user->username,
                'event' => 'created'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Akun admin berhasil ditambahkan');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('userCreated');
    }

    public function edit($user_id)
    {
        $user = User::find($user_id);
        
        $this->user_id = $user->id;
        $this->name = $user->name;
        $this->username = $user->username;
        $this->email = $user->email;
    }

    public function update()
    {
        $this->validate();

        DB::beginTransaction();
        try {
            $user = User::find($this->user_id);
            $user->name = $this->name;
            $user->username = $this->username;
            $user->email = $this->email;
            
            if($this->password) {
                $user->password = bcrypt($this->password);
            }

            $user->update();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'user account',
                'description' => 'mengubah akun admin',
                'value' => $user->username,
                'event' => 'updated'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Akun admin berhasil diubah');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('userUpdated');
    }

    public function delete($user_id)
    {
        $user = User::find($user_id);

        $this->user_id = $user->id;
        $this->name = $user->name;
    }

    public function destroy()
    {
        try {
            $user = User::find($this->user_id);
            $user->delete();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'user account',
                'description' => 'menghapus akun admin',
                'value' => $user->username,
                'event' => 'deleted'
            ]);

            $this->reset();
            session()->flash('success', 'Akun admin berhasil dihapus');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('userDeleted');
    }

    public function render()
    {
        $admins = User::latest()
                    ->where('role_as', 'admin')
                    ->where('name', 'like', "%$this->query%")
                    ->get();
        return view('livewire.admin.account.index', compact('admins'));
    }
}
