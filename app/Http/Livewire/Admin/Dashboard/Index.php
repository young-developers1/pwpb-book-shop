<?php

namespace App\Http\Livewire\Admin\Dashboard;

use App\Models\LogActivity;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Spatie\Activitylog\Models\Activity;

class Index extends Component
{
    public $orderDatas = [0, 0, 0, 0, 0, 0, 0];
    public $incomeDatas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    public function mount()
    {
        foreach ($this->orderDatas as $key => $item) {
            $getOrderItemsCurrentWeek = Order::select(DB::raw("COUNT(*) as count"))
            ->whereDate('created_at', [Carbon::now()->startOfWeek()->addDays($key)->format('Y-m-d')])
            ->where('status', 'Pesanan selesai')
            ->groupBy(DB::raw("Date(created_at)"))
            ->pluck("count")
            ->toArray();

            $this->orderDatas[$key] = count($getOrderItemsCurrentWeek) ? $getOrderItemsCurrentWeek[0] : 0;
        }

        $incomeData = Order::select(DB::raw("SUM(sub_total) as total"))
        ->whereYear("created_at", date('Y'))
        ->where('status', 'Pesanan selesai')
        ->groupBy(DB::raw("Month(created_at)"))
        ->pluck("total");

        $incomeMonths = Order::select(DB::raw("Month(created_at) as month"))
        ->whereYear('created_at', date('Y'))
        ->where('status', 'Pesanan selesai')
        ->groupBy(DB::raw("Month(created_at)"))
        ->pluck("month");

        foreach ($incomeMonths as $index => $month) {
            $this->incomeDatas[$month - 1] = $incomeData[$index];
        }
        
    }

    public function render()
    {
        $notifications = Order::latest()->where('status', 'Menunggu pembayaran')->limit(5)->get();
        $get_order_processed = Order::where('status', 'Sedang diproses')->count();
        $get_order_completed = Order::where('status', 'Pesanan selesai')->count();
        $get_total_income = Order::latest()->where('status', 'Pesanan selesai')->get();

        $total_incomes = 0;
        foreach ($get_total_income as $item) {
            $total_incomes += $item->sub_total;
        }

        $log_activities = LogActivity::latest()->limit(5)->get();

        return view('livewire.admin.dashboard.index', compact('notifications', 'get_order_processed', 'get_order_completed', 'total_incomes', 'log_activities'));
    }
}
