<?php

namespace App\Http\Livewire\Admin\Category;

use App\Models\Category;
use App\Models\LogActivity;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class Index extends Component
{
    use WithFileUploads;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $category_id;
    public $category_name;

    public $query = '';

    protected $rules = [
        'category_name' => 'required|max:30',
    ];

    // public function updated()
    // {
    //     $this->validate();
    // }
    
    public function resetAll()
    {
        $this->reset();
    }
    
    public function store()
    {
        $this->validate();

        DB::beginTransaction();
        try {

            $category = Category::create([
                'category_name' => $this->category_name,
                'slug' => Str::slug($this->category_name)
            ]);

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'category',
                'description' => 'menambahkan kategori',
                'value' => $category->category_name,
                'event' => 'created'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Kategori berhasil ditambahkan');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('categoryCreated');
    }

    public function edit($category_id)
    {
        $category = Category::find($category_id);

        $this->category_id = $category->id;
        $this->category_name = $category->category_name;        
    }

    public function update()
    {
        $this->validate();

        DB::beginTransaction();
        try {
            $category = Category::find($this->category_id);
            $category->category_name = $this->category_name;
            $category->slug = Str::slug($this->category_name);

            $category->update();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'category',
                'description' => 'mengubah kategori',
                'value' => $category->category_name,
                'event' => 'updated'
            ]);

            DB::commit();
            $this->reset();
            session()->flash('success', 'Kategori berhasil disimpan');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('categoryUpdated');
    }

    public function delete($category_id)
    {
        $category = Category::find($category_id);

        $this->category_id = $category->id;
        $this->category_name = $category->category_name;
    }

    public function destroy()
    {
        try {
            $category = Category::find($this->category_id);

            $category->delete();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'category',
                'description' => 'menghapus kategori',
                'value' => $category->category_name,
                'event' => 'deleted'
            ]);

            $this->reset();
            session()->flash('success', 'Kategori berhasil dihapus');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('categoryDeleted');
    }

    public function render()
    {
        $categories = Category::latest()
                            ->where('category_name', 'like', "%$this->query%")
                            ->paginate(10);

        return view('livewire.admin.category.index', compact('categories'));
    }
}
