<?php

namespace App\Http\Livewire\Admin\Order;

use App\Models\Order;
use Livewire\Component;

class Detail extends Component
{
    public $order_id;

    public function mount($id)
    {
        $this->order_id = $id;
    }

    public function render()
    {
        $order = Order::find($this->order_id);

        return view('livewire.admin.order.detail', compact('order'));
    }
}
