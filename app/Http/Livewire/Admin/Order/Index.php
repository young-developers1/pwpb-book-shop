<?php

namespace App\Http\Livewire\Admin\Order;

use App\Models\LogActivity;
use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;
    
    public $order_id;
    public $order_code;
    public $status;
    public $query = "";

    public function resetAll()
    {
        $this->reset();
    }

    public function edit($order_id)
    {
        $order = Order::find($order_id);

        $this->order_id = $order->id;
        $this->status = $order->status;
    }

    public function update()
    {
        try {
            $order = Order::find($this->order_id);
            $order->status = $this->status;

            $order->update();

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'order',
                'description' => 'mengubah status pesanan',
                'value' => $order->order_code,
                'event' => 'updated'
            ]);

            $this->reset();
            session()->flash('success', 'Status pesanan berhasil diubah');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('orderUpdated');
    }

    public function delete($order_id)
    {
        $order = Order::find($order_id);

        $this->order_id = $order->id;
        $this->order_code = $order->order_code;
    }

    public function destroy()
    {
        try {
            $order = Order::find($this->order_id);
            $order_items = Order::where('order_id', $order->id);

            foreach($order_items as $item) {
                $item->delete();
            }

            LogActivity::create([
                'user_id' => Auth::id(),
                'log_name' => 'order',
                'description' => 'menghapus pesanan',
                'value' => $order->order_code,
                'event' => 'deleted'
            ]);

            $order->delete();

            $this->reset();
            session()->flash('success', 'Pesanan berhasil dihapus');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('orderDeleted');
    }

    public function render()
    {
        $orders = Order::latest()
                        ->with('user')
                        ->where('status', 'Sedang diproses')
                        ->where('order_code', 'LIKE', "%$this->query%")                        
                        ->orWhere('status', 'Sedang dikirim')
                        ->orWhere('status', 'Pesanan sampai')
                        ->get();

        return view('livewire.admin.order.index', compact('orders'));
    }
}
