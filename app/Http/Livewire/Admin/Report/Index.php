<?php

namespace App\Http\Livewire\Admin\Report;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Index extends Component
{
    public $filterOrder = 'terbaru';
    public $filterFavoriteBook = 'terfavorit';

    public function render()
    {
        if($this->filterOrder == 'terbaru') {
            $orders = Order::latest()
                        ->with('user')
                        ->where('status', 'Pesanan selesai')
                        ->get();
        } else if($this->filterOrder == 'terlama') {
            $orders = Order::oldest()
                        ->with('user')
                        ->where('status', 'Pesanan selesai')
                        ->get();
        } else if($this->filterOrder == 'terlaris') {
            $orders = Order::with('user')
                        ->where('status', 'Pesanan selesai')
                        ->orderBy('sub_total', 'DESC')
                        ->get();
        }

        if($this->filterFavoriteBook == 'terfavorit') {
            $favorite_books = OrderItem::select('order_items.book_id', DB::raw('SUM(order_items.price * order_items.quantity) as total_price'), DB::raw('SUM(order_items.quantity) AS total_quantity'))
            ->groupBy('order_items.book_id')
            ->orderBy('total_quantity', 'DESC')
            ->with('book')
            ->get();
        } else if($this->filterFavoriteBook == 'terlaris') {
            $favorite_books = OrderItem::select('book_id', DB::raw('SUM(order_items.price * order_items.quantity) as total_price'), DB::raw('SUM(order_items.quantity) AS total_quantity'))
            ->groupBy('book_id')
            ->orderBy('total_price', 'DESC')
            ->with('book')
            ->get();
        }

        return view('livewire.admin.report.index', compact('orders', 'favorite_books'));
    }
}
