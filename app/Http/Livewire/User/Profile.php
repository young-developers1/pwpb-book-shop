<?php

namespace App\Http\Livewire\User;

use App\Models\Cart;
use App\Models\Category;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Profile extends Component
{
    public $search;

    public $name;
    public $username;
    public $email;

    public $old_password;
    public $new_password;
    public $confirm_new_password;

    public $address_id;
    public $receipent_name;
    public $phone;
    public $province;
    public $regency;
    public $district;
    public $pos_code;
    public $address;
    public $label = 'home';

    public function mount()
    {
        $this->name = Auth::user()->name;
        $this->username = Auth::user()->username;
        $this->email = Auth::user()->email;
    }

    public function updated()
    {
        if($this->search) {
            return redirect('/books?search=' . $this->search);
        }
    }

    public function resetAll()
    {
        $this->reset();
    }

    public function updatePassword()
    {
        $this->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:8|same:confirm_new_password',
        ]);
        try {
            DB::beginTransaction();
            $user = User::find(Auth::id());

            if(Hash::check($this->old_password, $user->password)) {
                $user->password = bcrypt($this->new_password);
                $user->update();

                $this->old_password = '';
                $this->new_password = '';
                $this->confirm_new_password = '';

                DB::commit();

                session()->flash('passwordUpdateSuccess', 'Password berhasil diubah');
            } else {
                session()->flash('passwordNotMatch', 'Password saat ini dengan password lama tidak sama!');
            }

        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('errorProfile', 'Error: ' . $e->getMessage());
        }
    }

    public function updateUserProfile()
    {
        $this->validate([
            'name' => 'required',
            'username' => 'required|alpha_dash|min:5',
            'email' => 'required|email:dns',            
        ]);
        try {
            DB::beginTransaction();
            $user = User::find(Auth::id());
            $user->name = $this->name;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->update();

            DB::commit();
            session()->flash('successProfile', 'Akun berhasil diubah');

        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('errorProfile', 'Error: ' . $e->getMessage());
        }
    }

    public function storeAddress()
    {
        $validatedData = $this->validate([
            'receipent_name' => 'required',
            'phone' => 'required|max:12',
            'province' => 'required',
            'regency' => 'required',
            'district' => 'required',
            'pos_code' => 'required',
            'address' => 'required',
            'label' => 'required',
        ]);

        $validatedData['user_id'] = Auth::id();

        try {
            DB::beginTransaction();

            UserAddress::create($validatedData);
            
            DB::commit();
            session()->flash('success', 'Berhasil menambahkan alamat baru');
            $this->reset();
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('Error: ' . $e->getMessage());
        }
        
        $this->emit('addressAdded');

    }

    public function editAddress($id)
    {
        $user_address = UserAddress::find($id);

        $this->address_id = $user_address->id;
        $this->receipent_name = $user_address->receipent_name;
        $this->phone = $user_address->phone;
        $this->province = $user_address->province;
        $this->regency = $user_address->regency;
        $this->district = $user_address->district;
        $this->pos_code = $user_address->pos_code;
        $this->address = $user_address->address;
        $this->label = $user_address->label;
    }

    public function updateAddress()
    {
        $validatedData = $this->validate([
            'receipent_name' => 'required',
            'phone' => 'required|max:12',
            'province' => 'required',
            'regency' => 'required',
            'district' => 'required',
            'pos_code' => 'required',
            'address' => 'required',
            'label' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $user_address = UserAddress::find($this->address_id);
            $user_address->update($validatedData);
            
            DB::commit();
            session()->flash('success', 'Berhasil mengubah alamat');
            $this->reset();
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('Error: ' . $e->getMessage());
        }
        
        $this->emit('addressUpdated');

    }

    public function deleteAddress($id)
    {
        $address = UserAddress::find($id);
        $this->address_id = $address->id;
        $this->receipent_name = $address->receipent_name;
    }

    public function destroyAddress()
    {
        try {
            DB::beginTransaction();

            $user_address = UserAddress::find($this->address_id);
            $user_address->delete();
            
            DB::commit();
            session()->flash('success', 'Berhasil menghapus alamat');
            $this->reset();
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('Error: ' . $e->getMessage());
        }
        
        $this->emit('addressDeleted');

    }

    public function editActiveAddress($id)
    {
        $address = UserAddress::find($id);
        $this->address_id = $address->id;
    }

    public function updateActiveAddress()
    {
        try {
            DB::beginTransaction();
            $get_active_address = UserAddress::where('is_active', 1)->first();
            $get_active_address->is_active = 0;
            $get_active_address->update();

            $user_address = UserAddress::find($this->address_id);
            $user_address->is_active = 1;
            $user_address->update();
            
            DB::commit();
            session()->flash('success', 'Berhasil mengubah alamat utama');
            $this->reset();
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('Error: ' . $e->getMessage());
        }
        
        $this->emit('activeAddressUpdated');

    }

    public function render()
    {
        $count_cart = Cart::where('user_id', Auth::id())->count();
        $categories = Category::latest()->get();

        $user_addresses = UserAddress::where('user_id', Auth::id())->orderBy('is_active', 'DESC')->get();

        return view('livewire.user.profile', compact('count_cart', 'categories', 'user_addresses'));
    }
}
