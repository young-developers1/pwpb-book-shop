<?php

namespace App\Http\Livewire\User;

use App\Models\Book;
use App\Models\Cart as ModelsCart;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Cart extends Component
{
    public $cart_id;
    public $quantity = 1;
    public $is_checked;
    public $search;

    public function mount()
    {        
        $this->is_checked = session()->get('is_checked');

        $carts = ModelsCart::where('user_id', Auth::id())->get();
        foreach($carts as $cart) {
            if($cart->is_checked == 1) {
                $this->is_checked = true;
            } else {
                return false;
            }
        }
    }

    public function updated()
    { 
        $cart = ModelsCart::find($this->cart_id);
        if($this->quantity < 1) {
            $cart->quantity = 1;
            $cart->update();
        }
        if($this->search) {
            return redirect('/books?search=' . $this->search);
        }
    }

    public function updateQuantity($id)
    { 
        $this->cart_id = $id;
    }

    public function updateChecked($id)
    {
        $cart = ModelsCart::find($id);
        $cart->is_checked = !$cart->is_checked;
        $cart->update();
    }

    public function checkAll()
    {
        $carts = ModelsCart::where('user_id', Auth::id())->get();

        foreach($carts as $cart) {
            if($this->is_checked) {
                $cart->is_checked = 1;
                session()->put('is_checked', true);
            } else {
                $cart->is_checked = 0;
                session()->put('is_checked', false);
            }
            
            $cart->update();
        }
    }

    public function decrementQty($id)
    {
        $cart = ModelsCart::find($id);

        if($cart->quantity == 1) {
            $cart->quantity = 1;
        } else {
            $cart->quantity -= 1;
        }

        $cart->update();
    }

    public function incrementQty($id)
    {        
        $cart = ModelsCart::find($id);
        $book = Book::find($cart->book_id);

        if($cart->quantity < $book->stock) {
            $cart->quantity += 1;
            $cart->update();
        } else { 
            session()->flash('error', 'Stok buku tersisa ' . $book->stock);           
        }
    }

    public function deleteCart($id)
    {
        $cart = ModelsCart::find($id);        
        $cart->delete();
    }

    public function deleteAllCart()
    {
        $carts = ModelsCart::all();        

        foreach($carts as $cart) {
            $cart->delete();
        }
        
    }

    public function checkBeforeCheckout()
    {
        $getBookSelected = ModelsCart::where('user_id', Auth::id())->where('is_checked', 1)->exists();

        if($getBookSelected) {
            return redirect('checkout');
        } else {
            session()->flash('error', 'Pilih produk terlebih dahulu sebelum checkout!');
        }
    }

    public function render()
    {
        $categories = Category::latest()->get();
        $count_cart = ModelsCart::where('user_id', Auth::id())->count();
        $carts = ModelsCart::latest()->with('book')->where('user_id', Auth::id())->get();   
        
        $recomended_books = Book::inRandomOrder()->where('status', 'active')->limit(8)->get();        

        return view('livewire.user.cart', compact('categories', 'count_cart', 'carts', 'recomended_books'));
    }
}
