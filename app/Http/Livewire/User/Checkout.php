<?php

namespace App\Http\Livewire\User;

use App\Models\Book;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\UserAddress;
use App\Models\Voucher;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Livewire\Component;

class Checkout extends Component
{
    public $search;

    public $address_id;
    public $receipent_name;
    public $phone;
    public $province;
    public $regency;
    public $district;
    public $pos_code;
    public $address;
    public $label = 'home';
    public $note;

    public $shipping_method;
    public $shipping_price = 0;
    public $payment_method;

    public $voucher_id;
    public $voucher_code;
    public $voucher_type;
    public $voucher_price = 0;

    public $final_total;

    public function updated()
    {
        if($this->shipping_method === 'j&t') {
            $this->shipping_price = 35000;
        } else if($this->shipping_method === 'sicepat') {
            $this->shipping_price = 31500;
        } else if($this->shipping_method === 'jne') {
            $this->shipping_price = 28000;
        } else {
            $this->shipping_price = 0;
        }

        if($this->search) {
            return redirect('/books?search=' . $this->search);
        }
    }

    public function resetAll()
    {
        $this->reset();
    }

    public function storeAddress()
    {
        $validatedData = $this->validate([
            'receipent_name' => 'required',
            'phone' => 'required|max:12',
            'province' => 'required',
            'regency' => 'required',
            'district' => 'required',
            'pos_code' => 'required',
            'address' => 'required',
            'label' => 'required',
        ]);

        $validatedData['user_id'] = Auth::id();

        try {
            DB::beginTransaction();
            $user_addresses = UserAddress::where('is_active', 1)->first();
            if($user_addresses) {
                $user_addresses->is_active = 0;
                $user_addresses->update();
            }

            $validatedData['is_active'] = 1;

            UserAddress::create($validatedData);
            
            DB::commit();
            session()->flash('success', 'Berhasil menambahkan alamat baru');
            $this->reset();
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('Error: ' . $e->getMessage());
        }
        
        $this->emit('addressAdded');

    }

    public function editAddress($id)
    {
        $user_address = UserAddress::find($id);

        $this->address_id = $user_address->id;
        $this->receipent_name = $user_address->receipent_name;
        $this->phone = $user_address->phone;
        $this->province = $user_address->province;
        $this->regency = $user_address->regency;
        $this->district = $user_address->district;
        $this->pos_code = $user_address->pos_code;
        $this->address = $user_address->address;
        $this->label = $user_address->label;
    }

    public function updateAddress()
    {
        $validatedData = $this->validate([
            'receipent_name' => 'required',
            'phone' => 'required|max:12',
            'province' => 'required',
            'regency' => 'required',
            'district' => 'required',
            'pos_code' => 'required',
            'address' => 'required',
            'label' => 'required',
        ]);

        try {
            DB::beginTransaction();

            $user_address = UserAddress::find($this->address_id);
            $user_address->update($validatedData);
            
            DB::commit();
            session()->flash('success', 'Berhasil mengubah alamat');
            $this->reset();
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('Error: ' . $e->getMessage());
        }
        
        $this->emit('addressUpdated');

    }

    public function updateActiveAddress($id)
    {
        try {
            DB::beginTransaction();

            $user_addresses = UserAddress::all();

            foreach($user_addresses as $address) {
                $address->is_active = 0;
                $address->update();
            }

            $user_address = UserAddress::find($id);
            $user_address->is_active = 1;
            $user_address->update();
            
            DB::commit();
            session()->flash('success', 'Berhasil mengubah alamat utama');
            $this->reset();
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('Error: ' . $e->getMessage());
        }

        $this->emit('updatedActiveAddress');
    }

    public function addVoucher($voucher_id)
    {
        $voucher = Voucher::find($voucher_id);

        if($voucher) {
            $check_voucher_date = $voucher->whereDate('effective_date_start', '<', Carbon::now())->whereDate('effective_date_end', '>', Carbon::now());
            if($check_voucher_date->exists()) {
                $this->voucher_id = $voucher->id;
                $this->voucher_code = $voucher->voucher_code;
                $this->voucher_type = $voucher->type;
                $this->voucher_price = $voucher->nominal;
                session()->flash('voucher_found', 'Voucher berhasil digunakan');
            } else {
                session()->flash('voucher_notfound', 'Voucher tidak berlaku');
            }
        } else {
            session()->flash('voucher_notfound', 'Voucher tidak ditemukan');
        }

        $this->emit('voucherAdded');
    }

    public function addVoucherDiscount()
    {
        $voucher = Voucher::where('voucher_code', $this->voucher_code)->first();

        if($voucher) {
            $check_voucher_date = $voucher->whereDate('effective_date_start', '<', Carbon::now())->whereDate('effective_date_end', '>', Carbon::now());
            if($check_voucher_date->exists()) {
                $this->voucher_id = $voucher->id;
                $this->voucher_type = $voucher->type;
                $this->voucher_price = $voucher->nominal;
                session()->flash('voucher_found', 'Voucher berhasil digunakan');
            } else {
                session()->flash('voucher_notfound', 'Voucher tidak berlaku');
            }
        } else {
            session()->flash('voucher_notfound', 'Voucher tidak ditemukan');
        }
    }

    public function checkoutOrder()
    {
        $this->validate([
            'shipping_method' => 'required',
            'payment_method' => 'required'
        ]);

        try {
            DB::beginTransaction();
            
            $user_address_active = UserAddress::where('is_active', 1)->first();
            $carts = Cart::where('user_id', Auth::id())->where('is_checked', 1)->with('book')->get();            

            $subtotal = 0;

            foreach($carts as $cart) {
                if($cart->book->discount > 0) {
                    $subtotal += ($cart->book->price * (100 - $cart->book->discount) / 100) * $cart->quantity;
                } else {
                    $subtotal += $cart->book->price * $cart->quantity;
                }
            }
            
            if($user_address_active) {
                $order = Order::create([
                    'order_code' => strtoupper(Str::random(10)),
                    'user_address_id' => $user_address_active->id,
                    'user_id' => Auth::id(),
                    'sub_total' => $subtotal,
                    'shipping_method' => $this->shipping_method,
                    'shipping_price' => $this->shipping_price,
                    'payment_method' => $this->payment_method,
                    'voucher_id' => $this->voucher_id,
                    'note' => $this->note,
                ]);                        
                                
                if($order) {                    

                    foreach($carts as $cart) {
                        if($cart->book->discount > 0) {
                            OrderItem::create([
                                'order_id' => $order->id,
                                'book_id' => $cart->book->id,
                                'quantity' => $cart->quantity,
                                'price' => $cart->book->price * (100 - $cart->book->discount) / 100,
                            ]); 
                        } else {
                            OrderItem::create([
                                'order_id' => $order->id,
                                'book_id' => $cart->book->id,
                                'quantity' => $cart->quantity,
                                'price' => $cart->book->price,
                            ]); 
                        }

                        $cart->delete();
                    }

                    // $books = Book::whereIn('id', [DB::raw("SELECT book_id FROM order_items WHERE order_id='$order->id'")])->get();
                    // foreach($books as $book) {
                    //     foreach($carts as $cart) {
                    //         $book->stock -= $cart->quantity;
                    //         $cart->delete();
                    //     }
                    //     $book->update();                        
                    // }                    
                }                
                DB::commit();
                
                session()->flash('success', 'Terima kasih ' . Auth::user()->name . ' telah order di website kami, Pesanan anda akan kami proses');
                return redirect('/orders');
            } else {
                session()->flash('error', 'Pilih alamat tujuan sebelum checkout!');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }
    }

    public function render()
    {
        $categories = Category::latest()->get();
        $count_cart = Cart::where('user_id', Auth::id())->count();
        $carts = Cart::where('user_id', Auth::id())->where('is_checked', 1)->with('book')->get();

        $get_active_user_address = UserAddress::where('is_active', 1)->where('user_id', Auth::id())->first();
        $user_addresses = UserAddress::where('user_id', Auth::id())->latest()->get();

        $vouchers = Voucher::latest()->where('status', 'public')->get();

        return view('livewire.user.checkout', compact('categories', 'count_cart', 'carts', 'get_active_user_address', 'user_addresses', 'vouchers'));
    }
}
