<?php

namespace App\Http\Livewire\User;

use App\Models\Book;
use App\Models\Cart;
use App\Models\Category;
use App\Models\Order;
use App\Models\RateReview;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class DetailOrder extends Component
{
    public $search;
    public $order_code;

    public $book_id;
    public $book_title;

    public $rating;
    public $review;
    public $is_hidden = false;

    public function mount($order_code)
    {
        $this->order_code = $order_code;
    }

    public function updated()
    {
        if($this->search) {
            return redirect('/books?search=' . $this->search);
        }
    }

    public function completingOrder()
    {
        try {
            $order = Order::where('order_code', $this->order_code)->first();
            $order->status = 'Pesanan selesai';
            $order->update();
            
            return redirect('/orders')->with('success', 'Pesanan anda telah diselesaikan');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('orderUpdated');
    }

    public function cancellingOrder()
    {
        try {
            $order = Order::where('order_code', $this->order_code)->first();
            $order->status = 'Pesanan dibatalkan';
            $order->update();
            
            return redirect('/orders')->with('success', 'Pesanan anda telah dibatalkan');
        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('orderUpdated');
    }

    public function buyAgain($book_id)
    {
        try {  
            DB::beginTransaction();     
            $cart = Cart::where('user_id', Auth::id())->where('book_id', $book_id)->first();

            if($cart) {
                $cart->quantity += 1;
                $cart->update();
            } else {
                Cart::create([
                    'user_id' => Auth::id(),
                    'book_id' => $book_id,
                    'quantity' => 1,
                ]);
            }
            
            DB::commit();
            return redirect('/cart');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }
    }
    public function review($book_id)
    {
        $book = Book::find($book_id);
        $this->book_id = $book->id;
        $this->book_title = $book->title;

        $rate_review = RateReview::where('user_id', Auth::id())->where('book_id', $book_id);

        if($rate_review->exists()) {
            $this->rating = $rate_review->first()->rating;
            $this->review = $rate_review->first()->review;
            $this->is_hidden = $rate_review->first()->is_hidden;
        }
    }

    public function resetAll()
    {
        $this->rating = '';
        $this->review = '';
        $this->is_hidden = '';
    }

    public function reviewBook()
    {
        try {  
            DB::beginTransaction();     
            $rate_review = RateReview::where('user_id', Auth::id())->where('book_id', $this->book_id)->first();
            
            if($rate_review) {                
                $rate_review->rating = $this->rating;
                $rate_review->review = $this->review;
                $rate_review->is_hidden = $this->is_hidden;
                $rate_review->update();
            } else {
                RateReview::create([
                    'book_id' => $this->book_id,
                    'user_id' => Auth::id(),
                    'rating' => $this->rating,
                    'review' => $this->review,
                    'is_hidden' => $this->is_hidden,
                ]);
            }

            $this->resetAll();
            
            DB::commit();
            session()->flash('success', 'Terimakasih telah memberikan penilaian untuk buku ini');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }

        $this->emit('reviewCreated');
    }

    public function render()
    {
        $order = Order::where('order_code', $this->order_code)->first();
        $categories = Category::latest()->get();
        $count_cart = Cart::where('user_id', Auth::id())->count();

        return view('livewire.user.detail-order', compact('categories', 'count_cart', 'order'));
    }
}
