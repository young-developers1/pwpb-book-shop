<?php

namespace App\Http\Livewire\User;

use App\Models\Book;
use App\Models\Cart;
use App\Models\Category;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Home extends Component
{
    public $search;

    public function updated()
    {
        if($this->search) {
            return redirect('/books?search=' . $this->search);
        }
    }

    public function render()
    {
        $latest_books = Book::where('status', 'active')->latest()->limit(8)->get();
        $favorite_books = OrderItem::select('order_items.book_id', DB::raw('SUM(order_items.price * order_items.quantity) as total_price'), DB::raw('SUM(order_items.quantity) AS total_quantity'))    
        ->groupBy('order_items.book_id')
        ->orderBy('total_quantity', 'DESC')
        ->with('book')
        ->get();

        $categories = Category::latest()->get();
        $count_cart = Cart::where('user_id', Auth::id())->count();

        return view('livewire.user.home', compact('latest_books', 'categories', 'count_cart', 'favorite_books'));
    }
}
