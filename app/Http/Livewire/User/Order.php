<?php

namespace App\Http\Livewire\User;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Order as ModelsOrder;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Order extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search;

    public function updated()
    {
        if($this->search) {
            return redirect('/books?search=' . $this->search);
        }
    }

    public function render()
    {
        $count_cart = Cart::where('user_id', Auth::id())->count();
        $categories = Category::latest()->get();

        $orders = ModelsOrder::latest()->where("user_id", Auth::id())->with(['order_items', 'voucher'])->paginate(1);

        return view('livewire.user.order', compact('count_cart', 'categories', 'orders'));
    }
}
