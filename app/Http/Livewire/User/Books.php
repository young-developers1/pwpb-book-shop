<?php

namespace App\Http\Livewire\User;

use App\Models\Book;
use App\Models\Cart;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Books extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    
    public $search = '';
    public $filter = 'latest';
    public $category = '';
    public $minprice = '';
    public $maxprice = '';
    public $filter_stock = '';

    protected $queryString = ['search', 'filter', 'category', 'minprice', 'maxprice'];

    public function updated()
    {
        $this->resetPage();      
    }

    public function resetFilter()
    {
        $this->category = '';
        $this->minprice = '';
        $this->maxprice = '';
    }

    public function render()
    {
        if($this->filter == 'latest') {
            $books = Book::latestBooks($this->search, $this->category, $this->minprice, $this->maxprice);
        } else if ($this->filter == 'oldest') {
            $books = Book::oldestBooks($this->search, $this->category, $this->minprice, $this->maxprice);
        } else if($this->filter == 'bestseller') {
            $books = Book::bestSellerBooks($this->search, $this->category, $this->minprice, $this->maxprice);
            dd($books);
        }

        $count_cart = Cart::where('user_id', Auth::id())->count();

        $categories = Category::latest()->get();

        return view('livewire.user.books', compact('books', 'count_cart', 'categories'));
    }
}
