<?php

namespace App\Http\Livewire\User;

use App\Models\Book as ModelsBook;
use App\Models\Cart;
use App\Models\Category;
use App\Models\OrderItem;
use App\Models\RateReview;
use App\Models\SubImageBook;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Book extends Component
{
    public $book_id;
    public $book_slug;
    public $category;
    public $quantity = 1;
    public $search;

    public $rate_review_id;
    public $rating = 1;
    public $review;
    public $is_hidden = false;

    public $filter_rating = 'latest';

    public function mount($category, $slug)
    {
        $this->book_slug = $slug;
        $this->category = $category;
    }

    public function updated()
    { 
        if($this->quantity < 1) {
            $this->quantity = 1;
        }
        if($this->search) {
            return redirect('/books?search=' . $this->search);
        }
    }

    public function resetAll()
    {
        $this->rate_review_id = '';
        $this->rating = '';
        $this->review = '';
        $this->is_hidden = '';
    }

    public function decrementQty()
    {
        if($this->quantity == 1) {
            $this->quantity = 1;
        } else {
            $this->quantity -= 1;
        }
    }

    public function incrementQty()
    {
        $this->quantity += 1;
    }

    public function addToCart()
    {
        try {
            DB::beginTransaction();

            if(Auth::check()) {
                $book = ModelsBook::where('slug', $this->book_slug)->first();

                $check_cart = Cart::where('book_id', $book->id)->where('user_id', Auth::id());
                
                if($this->quantity <= $book->stock) {
                    if($check_cart->exists()) {
                        $cart = $check_cart->first();
                        
                        $cart->quantity += $this->quantity;
                        $cart->update();
                    } else {
                        Cart::create([
                            'user_id' => Auth::id(),
                            'book_id' => $book->id,
                            'quantity' => $this->quantity
                        ]);
                    }
    
                    DB::commit();
                    session()->flash('success', 'Buku berhasil ditambahkan ke keranjang');
                } else {
                    session()->flash('error', 'Stok buku tersisa ' . $book->stock);
                }
            } else {
                return redirect('/login');
            }                      

        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }
    }

    public function buyNow()
    {
        try {
            DB::beginTransaction();

            if(Auth::check()) {
                $book = ModelsBook::where('slug', $this->book_slug)->first();

                $check_cart = Cart::where('book_id', $book->id)->where('user_id', Auth::id());
                
                if($check_cart->exists()) {
                    $cart = $check_cart->first();

                    $cart->quantity += $this->quantity;
                    $cart->update();
                } else {
                    Cart::create([
                        'user_id' => Auth::id(),
                        'book_id' => $book->id,
                        'quantity' => $this->quantity
                    ]);
                }

                DB::commit();
                return redirect('/checkout');
            } else {
                return redirect('/login');
            }                      

        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('error', 'Error: ' . $e->getMessage());
        }
    }

    public function review($review_id)
    {
        $book = ModelsBook::where('slug', $this->book_slug)->first();
        $this->book_id = $book->id;
        $this->book_title = $book->title;

        $rate_review = RateReview::find($review_id);

        $this->rate_review_id = $rate_review->id;
        $this->rating = $rate_review->rating;
        $this->review = $rate_review->review;
        $this->is_hidden = $rate_review->is_hidden;
        
    }

    public function updateReviewBook()
    {
        try {  
            DB::beginTransaction();     
            $rate_review = RateReview::find($this->rate_review_id);
            
            $rate_review->rating = $this->rating;
            $rate_review->review = $this->review;
            $rate_review->is_hidden = $this->is_hidden;
            $rate_review->update();
            
            DB::commit();
            session()->flash('successReview', 'Ulasan anda berhasil diubah');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('errorReview', 'Error: ' . $e->getMessage());
        }

        $this->emit('reviewUpdated');
    }

    public function deleteReview($review_id)
    {
        $this->rate_review_id = $review_id;
    }
    
    public function destroyReview()
    {
        try {  
            DB::beginTransaction();     
            $rate_review = RateReview::find($this->rate_review_id);
            $rate_review->delete();
            
            DB::commit();
            session()->flash('successReview', 'Ulasan anda berhasil dihapus');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('errorReview', 'Error: ' . $e->getMessage());
        }

        $this->emit('reviewDeleted');
    }

    public function render()
    {
        $book = ModelsBook::where('slug', $this->book_slug)->first();
        $book_images = SubImageBook::where('book_id', $book->id)->get();
        $count_cart = Cart::where('user_id', Auth::id())->count();

        $categories = Category::latest()->get();
        $category = Category::where('category_name', $this->category)->first();

        $related_books = ModelsBook::where('category_id', $category->id)->where('status', 'active')->where('id', '!=', $book->id)->get();

        if($this->filter_rating == 'latest') {
            $reviewBooks = RateReview::latest()->where('book_id', $book->id)->with('user')->paginate(5);
        } else if($this->filter_rating == 'oldest') {
            $reviewBooks = RateReview::oldest()->where('book_id', $book->id)->with('user')->paginate(5);
        } else if($this->filter_rating == 'highest_rate') {
            $reviewBooks = RateReview::where('book_id', $book->id)->with('user')->orderBy('rating', 'DESC')->paginate(5);
        } else if($this->filter_rating == 'lowest_rate') {
            $reviewBooks = RateReview::where('book_id', $book->id)->with('user')->orderBy('rating')->paginate(5);
        }
        
        $rating_count = RateReview::where('book_id', $book->id)->count('rating');
        $review_count = RateReview::where('book_id', $book->id)->count('review');

        $total_rating = RateReview::where('book_id', $book->id)->sum('rating');
        if($total_rating > 0) {
            $rating_value = $total_rating / $reviewBooks->count();
        } else {
            $rating_value = 0;
        }

        $book_sold = OrderItem::where('book_id', $book->id)->get();
        $book_sold_count = 0;

        foreach($book_sold as $order) {
            $book_sold_count += $order->quantity;
        }

        return view('livewire.user.book', compact('book', 'book_images', 'count_cart', 'categories', 'related_books', 'reviewBooks', 'rating_count', 'review_count', 'rating_value', 'book_sold_count'));
    }
}
