<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;

class Login extends Component
{
    public $username = '';
    public $password = '';

    protected $rules = [
        'username' => 'required',
        'password' => 'required',
    ];

    public function mount() {
        if(auth()->user()){
            redirect('/admin/dashboard');
        }        
    }

    public function login() {
        $credentials = $this->validate();
        if(auth()->attempt(['username' => $this->username, 'password' => $this->password])) {
            $user = User::where(["username" => $this->username])->first();
            auth()->login($user);
            return redirect('/admin/dashboard');        
        }
        else{
            $this->password = '';
            return $this->addError('username', trans('auth.failed')); 
        }
    }

    public function render()
    {
        return view('livewire.auth.login');
    }
}
