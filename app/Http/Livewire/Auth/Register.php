<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;

class Register extends Component
{
    public $name = '';
    public $username = '';
    public $email = '';
    public $password = '';
    public $confirmation_password = '';

    protected $rules = [
        'name' => 'required',
        'username' => 'required|min:5|alpha_dash',
        'email' => 'required|email:dns',
        'password' => 'required|min:8|same:confirmation_password',
    ];


    public function mount() {
        if(auth()->user()){
            redirect('/admin/dashboard');
        }        
    }

    public function register()
    {
        $this->validate();
        try {
            
            if(User::where('username', $this->username)->exists() || User::where('email', $this->email)->exists()) {
                session()->flash('error', 'Username atau email sudah terdaftar');
            } else {
                User::create([
                    'name' => $this->name,
                    'username' => $this->username,
                    'email' => $this->email,
                    'password' => bcrypt($this->password),
                ]);

                return redirect('login');
            }

        } catch (\Exception $e) {
            session()->flash('error', 'Error: ' . $e->getMessage());
        }
    }

    public function render()
    {
        return view('livewire.auth.register');
    }
}
