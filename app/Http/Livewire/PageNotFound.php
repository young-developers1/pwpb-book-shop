<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PageNotFound extends Component
{
    public function render()
    {
        return view('livewire.page-not-found');
    }
}
