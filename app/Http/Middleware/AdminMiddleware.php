<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->role_as == 'admin' || Auth::user()->role_as == 'Super admin') {
                return $next($request);
            } else {
                session()->flash('error', 'Access Denied! As you are not Admin');
                return redirect('/');
            }
        } else {
            session()->flash('error', 'Please Login First');
            return redirect('/login');
        }
    }
}
