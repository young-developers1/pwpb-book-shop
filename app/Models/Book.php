<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Book extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    
    protected $dates = ['issue_date'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function latestBooks($search, $category, $minprice, $maxprice)
    {
        return Book::latest()
                    ->with('category')
                    ->where('status', 'active')
                    ->where('title', 'LIKE', "%$search%")
                    ->where('category_id', 'LIKE', "%$category%")
                    ->orWhereBetween('price', [$minprice, $maxprice])                    
                    ->paginate(12);
    }

    public static function oldestBooks($search, $category, $minprice, $maxprice)
    {
        return Book::oldest()
                    ->with('category')
                    ->where('status', 'active')
                    ->where('title', 'LIKE', "%$search%")
                    ->where('category_id', 'LIKE', "%$category%")
                    ->orWhereBetween('price', [$minprice, $maxprice])                    
                    ->paginate(12);
    }

    public static function bestSellerBooks($search, $category, $minprice, $maxprice)
    {
        return Book::with('category')
                    ->where('status', 'active')
                    ->where('title', 'LIKE', "%$search%")
                    ->where('category_id', 'LIKE', "%$category%")
                    ->orWhereBetween('price', [$minprice, $maxprice])                    
                    ->paginate(12);
    }
}
