<?php

use App\Http\Livewire\Admin\Account\Index as AdminAccountIndex;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Logout;
use App\Http\Livewire\Auth\Register;
use App\Http\Livewire\Admin\Book\Index as BookIndex;
use App\Http\Livewire\Admin\Category\Index as CategoryIndex;
use App\Http\Livewire\Admin\Dashboard\Index as DashboardIndex;
use App\Http\Livewire\Admin\Notification\Index as NotificationIndex;
use App\Http\Livewire\Admin\Order\Detail as DetailOrder;
use App\Http\Livewire\Admin\Order\Index as OrderIndex;
use App\Http\Livewire\Admin\Report\Index as ReportIndex;
use App\Http\Livewire\Admin\Voucher\Index as VoucherIndex;
use App\Http\Livewire\User\Book;
use App\Http\Livewire\User\Books;
use App\Http\Livewire\User\Cart;
use App\Http\Livewire\User\Checkout;
use App\Http\Livewire\User\DetailOrder as UserDetailOrder;
use App\Http\Livewire\User\Home;
use App\Http\Livewire\User\Order;
use App\Http\Livewire\User\Profile;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->group(function() {
    Route::get('/login', Login::class)->name('login');
    Route::get('/register', Register::class)->name('register');
});

Route::middleware('auth', 'admin')->prefix('/admin')->group(function() {
    Route::get('/dashboard', DashboardIndex::class)->name('dashboard.index');
    Route::get('/notifications', NotificationIndex::class)->name('notification.index');
    Route::get('/books', BookIndex::class)->name('book.index');
    Route::get('/categories', CategoryIndex::class)->name('category.index');
    Route::get('/vouchers', VoucherIndex::class)->name('voucher.index');

    Route::get('/orders', OrderIndex::class)->name('order.index');
    Route::get('/orders/detail/{id}', DetailOrder::class)->name('order.detail');

    Route::get('/admin-accounts', AdminAccountIndex::class)->name('admin.index');
    Route::get('/reports', ReportIndex::class)->name('report.index');

    Route::get('/logout', Logout::class);
});

Route::middleware('auth')->group(function() {
    Route::get('/cart', Cart::class)->name('cart');
    Route::get('/checkout', Checkout::class)->name('checkout');
    Route::get('/orders', Order::class)->name('orders');
    Route::get('/orders/{order_code}', UserDetailOrder::class)->name('order-detail');
    Route::get('/profile', Profile::class)->name('profile');
});

Route::get('/', Home::class)->name('home');
Route::get('/books', Books::class)->name('books');
Route::get('/book/{category}/{slug}', Book::class)->name('book');

Route::fallback(function() {
    return view('livewire.page-not-found');
});
