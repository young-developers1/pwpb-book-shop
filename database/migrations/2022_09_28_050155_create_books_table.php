<?php

use App\Models\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Category::class);
            $table->string('title', 100);
            $table->string('slug', 100);
            $table->string('thumbnail_img', 150)->nullable();
            $table->text('description');
            $table->integer('total_page');
            $table->string('publisher', 50);
            $table->date('issue_date');
            $table->float('weight');
            $table->string('isbn', 100);
            $table->string('language', 15);
            $table->string('writer', 50);
            $table->integer('stock');
            $table->integer('price');
            $table->integer('discount')->default(0);
            $table->enum('status', ['active', 'inactive']);
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
};
