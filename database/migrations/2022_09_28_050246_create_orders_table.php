<?php

use App\Models\User;
use App\Models\UserAddress;
use App\Models\Voucher;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('order_code', 10);
            $table->foreignIdFor(UserAddress::class);
            $table->foreignIdFor(User::class);            
            $table->integer('sub_total');
            $table->string('shipping_method', 30);
            $table->integer('shipping_price');
            $table->string('payment_method', 30);
            $table->foreignIdFor(Voucher::class)->nullable();
            $table->text('note')->nullable();
            $table->enum('status', ['Menunggu pembayaran', 'Sedang diproses', 'Sedang dikirim', 'Pesanan sampai', 'Pesanan selesai', 'Pesanan dibatalkan'])->default('Menunggu pembayaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
