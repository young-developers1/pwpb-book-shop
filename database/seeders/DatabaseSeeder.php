<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Book;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\Voucher;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        Category::create([
            'category_name' => 'Novel',
            'slug' => 'novel'
        ]);
        Category::create([
            'category_name' => 'Pendidikan',
            'slug' => 'pendidikan'
        ]);
        Category::create([
            'category_name' => 'Fiksi',
            'slug' => 'fiksi'
        ]);
        Category::create([
            'category_name' => 'Biografi',
            'slug' => 'biografi'
        ]);
        Category::create([
            'category_name' => 'Ensiklopedia',
            'slug' => 'ensiklopedia'
        ]);

        Book::create([
            'category_id' => 1,
            'title' => 'Rasuk',
            'slug' => 'rasuk',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 123,
            'publisher' => 'Gramedia',
            'issue_date' => '2015-10-12',
            'weight' => 356.34,
            'isbn' => 'GHDNEJ39FU354',
            'language' => 'Indonesia',
            'writer' => 'Risa Saraswati',
            'stock' => 20,
            'price' => 150000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 3,
            'title' => 'Insecurity',
            'slug' => 'insecurity',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 156,
            'publisher' => 'Gramedia',
            'issue_date' => '2017-10-12',
            'weight' => 410.05,
            'isbn' => 'FJHEU3VFI3H35R',
            'language' => 'Indonesia',
            'writer' => 'Jeff Skyling',
            'stock' => 15,
            'price' => 175000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 2,
            'title' => 'Tentang Diriku',
            'slug' => 'tentang-diriku',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 123,
            'publisher' => 'Gramedia',
            'issue_date' => '2016-10-12',
            'weight' => 356.34,
            'isbn' => 'GHDNEJ39FU354',
            'language' => 'Indonesia',
            'writer' => 'Risa Saraswati',
            'stock' => 20,
            'price' => 225000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 4,
            'title' => 'Aku Tahu Kapan Kamu Mati',
            'slug' => 'aku-tahu-kapan-kamu-mati',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 156,
            'publisher' => 'Gramedia',
            'issue_date' => '2017-10-12',
            'weight' => 410.05,
            'isbn' => 'FJHEU3VFI3H35R',
            'language' => 'Indonesia',
            'writer' => 'Jeff Skyling',
            'stock' => 18,
            'price' => 98000,
            'discount' => 20,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 5,
            'title' => 'Sejarah Indonesia',
            'slug' => 'sejarah-indonesia',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 123,
            'publisher' => 'Gramedia',
            'issue_date' => '2011-10-12',
            'weight' => 356.34,
            'isbn' => 'GHDNEJ39FU354',
            'language' => 'Indonesia',
            'writer' => 'Risa Saraswati',
            'stock' => 25,
            'price' => 120000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 1,
            'title' => 'Matematika Menyenangkan',
            'slug' => 'matematika-menyenangkan',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 156,
            'publisher' => 'Gramedia',
            'issue_date' => '2013-10-12',
            'weight' => 410.05,
            'isbn' => 'FJHEU3VFI3H35R',
            'language' => 'Indonesia',
            'writer' => 'Jeff Skyling',
            'stock' => 15,
            'price' => 75000,
            'discount' => 10,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 2,
            'title' => 'Belajar Masak',
            'slug' => 'belajar-masak',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 123,
            'publisher' => 'Gramedia',
            'issue_date' => '2015-10-12',
            'weight' => 356.34,
            'isbn' => 'GHDNEJ39FU354',
            'language' => 'Indonesia',
            'writer' => 'Risa Saraswati',
            'stock' => 20,
            'price' => 85000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 3,
            'title' => 'Mengenal Jenis Hewan',
            'slug' => 'mengenal-jenis-hewan',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 156,
            'publisher' => 'Gramedia',
            'issue_date' => '2017-10-12',
            'weight' => 410.05,
            'isbn' => 'FJHEU3VFI3H35R',
            'language' => 'Indonesia',
            'writer' => 'Jeff Skyling',
            'stock' => 15,
            'price' => 35000,
            'discount' => 5,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 4,
            'title' => 'Tips Public Speaking',
            'slug' => 'tips-public-speaking',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 123,
            'publisher' => 'Gramedia',
            'issue_date' => '2015-10-12',
            'weight' => 356.34,
            'isbn' => 'GHDNEJ39FU354',
            'language' => 'Indonesia',
            'writer' => 'Risa Saraswati',
            'stock' => 20,
            'price' => 145000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 3,
            'title' => 'Belajar Ngoding Dengan Mudah',
            'slug' => 'belajar-ngoding-dengan-mudah',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 156,
            'publisher' => 'Gramedia',
            'issue_date' => '2017-10-12',
            'weight' => 410.05,
            'isbn' => 'FJHEU3VFI3H35R',
            'language' => 'Indonesia',
            'writer' => 'Jeff Skyling',
            'stock' => 15,
            'price' => 285000,
            'discount' => 20,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 4,
            'title' => 'Belajar Investasi Saham',
            'slug' => 'belajar-investasi-saham',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 123,
            'publisher' => 'Gramedia',
            'issue_date' => '2015-10-12',
            'weight' => 356.34,
            'isbn' => 'GHDNEJ39FU354',
            'language' => 'Indonesia',
            'writer' => 'Risa Saraswati',
            'stock' => 20,
            'price' => 230000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 3,
            'title' => 'Harry Potter 4',
            'slug' => 'harry-potter-4',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 156,
            'publisher' => 'Gramedia',
            'issue_date' => '2009-10-12',
            'weight' => 410.05,
            'isbn' => 'FJHEU3VFI3H35R',
            'language' => 'Indonesia',
            'writer' => 'Jeff Skyling',
            'stock' => 15,
            'price' => 445000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 1,
            'title' => 'Manusia Setengah Salmon',
            'slug' => 'manusia-setengah-salmon',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 123,
            'publisher' => 'Gramedia',
            'issue_date' => '2012-10-12',
            'weight' => 356.34,
            'isbn' => 'GHDNEJ39FU354',
            'language' => 'Indonesia',
            'writer' => 'Raditya Dika',
            'stock' => 20,
            'price' => 115000,
            'discount' => 0,
            'status' => 'active'
        ]);

        Book::create([
            'category_id' => 2,
            'title' => 'Manusia Brontasaurus',
            'slug' => 'manusia-brontasaurus',            
            'description' => 'Ini deskripsi buku',
            'total_page' => 156,
            'publisher' => 'Gramedia',
            'issue_date' => '2017-10-12',
            'weight' => 410.05,
            'isbn' => 'FJHEU3VFI3H35R',
            'language' => 'Indonesia',
            'writer' => 'Raditya Dika',
            'stock' => 15,
            'price' => 125000,
            'discount' => 10,
            'status' => 'active'
        ]);

        User::create([
            'name' => 'Danuartha',
            'username' => 'user1',
            'email' => 'user1@gmail.com',
            'password' => bcrypt('12345678'),
            'role_as' => 'user'
        ]);

        User::create([
            'name' => 'Admin 1',
            'username' => 'admin1',
            'email' => 'admin1@gmail.com',
            'password' => bcrypt('12345678'),
            'role_as' => 'admin'
        ]);

        User::create([
            'name' => 'Super Admin 1',
            'username' => 'superadmin1',
            'email' => 'superadmin1@gmail.com',
            'password' => bcrypt('12345678'),
            'role_as' => 'Super admin'
        ]);

        UserAddress::create([
            'user_id' => 1,
            'receipent_name' => 'Danuartha',
            'label' => 'home',
            'phone' => '081337359813',
            'province' => 'Bali',
            'regency' => 'Badung',
            'district' => 'Kuta Utara',
            'pos_code' => '80361',
            'address' => 'Perum dalung permai, Blok LL No. 29A',
            'is_active' => 1
        ]);

        Voucher::create([
            'voucher_name' => 'Discount 10%',
            'voucher_code' => 'DISC10',
            'type' => 'Persentase',
            'nominal' => 10,
            'effective_date_start' => '2022-11-19',
            'effective_date_end' => '2022-11-23',
            'status' => 'public'
        ]);
        
        Order::create([
            'order_code' => 'FK4J3F2',
            'user_address_id' => 1,
            'user_id' => 1,
            'sub_total' => 825000,
            'shipping_price' => 28000,
            'shipping_method' => 'jne',
            'payment_method' => 'cod',  
            'voucher_id' => 1          
        ]);
        
        OrderItem::create([
            'order_id' => 1,
            'book_id' => 1,
            'quantity' => 2,
            'price' => 150000
        ]);

        OrderItem::create([
            'order_id' => 1,
            'book_id' => 2,
            'quantity' => 3,
            'price' => 175000
        ]);    

    }
}
